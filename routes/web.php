 <?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/pamienruhdbs', 'AppController@phpinfo');
Route::get('/migrate', 'AppController@migrate');

// STATIC PAGES
Route::get('/', 'AppController@homepage');
Route::get('/quiénes-somos', 'AppController@aboutUs');
Route::get('/nuestra-solución', 'AppController@ourSolution');

Route::get('/para-las-agronomías', 'AppController@forAgronomia');
Route::get('/para-los-productores', 'AppController@forProducer');
Route::get('/ayuda', 'AppController@faq');
Route::get('/contacto', 'AppController@contact');
Route::post('/contacto/submit', 'AppController@submitContact');
Route::get('/versiones', 'AppController@versions');
Route::get('/landing', 'AppController@landingPage');
Route::get('/landing-page-2', 'AppController@landingPage2');
// END OF STATIC PAGES

//Auth routes : /login, /register, logout and Auth
Auth::routes();
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('home', 'AppController@auth');
Route::get('logout', 'AppController@logout');
//END OF Auth routes

// register Routes
Route::prefix('register')->group(function () {
  // producer
  Route::prefix('producer')->group(function () {
    Route::get('/', 'AppController@registerProducer');
    Route::get('/success', 'AppController@registerSuccess')->defaults('user','producer');
  });

  // agronomia
  Route::prefix('agronomia')->group(function () {
    Route::get('/', 'AppController@registerAgronomia');
    Route::get('/success', 'AppController@registerSuccess')->defaults('user','agronomia');
  });
});

// terms & conditions
Route::get('terms-and-conditions/producer', 'AppController@termsAndConditions')->defaults('user','producer');
Route::get('terms-and-conditions/agronomia', 'AppController@termsAndConditions')->defaults('user','agronomia');


// Authorization Required
Route::middleware('auth')->group(function () {

  // Authorization Required
  Route::middleware('AgronomiaAuth')->group(function () {
    Route::get('agronomia', 'AppController@init');
    Route::get('agronomia/{vue?}', 'AppController@init')->where('vue', '[\/\w\.-]*');
  });

  // Authorization Required
  Route::middleware('ProducerAuth')->group(function () {
    Route::get('producer', 'AppController@init');
    Route::get('producer/{vue?}', 'AppController@init')->where('vue', '[\/\w\.-]*');
  });
});

//Web API
Route::prefix('web_api')->group(function () {

  // `API` Namespace
  Route::namespace('API')->group(function () {

    // Common API without Middleware
    Route::post('producer/register', 'ProducerAPIController@register');
    Route::post('agronomia/register', 'AgronomiaAPIController@register');
    Route::post('provinces', 'CommonAPIController@provinces');
    Route::post('localities', 'CommonAPIController@localities');
    Route::post('login', 'CommonAPIController@login');
    Route::get('auth', 'CommonAPIController@web_auth');
    Route::post('forget_password', 'CommonAPIController@forgetPassword');
    Route::get('logout', 'CommonAPIController@logout');
    Route::post('custom_page', 'CommonAPIController@customPage');
    Route::post('faq', 'CommonAPIController@faq');
    Route::get('auth', 'CommonAPIController@userAuth');
    Route::post('contact_form', 'CommonAPIController@contactForm');

    // Authorization Required
    Route::middleware('auth')->group(function () {
      
      Route::post('register/step-2', 'CommonAPIController@registerStep2');

      //Activated User Required
      Route::middleware('CheckStatus')->group(function () {

        // Common API with Authorization Middleware
        Route::post('register_step_2', 'CommonAPIController@registerStep2');
        Route::get('menubar', 'CommonAPIController@menubar');
        Route::post('dashboard', 'CommonAPIController@dashboardData');
        Route::post('change_password', 'CommonAPIController@changePassword');
        Route::get('my_account', 'CommonAPIController@myAccount');
        Route::post('update_profile', 'CommonAPIController@updateProfile');
        Route::get('user_avtars', 'CommonAPIController@userAvtars');
        Route::post('user_detail', 'CommonAPIController@userDetail');
        Route::post('product_attributes', 'CommonAPIController@ProductAttributes');
        Route::post('product_filter_attributes', 'CommonAPIController@productFilterAttributes');
        Route::get('product_categories', 'CommonAPIController@productCategories');
        Route::post('products', 'CommonAPIController@products');
        Route::get('searchProductsbyName/{search}', 'CommonAPIController@searchProductsbyName');
        Route::get('searchProductoGenerals/{search}', 'CommonAPIController@searchProductoGenerals');
        Route::post('product_detail', 'CommonAPIController@productDetail');
        Route::get('payment_methods', 'CommonAPIController@paymentMethods');
        
        Route::prefix('orders')->group(function () {
          Route::post('/', 'CommonAPIController@orders');
          Route::get('/{order}', 'CommonAPIController@orderDetail');
        });

        Route::post('enable_disable_chat', 'CommonAPIController@enableDisableChat');
        Route::post('order_chat_messages', 'CommonAPIController@orderChatMessages');
        Route::post('send_order_chat_message', 'CommonAPIController@sendOrderChatMessage');
        Route::post('review_order', 'CommonAPIController@reviewOrder');
        Route::post('reviews', 'CommonAPIController@reviews');
        Route::post('my_reviews', 'CommonAPIController@myReviews');
        Route::post('get_reviews_by_user', 'CommonAPIController@getReviewsByUser');
        Route::get('unread_notification_count', 'CommonAPIController@unreadNotificationCount');
        Route::get('notifications', 'CommonAPIController@notifications');

        //Agronomia API with Agronomia Authorization Middleware
        Route::middleware('AgronomiaToken')->prefix('agronomia')->group(function () {
          Route::post('my_products', 'AgronomiaAPIController@products');
          Route::post('apply_increase_discount', 'AgronomiaAPIController@applyIncreaseDiscount');
          Route::post('add_product', 'AgronomiaAPIController@addProduct');
          Route::post('product_detail', 'AgronomiaAPIController@productDetail');
          Route::post('edit_product', 'AgronomiaAPIController@editProduct');
          Route::post('delete_product', 'AgronomiaAPIController@deleteProduct');
          Route::post('approve_reject_order', 'AgronomiaAPIController@approveRejectOrder');
        });

        //Producer API with Producer Authorization Middleware
        Route::middleware('ProducerToken')->prefix('producer')->group(function () {
          Route::get('geolocation', 'ProducerAPIController@geolocation');
          Route::post('geolocation', 'ProducerAPIController@createGeolocation');
          Route::post('vendor_search', 'ProducerAPIController@vendorSearch');
          Route::post('list_producto_generals', 'ProducerAPIController@listProductoGenerals');
          Route::get('get_producto_general_name/{id}', 'ProducerAPIController@getProductoGeneralName');
          Route::post('place_order', 'ProducerAPIController@placeOrder');
        });
      });
    });

  }); //End of `API` Namespace

}); //End of `Web API`