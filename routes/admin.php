<?php

// header('Access-Control-Allow-Origin:  *');
// header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
// header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@adminInit');

//Web API
Route::prefix('web_api')->group(function () {

  // `API` Namespace
  Route::namespace('API')->group(function () {

    // Common API without Middleware
    Route::post('provinces', 'CommonAPIController@provinces');
    Route::post('localities', 'CommonAPIController@localities');
	  Route::post('login', 'AdminAPIController@login');
    Route::get('auth', 'CommonAPIController@web_auth');
    Route::post('forget_password', 'CommonAPIController@forgetPassword');
    Route::get('logout', 'CommonAPIController@logout');
    Route::post('custom_page', 'CommonAPIController@customPage');
    Route::post('faq', 'CommonAPIController@faq');

    // Authorization Required
    Route::middleware('auth', 'CheckStatus')->group(function () {
      
      // Common API with Authorization Middleware
      Route::get('menubar', 'CommonAPIController@menubar');
      Route::post('dashboard', 'CommonAPIController@dashboardData');
      Route::post('change_password', 'CommonAPIController@changePassword');
      Route::get('my_account', 'CommonAPIController@myAccount');
      Route::post('update_profile', 'CommonAPIController@updateProfile');
      Route::get('user_avtars', 'CommonAPIController@userAvtars');
      Route::post('user_detail', 'CommonAPIController@userDetail');
      Route::post('product_attributes', 'CommonAPIController@ProductAttributes');
      Route::get('product_categories', 'CommonAPIController@productCategories');
      Route::get('producto_generals', 'CommonAPIController@productoGenerals');
      Route::post('products', 'CommonAPIController@products');
      Route::post('product_detail', 'CommonAPIController@productDetail');
      Route::get('payment_methods', 'CommonAPIController@paymentMethods');
      Route::post('orders', 'CommonAPIController@orders');
      Route::post('enable_disable_chat', 'CommonAPIController@enableDisableChat');
      Route::post('order_chat_messages', 'CommonAPIController@orderChatMessages');
      Route::post('send_order_chat_message', 'CommonAPIController@sendOrderChatMessage');
      Route::post('review_order', 'CommonAPIController@reviewOrder');
      Route::post('reviews', 'CommonAPIController@reviews');
      Route::post('my_reviews', 'CommonAPIController@myReviews');
      Route::post('get_reviews_by_user', 'CommonAPIController@getReviewsByUser');
      Route::get('unread_notification_count', 'CommonAPIController@unreadNotificationCount');
      Route::get('notifications', 'CommonAPIController@notifications');

      //with Authorization Middleware
      Route::middleware('AdminToken')->group(function () {
        Route::post('admin_users', 'AdminAPIController@adminUsers');
        Route::post('get_admin_user', 'AdminAPIController@getAdminUser');
        Route::post('store_admin_user', 'AdminAPIController@storeAdmin');
        Route::post('change_admin_state', 'AdminAPIController@changeAdminUserState');
        Route::get('permissions_json', 'AdminAPIController@permissionsJson');    
        Route::post('app_users', 'AdminAPIController@appUsers');
        Route::post('store_app_user', 'AdminAPIController@storeAppUser');
        Route::post('change_app_user_state', 'AdminAPIController@changeAppUserState');  
        Route::post('agronomia_products', 'AdminAPIController@agronomiaProducts');
        Route::post('apply_increase_discount', 'AdminAPIController@applyIncreaseDiscount');
        Route::post('agronomia_product_detail', 'AdminAPIController@agronomiaProductDetail');
        Route::post('add_agronomia_product', 'AdminAPIController@addAgronomiaProduct');
        Route::post('edit_agronomia_product', 'AdminAPIController@editAgronomiaProduct');
        Route::put('custom_page', 'AdminAPIController@customPage');
        Route::post('save_faq', 'AdminAPIController@saveFAQ');
        Route::post('change_faq_state', 'AdminAPIController@changeFAQState');
        Route::get('search_producto_general/{search}','AdminAPIController@searchProductGeneral');
        Route::post('store_product', 'AdminAPIController@storeProduct');
        Route::post('change_product_state', 'AdminAPIController@changeProductState');
        Route::post('delete_product', 'AdminAPIController@deleteProduct');
        Route::post('reviews', 'AdminAPIController@reviews');
        Route::post('change_review_state', 'AdminAPIController@changeReviewState');
        Route::post('notifications', 'AdminAPIController@notifications');
        Route::post('billing', 'AdminAPIController@billing');
        Route::post('export_billing_csv', 'AdminAPIController@exportBillingCSV');
        Route::post('store_admin_message', 'AdminAPIController@storeAdminMessage');
        Route::post('search_app_user', 'AdminAPIController@searchAppUser');
        Route::post('upload_products_csv', 'AdminAPIController@uploadProductsCSV');
        Route::post('export_products_csv', 'AdminAPIController@exportProductsCSV');
        Route::get('remove_products_csv', 'AdminAPIController@removeProductsCSV');
        Route::get('get_tnc', 'AdminAPIController@getTnc');
      });
    });

  }); //End of `API` Namespace

}); //End of `Web API`


Route::get('/{vue?}', 'AppController@adminInit')->where('vue', '[\/\w\.-]*');