<?php

// header('Access-Control-Allow-Origin:  *');
// header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
// header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// `API` Namespace
Route::namespace('API')->group(function () {

	// Common API without Middleware
  Route::post('producer/register', 'ProducerAPIController@register');
  Route::post('agronomia/register', 'AgronomiaAPIController@register');
	Route::post('login', 'CommonAPIController@login');
	Route::post('forget_password', 'CommonAPIController@forgetPassword');
	Route::post('custom_page', 'CommonAPIController@customPage');
	Route::post('faq', 'CommonAPIController@faq');
  Route::post('provinces', 'CommonAPIController@provinces');
  Route::post('localities', 'CommonAPIController@localities');
  Route::post('validate_cuit', 'CommonAPIController@validateCUIT');

	// Authorization Required
  Route::middleware('jwt.auth', 'CheckStatus')->group(function () {

    // Common API with Authorization Middleware
    Route::post('register_step_2', 'CommonAPIController@registerStep2');
    Route::post('dashboard', 'CommonAPIController@dashboardData');
    Route::post('change_password', 'CommonAPIController@changePassword');
    Route::get('my_account', 'CommonAPIController@myAccount');
    Route::post('update_profile', 'CommonAPIController@updateProfile');
    Route::get('user_avtars', 'CommonAPIController@userAvtars');
    Route::post('user_detail', 'CommonAPIController@userDetail');
    Route::post('product_filter_attributes', 'CommonAPIController@productFilterAttributes');
    Route::get('product_categories', 'CommonAPIController@productCategories');
    Route::post('products', 'CommonAPIController@products');
    Route::get('searchProductsbyName/{search}', 'CommonAPIController@searchProductsbyName');
    Route::get('searchProductoGenerals/{search}', 'CommonAPIController@searchProductoGenerals');
    Route::post('product_detail', 'CommonAPIController@productDetail');
    Route::get('payment_methods', 'CommonAPIController@paymentMethods');
    
    Route::prefix('orders')->group(function () {
      Route::post('/', 'CommonAPIController@orders');
      Route::get('/{order}', 'CommonAPIController@orderDetail');
    });

    Route::post('enable_disable_chat', 'CommonAPIController@enableDisableChat');
    Route::post('order_chat_messages', 'CommonAPIController@orderChatMessages');
    Route::post('send_order_chat_message', 'CommonAPIController@sendOrderChatMessage');
    Route::post('review_order', 'CommonAPIController@reviewOrder');
    Route::post('reviews', 'CommonAPIController@reviews');
    Route::post('my_reviews', 'CommonAPIController@myReviews');
    Route::post('get_reviews_by_user', 'CommonAPIController@getReviewsByUser');
    Route::get('unread_notification_count', 'CommonAPIController@unreadNotificationCount');
    Route::get('notifications', 'CommonAPIController@notifications');
    Route::post('update_FCM_token', 'CommonAPIController@updateFCMToken');

    //Agronomia API with Agronomia Authorization Middleware
    Route::middleware('AgronomiaToken')->prefix('agronomia')->group(function () {
      Route::post('my_products', 'AgronomiaAPIController@products');
      Route::post('apply_increase_discount', 'AgronomiaAPIController@applyIncreaseDiscount');
      Route::post('add_product', 'AgronomiaAPIController@addProduct');
      Route::post('product_detail', 'AgronomiaAPIController@productDetail');
      Route::post('edit_product', 'AgronomiaAPIController@editProduct');
      Route::post('delete_product', 'AgronomiaAPIController@deleteProduct');
      Route::post('approve_reject_order', 'AgronomiaAPIController@approveRejectOrder');
    });

    //Producer API with Producer Authorization Middleware
    Route::middleware('ProducerToken')->prefix('producer')->group(function () {
      Route::get('geolocation', 'ProducerAPIController@geolocation');
      Route::post('geolocation', 'ProducerAPIController@createGeolocation');
      Route::post('vendor_search', 'ProducerAPIController@vendorSearch');
      Route::post('list_producto_generals', 'ProducerAPIController@listProductoGenerals');
      Route::get('get_producto_general_name/{id}', 'ProducerAPIController@getProductoGeneralName');
      Route::post('place_order', 'ProducerAPIController@placeOrder');
    });

  });

});
