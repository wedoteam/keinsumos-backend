<?php
	return [

    // APPLICATION CONFIG
    'APP_COMPANY'   => 'Ke insumos',
    'APP_EMAIL'   => 'info@keinsumos.com',
    'APP_CONTACTFORM_EMAIL'   => 'contacto@keinsumos.com',
    'REVIEW_HOURS'  => 72,

		// USERS TYPE
    'BUSINESS_USER' => 1,
    'CUSTOMER_USER' => 2,
    'ADMIN_USER'    => 3,

    //REGISTERATION STEP 2 STATUS
    'REGISTERED' => [
      'PENDING'   => 0,
      'COMPLETED' => 1,
    ],

    // USER FIELD TYPE
    'OWN_FIELD'			=> 1, 
    'RENT_FIELD'		=> 2,

    // GLOBAL
    'ACTIVE' 				=> 1,
    'DISABLED'			=> 0,

    // GLOBAL
    'TRUE'        => 1,
    'FALSE'      => 0,

    // PRODUCT ACTION
    'INCREASE_PRICE' => 1,
    'APPLY_DISCOUNT' => 2,

    // MESSAGE STATUS
    'MESSAGE_READ' => 1,
    'MESSAGE_UNREAD' => 0,

    //ORDER STATUS
    'PENDING_ORDER_STATUS'   => 1,
    'APPROVED_ORDER_STATUS'  => 2,
    'CANCELLED_ORDER_STATUS' => 3,

    //PREFIX
    'PREFIX' => [
      'ORDER' => 'KE',
      'ORDER_LEADING_ZERO' => 4,
      'AGRONOMIA_USER' => 'AG',
      'PRODUCER_USER' => 'PR',
      'USER_LEADING_ZERO' => 4,
    ],

    //NOTIFICATION TYPE
    'NOTIFICATION' => [
      'ADMIN_MESSAGE'   => 1,
      'ORDER_PLACED'    => 2,
      'ORDER_APPROVED'  => 3,
      'ORDER_CANCELLED' => 4,
      'ORDER_RECEIVED'  => 5,
    ],

    //ADMIN MESSAGE TYPE 
    'ADMIN_MESSAGE' => [
      'AGRONOMIA'     => 1,
      'PRODUCER'      => 2,
      'ALL_USERS'     => 3,
      'SPECIFIC_USER' => 4,
      'ADMIN_NOTIFICATION' => 5,
    ],

    //REVIEW STATUS
    'REVIEW_STATUS' => [
      'PENDING'  => 1,
      'APPROVED' => 2,
      'REJECTED' => 3,
    ],
      

	];