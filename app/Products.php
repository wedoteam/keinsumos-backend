<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
  use SoftDeletes;
  protected $guarded = [];

  public function productSubCategories()
  {
  	return $this->belongsTo('App\ProductSubCategories', 'subcategory_id');
  }

  public function productoGenerals()
  {
  	return $this->belongsTo('App\ProductoGenerals', 'producto_general_id');
  }

  public function product_band()
  {
  	return $this->belongsTo('App\ProductBands', 'band');
  }

  public function product_treatment()
  {
  	return $this->belongsTo('App\ProductTreatments', 'treatments');
  }

  public function product_diameter()
  {
  	return $this->belongsTo('App\ProductDiameters', 'diameter');
  }

  public function product_length()
  {
  	return $this->belongsTo('App\ProductLengths', 'length');
  }

  public function agronomiaProducts()
  {
  	return $this->hasMany('App\agronomiaProducts', 'product_id', 'id');
  }
}
