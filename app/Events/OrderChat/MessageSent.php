<?php
namespace App\Events\OrderChat;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
  use Dispatchable, InteractsWithSockets, SerializesModels;

  public $id;
  public $order_id;
  public $sender_id;
  public $receiver_id;
  public $text;
  public $is_read;
  public $created_at;

  public function __construct($response)
  {
    $this->id          = $response->id;
    $this->order_id    = $response->order_id;
    $this->sender_id   = $response->sender_id;
    $this->receiver_id = $response->receiver_id;
    $this->text        = $response->text;
    $this->is_read     = $response->is_read;
    $this->created_at  = YMDtoDMY($response->created_at);
  }

  public function broadcastOn()
  {
    return ['my-channel'];
  }

  public function broadcastAs()
  {
    return 'order-chat-message-sent';
  }
}
