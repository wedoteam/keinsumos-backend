<?php
namespace App\Events\OrderChat;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageRead implements ShouldBroadcast
{
  use Dispatchable, InteractsWithSockets, SerializesModels;

  public $order_id;
  public $reader_id;

  public function __construct($order_id, $reader_id)
  {
    $this->order_id  = $order_id;
    $this->reader_id = $reader_id;
  }

  public function broadcastOn()
  {
    return ['my-channel'];
  }

  public function broadcastAs()
  {
    return 'order-chat-message-read';
  }
}
