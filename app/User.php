<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;
use Auth;


class User extends Authenticatable implements JWTSubject
{
	use SoftDeletes;
	use Notifiable;
  protected $guarded = [];
  protected $appends = ['avtar_path', 'rating', 'user_type_label', 'user_state_label', 'boolean_state'];

  public function user_province()
  {
    return $this->belongsTo('App\Provinces', 'province');
  }

  public function user_locality()
  {
    return $this->belongsTo('App\Localities', 'locality');
  }

  public function agronomiaPaymentMethods()
  {
    return $this->hasMany('App\AgronomiaPaymentMethods', 'agronomia_id', 'id');
  }

	public function agronomia_products()
	{
		return $this->hasMany('App\AgronomiaProducts', 'agronomia_id', 'id');
	}

  public function user_geolocation()
  {
    return $this->hasMany('App\UserGeolocation', 'user_id', 'id');
  }

	public function userAvtar()
	{
		return $this->belongsTo('App\UserAvtars', 'avtar_id');
  }
  
  public function AdminPermissionGroups()
	{
		return $this->belongsTo('App\AdminPermissionGroups', 'permission_id');
	}

	public function recievedReviews()
	{
		return $this->hasMany('App\UserReviews', 'receiver_id');
  }
  
  public function getUserTypeLabelAttribute()
  {
  	if($this->user_type == config('global.BUSINESS_USER'))
  	{
  		return 'agronomia';
  	}
  	else if($this->user_type == config('global.CUSTOMER_USER'))
  	{
  		return 'producer';
  	}
  	else if($this->user_type == config('global.ADMIN_USER'))
  	{
  		return 'admin';
  	}
  }

  public function getUserStateLabelAttribute()
  {
  	if($this->is_active == config('global.ACTIVE'))
  	{
  		return __('labels.active');
  	}
  	else if($this->is_active == config('global.DISABLED'))
  	{
  		return __('labels.disabled');
  	}
  }

  public function getBooleanStateAttribute()
  {
  	return ($this->is_active == config('global.ACTIVE'))?true:false;
  }

	public function submittedReviews()
	{
		return $this->hasMany('App\UserReviews', 'user_id');
	}

	public function getAvtarPathAttribute()
	{
		if($this->avtar_id == null)
		{
			return asset('public/uploads/user_avtars/default.png');
		}
		else
		{
			return asset('public/uploads/user_avtars/'.$this->userAvtar->filename);
		}
	}

	public function getRatingAttribute()
	{
		$rating = $this->recievedReviews->where('is_approved', 2)->avg('ratings');
		return ($rating != null)?number_format($rating, 1, '.', ''):'0.0';
	}

  public function getJWTIdentifier()
	{
	   return $this->getKey();
	}

	public function getJWTCustomClaims()
	{
	  return [];
  }
  
  public function sendPasswordResetNotification($token)
  {
    \Mail::to($this->email)->send(new \App\Mail\ForgetPasswordMail($this->email, $token));
  }
}
