<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdminPermissionGroups extends Model
{
  use SoftDeletes;
  protected $guarded = [];

  public function user()
  {
  	return $this->hasMany('App\User', 'permission_id');
  }
}
