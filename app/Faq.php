<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
  use SoftDeletes;
  protected $table = 'faq';
  protected $guarded = [];
}