<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserReviews extends Model
{
	use SoftDeletes;
  protected $guarded = [];
  protected $appends = ['is_approved_lbl'];

  public function order()
  {
  	return $this->belongsTo('App\Orders', 'order_id');
  }

  public function sender()
  {
  	return $this->belongsTo('App\User', 'user_id');
  }

  public function receiver()
  {
  	return $this->belongsTo('App\User', 'receiver_id');
  }

  public function getIsApprovedLblAttribute()
  {
  	if($this->is_approved == config('global.REVIEW_STATUS.PENDING'))
  	{
  		return __('labels.in_review');
  	}
  	else if($this->is_approved == config('global.REVIEW_STATUS.APPROVED'))
  	{
  		return __('labels.published');
  	}
  	else if($this->is_approved == config('global.REVIEW_STATUS.REJECTED'))
  	{
  		return trans('labels.rejected');
    }
  }
}