<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ProductSubCategories extends Model
{
	use SoftDeletes;
  protected $guarded = [];

  public function productCategories()
  {
  	return $this->belongsTo('App\ProductCategories', 'category_id');
  }

  public function products()
  {
  	return $this->hasMany('App\Products', 'subcategory_id', 'id')->where(['is_active' => config('global.ACTIVE')]);
  }
}
