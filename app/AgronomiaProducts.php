<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AgronomiaProducts extends Model
{
	use SoftDeletes;
  protected $guarded = [];

  public function products()
  {
  	return $this->belongsTo('App\Products', 'product_id')->where(['is_active' => config('global.ACTIVE')]);
  }

  public function agronomia()
  {
    return $this->belongsTo('App\User', 'agronomia_id')->where(['is_active' => config('global.ACTIVE')]);
  }
}
