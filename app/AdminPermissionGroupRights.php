<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdminPermissionGroupRights extends Model
{
  use SoftDeletes;
  protected $guarded = [];

  public function permission_group()
	{
		return $this->belongsTo('App\AdminPermissionGroups', 'group_id');
  }

  public function menu()
  {
  	return $this->belongsTo('App\Menubar', 'menu_id');
  }
}
