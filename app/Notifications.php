<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
	use SoftDeletes;
  protected $guarded = [];
  protected $appends = ['message_type', 'message_text'];

  public function user()
  {
  	return $this->belongsTo('App\User', 'user_id');
  }

  public function admin_message()
  {

    if($this->type == config('global.NOTIFICATION.ADMIN_MESSAGE'))
    {
      return $this->belongsTo('App\AdminMessages', 'admin_message_id');
    }
    return '';
  }
  
  public function getMessageTypeAttribute()
  {
  	if($this->type == config('global.NOTIFICATION.ADMIN_MESSAGE'))
  	{
  		return 'info';
  	}
  	else if($this->type == config('global.NOTIFICATION.ORDER_PLACED'))
  	{
  		return 'info';
  	}
  	else if($this->type == config('global.NOTIFICATION.ORDER_RECEIVED'))
  	{
  		return 'info';
  	}
  	else if($this->type == config('global.NOTIFICATION.ORDER_APPROVED'))
  	{
  		return 'success';
  	}
  	else if($this->type == config('global.NOTIFICATION.ORDER_CANCELLED'))
  	{
  		return 'danger';
  	}
  }

  public function order()
  {
    if($this->type == config('global.NOTIFICATION.ORDER_PLACED') || $this->type == config('global.NOTIFICATION.ORDER_RECEIVED') ||
       $this->type == config('global.NOTIFICATION.ORDER_APPROVED') || $this->type == config('global.NOTIFICATION.ORDER_CANCELLED'))
    {
      return $this->belongsTo('App\Orders', 'message');
    }
    return '';
  }

  public function getMessageTextAttribute()
  {
     //ORDER PLACE MESSAGE
  	if($this->type == config('global.NOTIFICATION.ORDER_PLACED'))
  	{
      if($this->user->user_type == config('global.CUSTOMER_USER')) //IF PRODUCER
      {
        return str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.producer.order_placed'));
      }
      elseif($this->user->user_type == config('global.ADMIN_USER')) //IF ADMIN
      {
        $message = str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.admin.order_placed'));
        return str_replace(':producer_id', $this->order->producer->custom_id, $message);
      }
    }
    // ORDER RECEIVED FOR AGRONOMIA
  	else if($this->type == config('global.NOTIFICATION.ORDER_RECEIVED'))
  	{
      return str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.agronomia.order_received'));
    }
    // ORDER APPROVED
  	else if($this->type == config('global.NOTIFICATION.ORDER_APPROVED'))
  	{
      if($this->user->user_type == config('global.BUSINESS_USER')) //IF AGRONOMIA
      {
        return str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.agronomia.order_approved'));
      }
      else if($this->user->user_type == config('global.CUSTOMER_USER')) //IF PRODUCER
      {
        return str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.producer.order_approved'));
      }
      elseif($this->user->user_type == config('global.ADMIN_USER')) //IF ADMIN
      {
        $message = str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.admin.order_approved'));
        return str_replace(':agronomia_id', $this->order->agronomia->custom_id, $message);
      }
    }
    // ORDER REJECTED
  	else if($this->type == config('global.NOTIFICATION.ORDER_CANCELLED'))
  	{
      if($this->user->user_type == config('global.BUSINESS_USER')) //IF AGRONOMIA
      {
        return str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.agronomia.order_rejected'));
      }
      else if($this->user->user_type == config('global.CUSTOMER_USER')) //IF PRODUCER
      {
        return str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.producer.order_rejected'));
      }
      elseif($this->user->user_type == config('global.ADMIN_USER')) //IF ADMIN
      {
        $message = str_replace(':order_no', $this->order->custom_order_no, __('labels.notification_messages.admin.order_rejected'));
        return str_replace(':agronomia_id', $this->order->agronomia->custom_id, $message);
      }
  	}
    else if($this->type == config('global.NOTIFICATION.ADMIN_MESSAGE') && $this->user->user_type == config('global.ADMIN_USER'))
    {
      if($this->admin_message->type == config('global.ADMIN_MESSAGE.AGRONOMIA')) //IF SENT TO AGRNOMIA
      {
        return str_replace(':message', $this->message, __('labels.notification_messages.admin.agronomia_message'));
      }
      else if($this->admin_message->type == config('global.ADMIN_MESSAGE.PRODUCER')) //IF SENT TO PRODUCER
      {
        return str_replace(':message', $this->message, __('labels.notification_messages.admin.producer_message'));
      }
      else if($this->admin_message->type == config('global.ADMIN_MESSAGE.ALL_USERS')) //IF SENE TO ALL USERS
      {
        return str_replace(':message', $this->message, __('labels.notification_messages.admin.all_users_message'));
      }
      else if($this->admin_message->type == config('global.ADMIN_MESSAGE.SPECIFIC_USER')) //IF SENT TO SPECIFIC USER
      {
        $message = str_replace(':message', $this->message, __('labels.notification_messages.admin.specific_user_message'));
        return str_replace(':user_id', $this->admin_message->user->custom_id, $message);
      }
    }
  	else
  	{
  		return $this->message;
  	}
  }
}
