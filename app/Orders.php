<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
  use SoftDeletes;
  protected $guarded = [];
  protected $appends = ['order_status_label'];

  public function product()
  {
  	return $this->belongsTo('App\Products', 'product_id');
  }

  public function agronomia()
  {
  	return $this->belongsTo('App\User', 'agronomia_id');
  }

  public function producer()
  {
  	return $this->belongsTo('App\User', 'producer_id');
  }

  public function paymentMethod()
  {
  	return $this->belongsTo('App\PaymentMethods', 'payment_method_id');
  }

  public function delivery_location()
  {
  	return $this->belongsTo('App\UserGeolocation', 'producer_location_id');
  }

  public function getOrderStatusLabelAttribute()
  {
  	if($this->order_status == config('global.PENDING_ORDER_STATUS'))
  	{
  		return trans('labels.pending');
  	}
  	else if($this->order_status == config('global.APPROVED_ORDER_STATUS'))
  	{
  		return trans('labels.approved');
  	}
  	else if($this->order_status == config('global.CANCELLED_ORDER_STATUS'))
  	{
  		return trans('labels.cancelled');
  	}
  }
}
