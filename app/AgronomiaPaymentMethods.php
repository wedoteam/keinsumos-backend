<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AgronomiaPaymentMethods extends Model
{
  use SoftDeletes;
  protected $guarded = [];
  public 		$timestamps = true;

  public function agronomia()
  {
    return $this->belongsTo('App\User', 'agronomia_id')->where(['is_active' => config('global.ACTIVE')]);
  }

  public function payment_method()
  {
    return $this->belongsTo('App\PaymentMethods', 'payment_method_id')->where(['is_active' => config('global.ACTIVE')]);
  }
}