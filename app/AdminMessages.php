<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdminMessages extends Model
{
	use SoftDeletes;
  protected $guarded = [];

  public function user()
  {
    if($this->type == config('global.ADMIN_MESSAGE.SPECIFIC_USER'))
    {
      return $this->belongsTo('App\User', 'user_id');
    }
    return '';
  }
}
