<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
  use SoftDeletes;
  protected $table   = 'provinces';
  protected $guarded = [];
}
