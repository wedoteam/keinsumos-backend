<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Versions;
use App\User;
use App\CustomPages;
use App\Faq;
use Session;
use Artisan;
use DB;
use Mail;

class AppController extends Controller
{
  /**
   * Homepage
   */
  public function homepage()
  {
    return view('homepage');
  }

  /**
   * About Us
   */
  public function aboutUs()
  {
    return view('about_us');
  }

  /**
   * Our Solutions
   */
  public function ourSolution()
  {
    return view('our_solution');
  }

  /**
   * For Agronomia
   */
  public function forAgronomia()
  {
    return view('for_agronomia');
  }

  /**
   * For Producer
   */
  public function forProducer()
  {
    return view('for_producer');
  }

  /**
   * FAQ
   */
  public function faq()
  {
    $faq = Faq::orderBy('order_priority', 'ASC')->where('is_active', '=', config('global.ACTIVE'))->get();
    return view('faq', ['faq' => $faq, 'active_page' => 'faq']);
  }

  /**
   * Contact Us
   */
  public function contact()
  {
    return view('contact');
  }

  /**
   * Submit Contact Form
   */
  public function submitContact(Request $request)
  {
    //VALIDATIONS
    $validator = Validator::make($request->all(), [
      'name'      => 'required',
      'email'     => 'required|email',
      'phone'     => 'required',
      'message'   => 'required',
      'g-recaptcha-response' => 'required',
    ]);

    //IF VALIDATION FAILS
    if ($validator->fails()) {
      return redirect('contacto')->withInput($request->all())->withErrors([$validator->messages()->first()]);
    }

    /* SEND MAIL TO RECEIVER */
    \Mail::to(env('CONTACT_FORM_MAIL'))
    ->cc(env('CONTACT_FORM_MAIL_CC'))
    ->send(new \App\Mail\ContactForm($request));
    return redirect('contacto')->with('success', __('api.your_message_received_our_advisor_will_contact_you_sortly'));
  }

  /**
   * Landing Page
   */
  public function landingPage()
  {
    return view('landing_page');
  }

  /**
   * Landing Page 2
   */
  public function landingPage2()
  {
    return view('landing_page_2');
  }

  /**
   * Init Application
   */
  public function init()
  {
    $user     = Auth::user();
    $vue_base = url($user->user_type_label);
    $vue_base = str_replace('https://', '', $vue_base);
    $vue_base = str_replace('http://', '', $vue_base);
    $vue_base = str_replace($_SERVER['SERVER_NAME'].'/', '', $vue_base);
    $vue_base = str_replace($_SERVER['SERVER_NAME'], '', $vue_base);

    $page_path = '/'.$user->user_type_label;

    return view('app.index', ['user' => $user, 'vue_base' => $vue_base, 'page_path' => $page_path]);
  }

  /**
   * Init Admin Panel
   */
  public function adminInit()
  {
    $vue_base = (env('APP_ENV') == 'local') ? url('admin') : env('MIX_ADMIN_DOMAIN');
    $vue_base = str_replace('https://', '', $vue_base);
    $vue_base = str_replace('http://', '', $vue_base);
    $vue_base = str_replace($_SERVER['SERVER_NAME'].'/', '', $vue_base);
    $vue_base = str_replace($_SERVER['SERVER_NAME'], '', $vue_base);

    $page_path = (env('APP_ENV') == 'local') ? '/admin' : '';

    return view('admin.index', ['vue_base' => $vue_base, 'page_path' => $page_path]);
  }

  /**
   * Register Producer
   */
  public function registerProducer(Request $request)
  {
    return view('auth.register.producer', ['active_page' => 'register']);
  }

  /**
   * Register Agronomia
   */
  public function registerAgronomia(Request $request)
  {
    return view('auth.register.agronomia', ['active_page' => 'register']);
  }

  /**
   * Init Register Success
   */
  public function registerSuccess($user)
  {
    return view('auth.registeration_success', ['user' => $user]);
  }

  /**
   * Terms & Conditions
   */
  public function termsAndConditions($user)
  {
    $page = CustomPages::where('name', $user.'_terms_and_conditions')->first();
    return view('terms_and_conditions', ['content' => $page->content, 'user' => $user]);
  }

  /**
   * Auth Check
   */
  public function auth()
  {
    if(Auth::check()) 
    {
      $user = Auth::user();
      
      // IF ACTIVE AGRONOMIA REDIRECT IT TO AGRONOMIA PANEL
      if($user->user_type == config('global.BUSINESS_USER') && $user->is_active = config('global.ACTIVE'))
      {
        if($user->last_login_at == '')
        {
          return redirect('agronomia/mis-productos');
          exit;
        }
        return redirect('agronomia');
      }
      // IF ACTIVE PRODUCER REDIRECT IT TO PRODUCER PANEL
      else if($user->user_type == config('global.CUSTOMER_USER') && $user->is_active = config('global.ACTIVE'))
      {
        return redirect('producer');
      }
      Auth::logout();
    }
    //ELSE LOGOUT SESSION AND REDIRECT TO LOGIN PAGE
    Auth::logout();
    return redirect('login')->withErrors([__('labels.invalid_access')]);
  }

  /**
   * Logout
   */
  public function logout()
  {
    Auth::logout();
    return redirect('');
  }

  /**
   * Release
   */
  public function versions()
  {
    $web = Versions::where(['type' => 'web'])->orderBy('id', 'desc')->get();
    $android = Versions::where(['type' => 'android'])->orderBy('id', 'desc')->get();
    $ios = Versions::where(['type' => 'ios'])->orderBy('id', 'desc')->get();
    return view('versions', ['web' => $web, 'android' => $android, 'ios' => $ios]);
  }

  /**
   * Migrate
   */
  public function migrate()
  {
    Artisan::call('migrate');
  }

  /**
   * php Info
   */
  public function phpinfo()
  {
    Artisan::call('cache:clear');
    return phpinfo();
  }
}
