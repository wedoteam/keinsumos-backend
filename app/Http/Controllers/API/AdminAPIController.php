<?php
namespace App\Http\Controllers\API;

use DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProductsExport;
use App\Exports\BillingExport;

use App\Events\OrderChat\MessageRead;
use App\Events\OrderChat\MessageSent;
use App\Jobs\SendAdminMessageMailJob;
use App\Rules\AlphaSpace;

use App\AdminPermissionGroups;
use App\AgronomiaProducts;
use App\AgronomiaPaymentMethods;
use App\ProductCategories;
use App\PaymentMethods;
use App\Notifications;
use App\AdminMessages;
use App\UserReviews;
use App\CustomPages;
use App\UserAvtars;
use App\Products;
use App\Messages;
use App\Orders;
use App\User;
use App\Faq;
use App\ProductBands;
use App\ProductDiameters;
use App\ProductLengths;
use App\ProductoGenerals;
use App\ProductSubCategories;
use App\ProductTreatments;
use App\UserGeolocation;
use Storage;
use Hash;
use Auth;
use Mail;

class AdminAPIController extends BaseController
{
  /* START OF LOGIN FUNCTION */
  public function login(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email'    => 'required|email',
      'password' => 'required',
    ]);

    // REDIRECT IS CLIENT SIDE VALIDATION FAILED
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
      return Redirect::back()->withErrors($validator);
    }

    $user = User::where('email', $request->email)->get()->first();

    if(!empty($user) && $user->count() > 0) //IF EMAIL MATCHES
    { 
      if($user->user_type == config('global.ADMIN_USER')) //CHECK IF ADMIN
      {
        if($user->is_active == config('global.ACTIVE')) //IF ACTIVE
        {
          $credentials = request(['email', 'password']);

          if(auth()->attempt($credentials)) //IF AUTHENTICATE
          {
            return response()->json([
              'code'    => 200,
              'message' => trans('login_messages.successfully_logged_in'),
              'user'    => Auth::user()
            ]);
          }
          else
          {
            return response()->json([
              'code'    => 404,
              'message' => trans('login_messages.please_enter_valid_password'),
            ]);
          }
        }
        else
        {
          return response()->json([
            'code'    => 404,
            'message' => trans('api.the_user_is_in_approval_process'),
          ]);
        }
      }
      else
      {
        return response()->json([
          'code'    => 404,
          'message' => trans('login_messages.unauthorized_access'),
        ]);
      }
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.account_not_found'),
      ]);
    }
  }
  /* END OF LOGIN FUNCTION */

  /* START OF APP USERS LIST */
  public function appUsers(Request $request)
  {
    $search  = ($request->search)?$request->search:false;
    $length  = ($request->length && $request->start != '')?$request->length:false;
    $start   = ($length && $request->start && $request->start != '')?$request->start:false;
    $columns = $request->columns;
    $order   = $request->order;
    if(empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }
    $business_name = ($request->business_name && $request->business_name != '')?$request->business_name:false;
    $searchTerm    = (isset($search['value']) && $search['value'] != "")?$search['value']:false;

    $query      = User::where(function($query){
                          $query->where('user_type', '=', config('global.BUSINESS_USER'))
                                ->orWhere('user_type', '=', config('global.CUSTOMER_USER'));
                        })
                      ->when($business_name, function($query) use ($business_name){
                          return $query->where('users.business_name', 'like', '%'.$business_name.'%');
                        })
                      ->when($searchTerm, function($query) use ($searchTerm){
                        return $query->where(function($sub_query) use ($searchTerm){
                            $sub_query->orWhere(DB::raw("DATE_FORMAT(users.created_at, '%d-%m-%Y %H:%i:%s')"), 'like', '%'.$searchTerm.'%')
                            ->orWhere('users.custom_id', 'like', '%'.$searchTerm.'%')
                            ->orWhere(DB::raw("CASE WHEN users.user_type = '".config('global.BUSINESS_USER')."' THEN '".__('labels.agronomia')."' WHEN users.user_type = '".config('global.CUSTOMER_USER')."' THEN '".__('labels.producer')."' ELSE '' END"), 'like', '%'.$searchTerm.'%');
                          });
                        });

    $totalData  = $query->count();
    
    $results    = $query->when($order, function($q) use ($order, $columns){
                            return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                          })
                        ->when($start, function($q) use ($start){
                            return $q->skip($start);
                          })
                        ->when($length, function($q) use ($length){
                          return $q->take($length);
                          })
                        ->get(['users.*']);

    $data = array ();

    foreach ($results as $result) 
    {
      $row = [];
      $row['id']              = $result->id;
      $row['custom_id']       = $result->custom_id;
      $row['email']           = $result->email;
      $row['business_name']   = $result->business_name;
      $row['user_type_label'] = ($result->user_type == config('global.BUSINESS_USER'))?__('labels.agronomia'):__('labels.producer');
      $row['created_at']      = YMDtoDMY($result->created_at);
      $row['is_active']       = (($result->is_active == config('global.ACTIVE'))?true:false);

      $data [] = $row; 
    }

    $tableContent = array (
      "draw" => intval ( $request->input ( 'draw' ) ), 
      "recordsTotal" => intval ( $totalData ),
      "recordsFiltered" => intval ( $totalData ),
      "data" => $data
    );
    return $tableContent;
  }
  /* END OF APP USERS LIST */

  /* STORE APP USER */
  public function storeAppUser(Request $request)
  {
    $user = User::find($request->id);

    if(empty($user) || $user->count() == 0)
    {
      return response()->json([
        'code'    => 404,
        'message' => __('api.invalid_id_field'),
      ]);
    }

    $initialStatus = $user->is_active;
      
    if($user->user_type == config('global.BUSINESS_USER'))
    {
      $step_2_condition = ($user->step_2) ? 'required' : 'nullable';
      $validator = Validator::make($request->all(), [
        'is_active'     => ['required', 'in:1,0'], // 1 => active, 0 => disabled
        'business_name' => ['required', 'string', 'max:128'],
        'firstname'     => ['required', 'string', 'max:128', new AlphaSpace],
        'surname'       => ['required', 'string', 'max:128', new AlphaSpace],
        'cuit'          => ['required', 'string', 'max:128'],
        'province'      => ['required', 'exists:provinces,id,deleted_at,NULL'],
        'locality'      => [
          'required',
          'exists:localities,id,deleted_at,NULL,province_id,'.$request->province
        ],
        'latitude'         => [$step_2_condition, 'numeric', 'min:-90', 'max:90'],
        'longtitude'       => [$step_2_condition, 'numeric', 'min:-180', 'max:180'],
        'avtar_id'         => [
          $step_2_condition,
          'exists:user_avtars,id,deleted_at,NULL'
        ],
        'payment_methods'  => [$step_2_condition, 'present'],
        'payment_methods.*'=> [
          'required',
          'exists:payment_methods,id,is_active,'.config('global.ACTIVE').',deleted_at,NULL'
        ],
        'mobile_prefix' => [
          'required', 'string',
          'regex:/^[0-9]{1,4}$/',
        ],
        'mobile'        => [
          'required', 'string',
          'regex:/^[0-9]{1,8}$/',
          'unique:users,mobile,'.$request->id.',id',
        ],
        'email'         => [
          'required', 'email', 'max:255',
          'unique:users,email,'.$request->id.',id'
        ],
        'password'      => ['nullable', 'min:8', 'max:32'],
      ]);

      if ($validator->fails()) {
        return response()->json([
          'code'    => 404,
          'message' => $validator->messages()->first(),
        ]);
      }
    }
    elseif($user->user_type == config('global.CUSTOMER_USER'))
    {
      $step_2_condition = ($user->step_2) ? 'required' : 'nullable';
      $validator = Validator::make($request->all(), [
        'is_active'     => [ 'required', 'in:1,0'], // 1 => active, 0 => disabled
        'business_name' => [ 'required', 'string', 'max:128'],
        'firstname'     => [ 
          'required_if:verified,1',
          $step_2_condition,  'string', 'max:128', new AlphaSpace
        ],
        'surname'       => [ 
          'required_if:verified,1',
          $step_2_condition, 'string', 'max:128', new AlphaSpace
        ],
        'cuit'          => ['required', 'string', 'max:128'],
        'province'      => [
          'required_if:verified,1',
          $step_2_condition, 'exists:provinces,id,deleted_at,NULL'],
        'locality'      => [
          'required_if:verified,1',
          $step_2_condition,
          'exists:localities,id,deleted_at,NULL,province_id,'.$request->province
        ],
        'renspa_no'     => [
          'required_if:verified,1',
          'nullable', 'max:128',
          'regex:/^[0-9]{2}[.][0-9]{3}[.][0-9]{1}[.][0-9]{1,}$/'
        ],
        'verified'                 => ['required', 'in:1,0'], // 1 => yes, 0 => no
        'geolocation'              => [
          'required_if:verified,1',
          $step_2_condition, 'present'
        ],
        'geolocation.*.id'         => [
          'nullable', 
          'exists:user_geolocation,id,user_id,'.$user->id.',deleted_at,NULL'
        ],
        'geolocation.*.name'       => [
          'required', 'max:128'
        ],
        'geolocation.*.latitude'   => ['required', 'numeric', 'min:-90', 'max:90'],
        'geolocation.*.longtitude' => ['required', 'numeric', 'min:-180', 'max:180'],
        'mobile_prefix' => [
          'required_if:verified,1', $step_2_condition, 'string',
          'regex:/^[0-9]{1,4}$/',
        ],
        'mobile'        => [
          'required_if:verified,1', $step_2_condition, 'string',
          'regex:/^[0-9]{1,8}$/',
          'unique:users,mobile,'.$request->id.',id'
        ],
        'email'         => [
          'required', 'email', 'max:255',
          'unique:users,email,'.$request->id.',id'
        ],
        'password'                 => ['nullable', 'min:8', 'max:32'],
      ]);
      
      if ($validator->fails()) {
        return response()->json([
          'code'    => 404,
          'message' => $validator->messages()->first(),
        ]);
      }
    }
      
    $data = [
      'business_name' => $request->business_name,  
      'firstname'     => $request->firstname,  
      'surname'       => $request->surname,  
      'name'          => $request->firstname.' '.$request->surname,
      'cuit'          => $request->cuit,   
      'province'      => $request->province,
      'locality'      => $request->locality,
      'mobile_prefix' => $request->mobile_prefix,
      'mobile'        => $request->mobile,
      'email'         => $request->email,
      'worst_credit_bureau'          => $request->worst_credit_bureau,
      'user_score'                   => $request->user_score,
      'bad_cheques'                  => $request->bad_cheques,
      'unpaid_employer_contribution' => $request->unpaid_employer_contribution,
      'vat_condition'                => $request->vat_condition,
      'is_active'                    => $request->is_active, 
    ];

    // if producer 
    if($user->user_type == config('global.CUSTOMER_USER'))
    {
      $data['renspa_no'] = $request->renspa_no;
      $data['verified']  = $request->verified;
      if($request->firstname != '' && $request->surname != ''
          && $request->province != '' && $request->locality != ''
          && $request->mobile_prefix != '' && $request->mobile != ''
          && is_array($request->geolocation) && count($request->geolocation)  > 0)
      {
        $data['step_2']  = 1;
      }
    }

    // if agronomia
    if($user->user_type == config('global.BUSINESS_USER'))
    {
      $data['latitude']   = $request->latitude;
      $data['longtitude'] = $request->longtitude;
      $data['avtar_id']   = $request->avtar_id;
      if($request->latitude != '' && $request->longtitude != ''
          && is_array($request->payment_methods) && count($request->payment_methods)  > 0)
      {
        $data['step_2']   = 1;
        $data['verified'] = 1;
      }
    }

    // password
    if($request->password != '')
    {
      $data['password'] = Hash::make($request->password);
    }
    
    //update user
    $user->update($data);

    // get new user
    $user = User::find($request->id);

    // if agronomia
    if($user->user_type == config('global.BUSINESS_USER'))
    {
      //DELETE ALL PAYMENT METHODS
      AgronomiaPaymentMethods::where(['agronomia_id' => $user->id])->delete();

      //IF PAYMENT METHODS EXIST
      if(is_array($request->payment_methods) && !empty($request->payment_methods))
      {
        $i = 0;
        $paymentMethods = array_unique($request->payment_methods);
        foreach($paymentMethods as $paymentMethod)
        {
          $paymentMethodData[$i]['agronomia_id']      = $user->id;
          $paymentMethodData[$i]['payment_method_id'] = $paymentMethod;
          $paymentMethodData[$i]['created_at'] = date('Y-m-d H:i:s');
          $paymentMethodData[$i]['updated_at'] = date('Y-m-d H:i:s');
          $i++;
        }
        AgronomiaPaymentMethods::insert($paymentMethodData);
      }
    }

    // if producer
    if($user->user_type == config('global.CUSTOMER_USER'))
    {
      $geolocations = $request->geolocation;
      foreach($geolocations as $geolocation)
      {
        $single_location['user_id']    = $user->id;
        $single_location['name']       = $geolocation['name'];
        $single_location['latitude']   = $geolocation['latitude'];
        $single_location['longtitude'] = $geolocation['longtitude'];
        if($geolocation['id'])
        {
          UserGeolocation::where('id', '=', $geolocation['id'])
                         ->update($single_location);
        }
        else
        {
          UserGeolocation::insert($single_location);
        }
      }
    }
    
    // if newly activated, send mail
    if($initialStatus == 0 && $user->is_active == 1)
    {
      \Mail::to($user->email)->send(new \App\Mail\UserActivated($user));

      $this->sendFcmPushNotification(
        [ $user->toArray() ],
        [ 'message'  => trans('labels.user_activated') ],
        [
          'type'    => 'user',
        ]);
    }

    return response()->json([
      'code'    => 200,
      'message' => __('labels.user_successfully_updated'),
    ]);
  }
  /* END OF STORE APP USER */

  /* CHANGE APP USER STATE */
  public function changeAppUserState(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'    => 'required|exists:users,id,deleted_at,NULL',
      'state' => 'required',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $user = User::find($request->id);
    
    if($user->user_type == config('global.BUSINESS_USER') || $user->user_type == config('global.CUSTOMER_USER'))
    {
      $state = ($request->state == true)?config("global.ACTIVE"):config("global.DISABLED");

      User::where('id', $request->id)->update(['is_active' => $state]);
      if($state)
      {
        \Mail::to($user->email)->send(new \App\Mail\UserActivated($user));

        $this->sendFcmPushNotification(
          [ $user->toArray() ],
          [ 'message'  => trans('labels.user_activated') ],
          [
            'type'    => 'user',
          ]);
      }
      return response()->json([
        'code'    => 200,
        'message' => ($state == config("global.ACTIVE"))?__('labels.user_activated'):__('labels.user_disabled'),
      ]);
    }

    return response()->json([
      'code'    => 401,
      'message' => __('labels.permission_denied'),
    ]);
  }
  /* END OF CHANGE APP USER STATE */

  /* START OF SEARCH USER */
  public function searchAppUser(Request $request)
  {
    $custom_id = ($request->custom_id && $request->custom_id != '')?$request->custom_id:false;
    $users = User::when($custom_id, function($query) use ($custom_id){
                    return $query->where('custom_id', 'like', '%'.$custom_id.'%');
                   })
                 ->whereIn('user_type', [config('global.BUSINESS_USER'), config('global.CUSTOMER_USER')])
                 ->get(['id', DB::raw('custom_id as name')]);
    return response()->json($users);
  }
  /* END OF SEARCH USER */

  /* START OF AGRONOMIA PRODUCT LIST */
  public function agronomiaProducts(Request $request)
  {
    $search = ($request->search)?$request->search:false;
    $length = ($request->length && $request->start != '')?$request->length:false;
    $start  = ($length && $request->start && $request->start != '')?$request->start:false;
    $order  = $request->order;
    $columns = $request->columns;
    if(empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }

    $i = 0;

    $agronomia_id   = $request->agronomia_id;
    $category_id    = ($request->category_id && $request->category_id != '')?$request->category_id:false;
    $subcategory_id = ($request->subcategory_id && $request->subcategory_id != '')?$request->subcategory_id:false;
    $searchTerm     = (isset($search['value']) && $search['value'] != "")?$search['value']:false;

    $query          =  AgronomiaProducts::join('products', 'products.id', '=', 'agronomia_products.product_id')
                                        ->join('product_sub_categories', 'product_sub_categories.id', '=', 'products.subcategory_id')
                                        ->join('product_categories', 'product_categories.id', '=', 'product_sub_categories.category_id')
                                        ->where(['agronomia_products.agronomia_id' => $agronomia_id])
                                        ->where(['products.deleted_at' => null, 'products.is_active' => config('global.ACTIVE')])
                                        ->where(['product_sub_categories.deleted_at' => null, 'product_sub_categories.is_active' => config('global.ACTIVE')])
                                        ->where(['product_categories.deleted_at' => null, 'product_categories.is_active' => config('global.ACTIVE')])
                                        ->when($category_id, function($query, $category_id){
                                          return $query->where('product_categories.id', $category_id);
                                        })
                                        ->when($subcategory_id, function($query, $subcategory_id){
                                          return $query->where('product_sub_categories.id', $subcategory_id);
                                        })
                                        ->when($searchTerm, function($q) use ($searchTerm){
                                          return $q->where(function($sub_q) use ($searchTerm) {
                                            $sub_q->where('products.name', 'like', '%'.$searchTerm.'%')
                                                  ->orWhere('agronomia_products.available_quanity', 'like', '%'.$searchTerm.'%')
                                                  ->orWhere('agronomia_products.agronomia_price', 'like', '%'.$searchTerm.'%');
                                          });
                                        });

    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                            return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                          })
                          ->when($start, function($q) use ($start){
                            return $q->skip($start);
                          })
                          ->when($length, function($q) use ($length){
                            return $q->take($length);
                          })
                          ->get(['agronomia_products.id', 'agronomia_products.product_id', 'products.name', 'products.description', 'products.image', 'agronomia_products.agronomia_price', 'agronomia_products.available_quanity', 'agronomia_products.is_active']);

    $data = [];

    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['id']                = $result->id;
        $row['product_id']        = $result->product_id;
        $row['name']              = $result->name;
        $row['description']       = $result->description;
        $row['image']             = asset('storage/app/public/uploads/products/'.$result->image);
        $row['agronomia_price']   = number_format($result->agronomia_price, 2, ',', '') . ' USD';
        $row['available_quanity'] = (string)$result->available_quanity;
        $row['is_active']         = $result->is_active;

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.products_fetched_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_products_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'    => [],
      ]);
    }
  }
  /* END OF AGRONOMIA PRODUCT LIST */

  /* START OF AGRONOMIA PRODUCT DETAIL */
  public function agronomiaProductDetail(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:agronomia_products,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $agronomia_product = AgronomiaProducts::find($request->id);

    return response()->json([
        'code'    => 200,
        'message' => trans('api.product_fetched_successfully'),
        'data'    => [
                      'id'                  => $agronomia_product->id,
                      'product_id'          => $agronomia_product->product_id,
                      'name'                => $agronomia_product->products->name,
                      'description'         => $agronomia_product->products->description,
                      'image'               => asset('public/uploads/products/'.$agronomia_product->products->image),
                      'admin_price'         => (string)$agronomia_product->products->price,
                      'unit_of_measure'     => $agronomia_product->products->unit_of_measure,
                      'ingredients'         => $agronomia_product->products->ingredients,
                      'available_quanity'   => (string)$agronomia_product->available_quanity,
                      'sales_range_in_km'   => (string)$agronomia_product->sales_range_in_km,
                      'sales_range_in_km'   => (string)$agronomia_product->sales_range_in_km,
                      'agronomia_price'     => (string)$agronomia_product->agronomia_price,
                      'bonus_applicable'    => $agronomia_product->bonus_applicable,
                      'bonus_price'         => ($agronomia_product->bonus_price)?(string)$agronomia_product->bonus_price:'',
                      'min_bonus_quanitity' => ($agronomia_product->bonus_price)?(string)$agronomia_product->min_bonus_quanitity:'',
                      'is_active'           => $agronomia_product->is_active,
                     ]
      ]);
  }
  /* END OF AGRONOMIA PRODUCT DETAIL */

  /* START OF ADD AGRONOMIA PRODUCT IN AGRONOMIA PRODUCTS */
  public function addAgronomiaProduct(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'agronomia_id'        => 'required|exists:users,id,user_type,'.config("global.BUSINESS_USER").',deleted_at,NULL',
      'product_id'          => 'required|exists:products,id,deleted_at,NULL',
      'available_quanity'   => 'required|numeric',
      'sales_range_in_km'   => 'required|numeric',
      'agronomia_price'     => 'required|numeric',
      'bonus_applicable'    => 'required|in:'.config('global.TRUE').','.config('global.FALSE'),
      'bonus_price'         => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable|max:'.$request->agronomia_price,
      'min_bonus_quanitity' => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable',
      'is_active'           => 'required|in:'.config('global.ACTIVE').','.config('global.DISABLED'),
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }

    //CHECK IF ALREADY EXIST
    if(AgronomiaProducts::where(['product_id' => $request->product_id, 'agronomia_id' => $request->agronomia_id])->count() > 0)
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.product_is_already_added_in_selling_list'),
      ]);
    }

    $data = array(
      'agronomia_id'        => $request->agronomia_id,
      'product_id'          => $request->product_id,
      'available_quanity'   => $request->available_quanity,
      'sales_range_in_km'   => $request->sales_range_in_km,
      'agronomia_price'     => $request->agronomia_price,
      'bonus_applicable'    => $request->bonus_applicable,
      'bonus_price'         => ($request->bonus_applicable == config('global.TRUE'))?$request->bonus_price:null,
      'min_bonus_quanitity' => ($request->bonus_applicable == config('global.TRUE'))?$request->min_bonus_quanitity:null,
      'is_active'           => $request->is_active,
    );

    AgronomiaProducts::create($data);

    return response()->json([
        'code'    => 200,
        'message' => trans('api.product_added_successfully'),
      ]);
  }
  /* END OF ADD AGRONOMIA PRODUCT IN AGRONOMIA PRODUCTS */

  /* START OF AGRONOMIA PRODUCT EDIT */
  public function editAgronomiaProduct(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'                  => 'required|exists:agronomia_products,id,deleted_at,NULL',
      'product_id'          => 'required|exists:products,id,deleted_at,NULL',
      'available_quanity'   => 'required|numeric',
      'sales_range_in_km'   => 'required|numeric',
      'agronomia_price'     => 'required|numeric',
      'bonus_applicable'    => 'required|in:'.config('global.TRUE').','.config('global.FALSE'),
      'bonus_price'         => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable|max:'.$request->agronomia_price,
      'min_bonus_quanitity' => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable',
      'is_active'           => 'required|in:'.config('global.ACTIVE').','.config('global.DISABLED'),
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }

    $agronomia_product = AgronomiaProducts::find($request->id);

    $data = array(
      'product_id'          => $request->product_id,
      'available_quanity'   => $request->available_quanity,
      'sales_range_in_km'   => $request->sales_range_in_km,
      'bonus_applicable'    => $request->bonus_applicable,
      'agronomia_price'     => $request->agronomia_price,
      'bonus_price'         => ($request->bonus_applicable == config('global.TRUE'))?$request->bonus_price:null,
      'min_bonus_quanitity' => ($request->bonus_applicable == config('global.TRUE'))?$request->min_bonus_quanitity:null,
      'is_active'           => $request->is_active,
    );

    $agronomia_product->update($data);

    return response()->json([
        'code'    => 200,
        'message' => trans('api.product_updated_successfully'),
      ]);
  }
  /* END OF AGRONOMIA PRODUCT EDIT */

  /* START OF APPLY INCREASE / DISCOUNT IN PRODUCTS */
  public function applyIncreaseDiscount(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'agronomia_product_ids' => 'required',
      'action_type'           => 'required|in:'.config('global.INCREASE_PRICE').','.config('global.APPLY_DISCOUNT'),
      'percentage'            => 'required|numeric|min:0|max:100',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    // FILTER AUTHORISED PRODUCTS
    $agronomia_product_ids = AgronomiaProducts::whereIn('id', $request->agronomia_product_ids)->pluck('id');

    //CHECK IF PRODUCTS AVAILABLE
    if(!empty($agronomia_product_ids) && $agronomia_product_ids->count() > 0)
    {
      if($request->action_type == config('global.INCREASE_PRICE'))
      {
        AgronomiaProducts::whereIn('id', $request->agronomia_product_ids)
                          ->update(['agronomia_price' => DB::raw('(agronomia_price + ((agronomia_price * '.$request->percentage.') / 100))'),
                                    'bonus_price'     => DB::raw('(bonus_price + ((bonus_price * '.$request->percentage.') / 100))')
                                  ]);
        return response()->json([
          'code'    => 200,
          'message' => trans('api.price_increased_successfully'),
        ]);
      }
      else if($request->action_type == config('global.APPLY_DISCOUNT'))
      {
        AgronomiaProducts::whereIn('id', $request->agronomia_product_ids)
                          ->update(['agronomia_price' => DB::raw('(agronomia_price - ((agronomia_price * '.$request->percentage.') / 100))'),
                                    'bonus_price'     => DB::raw('(bonus_price - ((bonus_price * '.$request->percentage.') / 100))')
                                  ]);
        return response()->json([
          'code'    => 200,
          'message' => trans('api.discount_applied_successfully'),
        ]);
      }
    }

    return response()->json([
        'code'    => 404,
        'message' => trans('api.no_products_selected_to_do_action')
      ]);
  }
  /* END OF APPLY INCREASE / DISCOUNT IN PRODUCTS */
  
  /* CUSTOM PAGE UPDATE START */
  public function customPage(Request $request)
  {
    $id = $request->id;
    $page = CustomPages::find($id);

    if(!empty($page) && $page->count() > 0)
    {
      $page->update(['content' => $request->content]);

      return response()->json(['code' => 200, 'message' => __('api.page_content_updated_successfully')]);
    }
    else
    {
      return response()->json(['code' => 400, 'message' => __('api.page_not_found')]);
    }
  }
  /* CUSTOM PAGE UPDATE END */

  /* START OF ADD FAQ */
  public function saveFAQ(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'question'      => 'required',
      'reply'          => 'required',
      'is_active'      => 'required',
      'order_priority' => 'required|numeric'
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $data = array(
      'question'       => $request->question,
      'reply'          => $request->reply,
      'is_active'      => ($request->is_active)?config('global.ACTIVE'):config('global.DISABLED'),  
      'order_priority' => $request->order_priority,
    );

    if($request->id == '')
    {
      Faq::create($data);
      return response()->json([
        'code'    => 200,
        'message' => trans('api.question_added_successfully'),
      ]);
    }
    else
    {
      Faq::where(['id' => $request->id])->update($data);
      return response()->json([
        'code'    => 200,
        'message' => trans('api.question_updated_successfully'),
      ]);
    }
  }
  /* END OF ADD FAQ */

  /* START OF FAQ STATUS CHANGE */
  public function changeFAQState(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'    => 'required|exists:faq,id,deleted_at,NULL',
      'state' => 'required',
    ]);

    $faq = Faq::find($request->id);
    
    $state = ($request->state == true)?config("global.ACTIVE"):config("global.DISABLED");
    $faq->update(['is_active' => $state]);
 
    return response()->json([
      'code'    => 200,
      'message' => ($state == config("global.ACTIVE"))?__('api.faq_activated'):__('api.faq_disabled'),
    ]);
  }
  /* END OF FAQ STATUS CHANGE */

  /* START OF SEARCH PRODUCT GENERAL BY NAME */
  public function searchProductGeneral(Request $request, $search = '') {
    if (!$search || strlen($search) < 3) {
      return response()->json([
        'code'    => 404,
        'message' => 'Invalid search term',
      ]);
    }

    $query = 'SELECT `producto_generals`.`id`, `producto_generals`.`name`
              FROM `producto_generals`
              WHERE `producto_generals`.`name` LIKE "%'.$search.'%"
              ORDER BY `producto_generals`.`name` ASC';
    $results = DB::select($query);
    return response()->json([
      'code'    => 200,
      'message' => trans('api.product_fetched_successfully'),
      'data'    => $results
    ]);
  }
  /* END OF SEARCH PRODUCT GENERAL BY NAME */

  /* START OF STORE PRODUCT */
  public function storeProduct(Request $request)
  {
    //CHECK IF REQUEST IS FOR UPDATING PRODUCT, BUT PRODUCT DOESN'T NOT FOUND.
    if($request->id && $request->id != '')
    {
      $product = Products::find($request->id);
      
      if($product->count() == 0)
      {
        return response()->json([
          'code'    => 404,
          'message' => trans('api.product_not_found'),
        ]);
      }
    }

    //VALIDATIONS
    $validator = Validator::make($request->all(), [
      'name'           => 'required|unique:products,name,'.$request->id.',id,deleted_at,NULL',
      'custom_id'      => 'required|unique:products,custom_id,'.$request->id.',id,deleted_at,NULL',
      'unit_of_measure'=> 'required',
      'description'    => 'required',
      'subcategory_id' => 'required|exists:product_sub_categories,id,deleted_at,NULL',
      'producto_general_id' => 'required|exists:producto_generals,id,deleted_at,NULL',
      // 'image'          => 'required_if:id,|image|nullable'
    ]);

    //IF VALIDATION FAILS
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }
    
    /*
    $image = str_replace(' ', '-', $request->name);
    $image = preg_replace('/[^A-Za-z0-9\-]/', '', $image).'_'.time().'.jpg';

    if($request->hasFile('image')) //UPLOAD IMAGE IF SELECTED
    {
      if($request->id != '' && $product->image != '') //DELETE IF UPDATING
      {
        Storage::disk('public_uploads')->delete('uploads/products/'.$product->image);
      }
      Storage::disk('public_uploads')->putFileAs('uploads/products/', $request->file('image'), $image);
    }
    else if($request->id != '' && $product->image != '' && Storage::disk('public_uploads')->exists('uploads/products/'.$product->image))
    {
      Storage::disk('public_uploads')->move('uploads/products/'.$product->image, 'uploads/products/'.$image);
    }
    */
    
    $data = array(
      'name'           => $request->name,
      'is_active'      => $request->state,
      'custom_id'      => $request->custom_id,
      'unit_of_measure'=> $request->unit_of_measure,
      'description'    => $request->description,
      'subcategory_id' => $request->subcategory_id,
      'producto_general_id' => $request->producto_general_id,
      // 'image'          => $image,
      'brand'          => ($request->brand) ? $request->brand : null,
      'band'           => ($request->band) ? $request->band : null,
      'treatments'     => ($request->treatments) ? $request->treatments : null,
      'ingredients'    => ($request->ingredients) ? $request->ingredients : null,
      'attribute_1'    => ($request->attribute_1) ? $request->attribute_1 : null,
      'grade'          => ($request->grade) ? $request->grade : null,
      'diameter'       => ($request->diameter) ? $request->diameter : null,
      'length'         => ($request->length) ? $request->length : null,
      'gasoline_grade' => ($request->gasoline_grade) ? $request->gasoline_grade : null,
      'attribute_2'    => ($request->attribute_2) ? $request->attribute_2 : null,
    );

    if($request->id == '')
    {
      Products::create($data);
      return response()->json([
        'code'    => 200,
        'message' => trans('api.product_added_successfully'),
      ]);
    }
    else
    {
      Products::where(['id' => $request->id])->update($data);
      return response()->json([
        'code'    => 200,
        'message' => trans('api.product_updated_successfully'),
      ]);
    }
  }
  /* END OF STORE PRODUCT */

  /* START OF PRODUCT STATUS CHANGE */
  public function changeProductState(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'    => 'required|exists:products,id,deleted_at,NULL',
      'state' => 'required',
    ]);

    $product = Products::find($request->id);
    
    $state = ($request->state == true)?config("global.ACTIVE"):config("global.DISABLED");
    $product->update(['is_active' => $state]);
 
    return response()->json([
      'code'    => 200,
      'message' => ($state == config("global.ACTIVE"))?__('api.product_activated'):__('api.product_disabled'),
    ]);
  }
  /* END OF PRODUCT STATUS CHANGE */

  /* START OF DELETE PRODUCT */
  public function deleteProduct(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'    => 'required|exists:products,id,deleted_at,NULL',
    ]);

    $product = Products::find($request->id)->delete();
 
    return response()->json([
      'code'    => 200,
      'message' =>__('api.product_deleted'),
    ]);
  }
  /* END OF DELETE PRODUCT */

  /* START OF REVIEWS LIST */
  public function reviews(Request $request)
  {
    $search  = ($request->search)?$request->search:false;
    $length  = ($request->length && $request->start != '')?$request->length:false;
    $start   = ($length && $request->start && $request->start != '')?$request->start:false;
    $columns = ($request->columns)?$request->columns:false;
    $order   = ($columns && $request->order)?$request->order:false;
    if($order && empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }
    $searchTerm  = (isset($search['value']) && $search['value'] != "")?$search['value']:false;
    $query       = UserReviews::join('orders', 'orders.id', '=', 'user_reviews.order_id')
                          ->join(DB::raw('users as sender'), 'sender.id', '=', 'user_reviews.user_id')
                          ->join(DB::raw('users as receiver'), 'receiver.id', '=', 'user_reviews.receiver_id')
                          ->when($searchTerm, function($q) use ($searchTerm){
                            return $q->where('orders.id', 'like', '%'.$searchTerm.'%')
                            ->orWhere('sender.business_name', 'like', '%'.$searchTerm.'%')
                            ->orWhere('receiver.business_name', 'like', '%'.$searchTerm.'%')
                            ->orWhere('user_reviews.comment', 'like', '%'.$searchTerm.'%');
                          });

    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                            return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                          })
                        ->when($start, function($q) use ($start){
                          return $q->skip($start);
                        })
                        ->when($length, function($q) use ($length){
                          return $q->take($length);
                        })
                        ->get(['user_reviews.*']);

    $data = [];
    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['id'] = $result->id;
        $row['order_id']          = $result->order_id;
        $row['custom_order_no']   = $result->order->custom_order_no;
        $row['user_id']           = $result->user_id;
        $row['sender_name']       = $result->sender->business_name;
        $row['sender_custom_id']  = $result->sender->custom_id;
        $row['receiver_id']       = $result->receiver_id;
        $row['receiver_name']     = $result->receiver->business_name;
        $row['receiver_custom_id']= $result->receiver->custom_id;
        $row['comment']           = $result->comment;
        $row['ratings']           = number_format($result->ratings, 1, ".", "");
        $row['is_approved']       = $result->is_approved;
        $row['is_approved_lbl']   = $result->is_approved_lbl;
        $row['created_at']        = YMDtoDMY($result->created_at);

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.reviews_retrived_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_reviews_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'    => [],
      ]);
    }
  }
  /* END OF REVIEWS LIST */

  /* START OF REVIEW STATUS CHANGE */
  public function changeReviewState(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'     => 'required|exists:user_reviews,id,deleted_at,NULL',
      'action' => 'required|in:delete,close,publish',
    ]);

    //IF VALIDATION FAILS
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $review = UserReviews::find($request->id);

    if($request->action == 'delete')
    {
      $review->delete();
      return response()->json([
        'code'    => 200,
        'message' => __('api.review_deleted'),
      ]);
    }
    else if($request->action == 'close')
    {
      $review->update(['is_approved' => config('global.REVIEW_STATUS.REJECTED')]);
      return response()->json([
        'code'    => 200,
        'message' => __('api.review_closed'),
      ]);
    }
    else if($request->action == 'publish')
    {
      $review->update(['is_approved' => config('global.REVIEW_STATUS.APPROVED')]);
      return response()->json([
        'code'    => 200,
        'message' => __('api.review_approved'),
      ]);
    }
  }
  /* END OF REVIEW STATUS CHANGE */

  /* START OF NOTIFICATIONS LIST */
  public function notifications(Request $request)
  {
    $search  = ($request->search)?$request->search:false;
    $length  = ($request->length && $request->start != '')?$request->length:false;
    $start   = ($length && $request->start && $request->start != '')?$request->start:false;
    $columns = ($request->columns)?$request->columns:false;
    $order   = ($columns && $request->order)?$request->order:false;
    if($order && empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }
    $searchTerm  = (isset($search['value']) && $search['value'] != "")?$search['value']:false;
    $query       = AdminMessages::when($searchTerm, function($q) use ($searchTerm){
                                    return $q->where('admin_messages.message', 'like', '%'.$searchTerm.'%');
                                  });

    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                            return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                          })
                        ->when($start, function($q) use ($start){
                          return $q->skip($start);
                        })
                        ->when($length, function($q) use ($length){
                          return $q->take($length);
                        })
                        ->get(['admin_messages.*']);

    $data = [];
    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['id']          = $result->id;
        $row['type']        = $result->type;
        $row['user_id']     = $result->user_id;
        $row['message']     = $result->message;
        $row['created_at']  = YMDtoDMY($result->created_at);

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.admin_messages_retrived_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_admin_messages_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'    => [],
      ]);
    }
  }
  /* END OF NOTIFICATIONS LIST */

  /* BILLING */
  public function billing(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'from_date' => 'required|date_format:Y-m-d',
      'to_date'   => 'required|date_format:Y-m-d',
    ]);

    //IF VALIDATION FAILS
    if ($validator->fails()) {
      return response()->json([
        'code'            => 404,
        'message'         => $validator->messages()->first(),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'            => [],
      ]);
    }

    $from_date = ($request->from_date != '')?$request->from_date.' 00:00:00':false;
    $to_date   = ($request->to_date != '')?$request->to_date.' 11:59:59':false;
    $length    = ($request->length && $request->start != '')?$request->length:false;
    $start     = ($length && $request->start && $request->start != '')?$request->start:false;
    
    $query      = Orders::where('orders.order_status', '=', config('global.APPROVED_ORDER_STATUS'))
                        ->when($from_date, function ($q) use ($from_date) {
                            return $q->where('orders.created_at', '>=', $from_date);
                          })
                        ->when($to_date, function ($q) use ($to_date) {
                            return $q->where('orders.created_at', '<=', $to_date);
                          })
                        ->groupBy('orders.agronomia_id');

    $totalData  = $query->count();

    $results    = $query->when($start, function($q) use ($start){
                            return $q->skip($start);
                          })
                        ->when($length, function($q) use ($length){
                            return $q->take($length);
                          })
                        ->get(['orders.*', DB::raw('COUNT(*) as total_orders'), DB::raw('SUM(orders.total_price) as total_price'), DB::raw('SUM(orders.commission_amount) as total_commission')]);

    $data = [];
    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['agronomia_id']        = $result->agronomia_id;
        $row['agronomia_custom_id'] = $result->agronomia->custom_id;
        $row['total_orders']        = $result->total_orders;
        $row['total_price']         = number_format($result->total_price, 2, ',', '');
        $row['total_commission']    = number_format($result->total_commission, 2, ',', '');
        $row['orders']              = [];

        $orders = Orders::where('agronomia_id', '=', $result->agronomia_id)
                        ->where('orders.order_status', '=', config('global.APPROVED_ORDER_STATUS'))
                        ->when($from_date, function ($q) use ($from_date) {
                            return $q->where('orders.created_at', '>=', $from_date);
                          })
                        ->when($to_date, function ($q) use ($to_date) {
                            return $q->where('orders.created_at', '<=', $to_date);
                          })
                        ->get(['orders.id', 'orders.custom_order_no']);

        foreach($orders as $order)
        {
          array_push($row['orders'], ['id' => $order->id, 'custom_order_no' => $order->custom_order_no]);
        }

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.orders_retrived_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'            => 200,
        'message'         => trans('api.no_orders_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'            => $data,
      ]);
    }
  }
  /* END OF BILLING *

  /* EXPORT BILLING CSV */
  public function exportBillingCSV(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'from_date' => 'required|date_format:Y-m-d',
      'to_date'   => 'required|date_format:Y-m-d',
    ]);

    //IF VALIDATION FAILS
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    Excel::store(new BillingExport($request->from_date, $request->to_date), 'facturación.csv', 'public_uploads');

    return response()->json([
      'code'    => 200,
      'data' => ['link' => asset('public/facturación.csv')]
    ]);
  }
  /* END OF EXPORT BILLING CSV */
  /* START OF ADMIN MESSAGE STORE */
  public function storeAdminMessage(Request $request)
  {
    //VALIDATIONS
    $validator = Validator::make($request->all(), [
      'type'    => 'required|in:'.config('global.ADMIN_MESSAGE.AGRONOMIA').','.config('global.ADMIN_MESSAGE.PRODUCER').','.config('global.ADMIN_MESSAGE.ALL_USERS').','.config('global.ADMIN_MESSAGE.SPECIFIC_USER'),
      'user_id' => 'required_if:type,'.config('global.ADMIN_MESSAGE.SPECIFIC_USER').'|nullable|exists:users,id,deleted_at,NULL',
      'message' => 'required',
    ]);

    //IF VALIDATION FAILS
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }
    
    $data = array(
      'type'    => $request->type,
      'user_id' => ($request->type == config('global.ADMIN_MESSAGE.SPECIFIC_USER'))?$request->user_id:null,
      'message' => $request->message,
    );

    $message = AdminMessages::create($data);
    
    $user = Auth::user();

    Notifications::create([
      'user_id'          => $user->id,
      'admin_message_id' => $message->id,
      'type'             => config('global.NOTIFICATION.ADMIN_MESSAGE'),
      'message'          => $request->message
    ]);

    // PUSH NOTIFICATION
    $users = [];
    if($request->type == config('global.ADMIN_MESSAGE.SPECIFIC_USER')) {
      $users = User::where('id', $request->user_id)->get()->toArray();

    } else if($request->type == config('global.ADMIN_MESSAGE.AGRONOMIA')) {
      $users = User::where(['user_type' => config('global.BUSINESS_USER'), 'is_active' => 1])->get()->toArray();

    } else if($request->type == config('global.ADMIN_MESSAGE.PRODUCER')) {
      $users = User::where(['user_type' => config('global.CUSTOMER_USER'), 'is_active' => 1])->get()->toArray();

    } else if($request->type == config('global.ADMIN_MESSAGE.ALL_USERS')) {
      $users = User::where(['is_active' => 1])->get()->toArray();

    }
    if (!empty($users)) {
      $this->sendFcmPushNotification(
        $users,
        [ 'message'  => $request->message ],
        [
          'type'    => 'notification',
        ]);
    }

    // SEND MAIL
    if (!empty($users)) {
      foreach($users as $user) {
        SendAdminMessageMailJob::dispatch($user['email'], $request->message);
      }
    }

    return response()->json([
      'code'    => 200,
      'message' => trans('api.notification_sent'),
    ]);
  }
  /* END OF ADMIN MESSAGE STORE */

  public function uploadProductsCSV(Request $request) {
    $validator = Validator::make($request->all(), [
      'csv_file'    => 'required|mimes:csv,txt',
    ]);
    //IF VALIDATION FAILS
    if ($validator->fails()) {
      return response()->json([
        'code'    => 401,
        'message' => $validator->messages()->first(),
      ]);
    }

    $path = $request->file('csv_file')->getRealPath();
    $data = array_map('str_getcsv', file($path));

    // Remove header column
    $csv_data = array_slice($data, 1, count($data));
    $new_products = [];
    $updatedRows = [];
    $date = date('Y-m-d H:i:s');
    foreach($csv_data as $key => $row) {
      // print_r($row); continue;
      $id = $row[0];
      $subcategory_id = null;
      $producto_general_id = null;
      $band_id = null;
      $treatment_id = null;
      $diameter_id = null;
      $length_id = null;

      // Subcategory ID from name
      $subcategory_name = trim($row[5]);
      if (!empty($subcategory_name)) {
        $item = ProductSubCategories::where('name','=',$subcategory_name)->first();
        if ($item) {
          $subcategory_id = $item->id;
        } else  {
          return response()->json([
            'code'    => 401,
            'message' => 'Por favor verifique los nombres de las Subcategorías en el número de fila: ' . ($key + 2)
          ]);
          exit;
        }
      }

      // Producto General ID from name
      $producto_general_name = trim($row[6]);
      if (!empty($producto_general_name)) {
        $item = ProductoGenerals::where('name','=',$producto_general_name)->first();
        if ($item) {
          $producto_general_id = $item->id;
        } else  {
          return response()->json([
            'code'    => 401,
            'message' => 'Por favor verifique los nombres de las Producto General en el número de fila: ' . ($key + 2)
          ]);
          exit;
        }
      }

      // Band ID from name
      $band_name = trim($row[8]);
      if (!empty($band_name)) {
        $item = ProductBands::where('name', $band_name)->first();
        if ($item) {
          $band_id = $item->id;
        } else  {
          return response()->json([
            'code'    => 401,
            'message' => 'Por favor verifique los nombres de las Bandas en el número de fila: ' . ($key + 2)
          ]);
          exit;
        }
      }

      // Treatment ID from name
      $treatment_name = trim($row[9]);
      if (!empty($treatment_name)) {
        $item = ProductTreatments::where('name', $treatment_name)->first();
        if ($item) {
          $treatment_id = $item->id;
        } else  {
          return response()->json([
            'code'    => 401,
            'message' => 'Por favor verifique los nombres de los Tratamientos en el número de fila: ' . ($key + 2)
          ]);
          exit;
        }
      }

      // Diameter ID from name
      $diameter_name = trim($row[11]);
      if (!empty($diameter_name)) {
        $item = ProductDiameters::where('name', $diameter_name)->first();
        if ($item) {
          $diameter_id = $item->id;
        } else  {
          return response()->json([
            'code'    => 401,
            'message' => 'Por favor verifique los nombres de los Diámetros en el número de fila: ' . ($key + 2)
          ]);
          exit;
        }
      }

      // Length ID from name
      $length_name = trim($row[12]);
      if (!empty($length_name)) {
        $item = ProductLengths::where('name', $length_name)->first();
        if ($item) {
          $length_id = $item->id;
        } else  {
          return response()->json([
            'code'    => 401,
            'message' => 'Por favor verifique los nombres de los Largos en el número de fila: ' . ($key + 2)
          ]);
          exit;
        }
      }

      $fillable_data = array(
        'name' => $row[1],
        'custom_id' => $row[2],
        'unit_of_measure'=> $row[3],
        'description'=> $row[4],
        'subcategory_id'=> $subcategory_id,
        'producto_general_id'=> $producto_general_id,
        'brand'=> $row[7],
        'band'=> $band_id,
        'treatments'=> $treatment_id,
        'grade'=> $row[10],
        'diameter'=> $diameter_id,
        'length'=> $length_id,
        'gasoline_grade'=> $row[13],
        'attribute_1'=> $row[14],
        'attribute_2'=> $row[15],
        'ingredients'=> $row[16],
        'is_active'=> $row[17],
      );

      // Create new row or Update new row
      if(!$id || $id === '') {
        $fillable_data['created_at'] = $date;
        $new_products[] = $fillable_data;
      } else {
        $fillable_data['updated_at'] = $date;
        $affected = Products::where(['id' => $id])->update($fillable_data);
        if($affected) {
          $updatedRows[] = $fillable_data;
        }
      }
    }

    if(!empty($new_products)) {
      $chuncks = array_chunk($new_products, 400);
      foreach($chuncks as $chuck) {
        Products::insert($chuck);
      }
    }

    return response()->json([
      'code'    => 200,
      'data' => ['inserted'=>count($new_products).' Productos insertados.', 'updated' => count($updatedRows).' Productos actualizados.',]
    ]);
  }

  public function exportProductsCSV(Request $request) {
    Excel::store(new ProductsExport($request->ids), 'products.csv', 'public_uploads');
    return response()->json([
      'code'    => 200,
      'data' => ['link' => asset('public/products.csv')]
    ]);
  }

  public function removeProductsCSV() {
    Storage::disk('public_uploads')->delete('products.csv');
    return response()->json([
      'code'    => 200,
      'message' => 'removed'
    ]);
  }

  public function getTnc() {
    return CustomPages::where('id','1')->orWhere('id', 2)->get()->toArray();
  }

  public function adminUsers(Request $request)
  {
    $search = $request->input('search');
    $start  = $request->input('start');
    $length = $request->input('length');
    $order  = $request->input('order');
    $dir    = $order[0]['dir'];

    $i = 0;
    $columns[$i++] = 'users.id';
    $columns[$i++] = 'users.email';
    $columns[$i++] = 'users.name';
    $columns[$i++] = 'admin_permission_groups.name';
    $columns[$i++] = 'users.is_active';
    $columns[$i++] = 'users.id';

    $searchTerm = (isset($search['value']) && $search['value'] != "")?$search['value']:false;
    $query      = User::where(['user_type' => config('global.ADMIN_USER')])
                       ->join('admin_permission_groups', 'admin_permission_groups.id', '=', 'users.permission_id')
                       ->when($searchTerm, function($query) use ($searchTerm){
                        return $query->where('users.id', 'like', '%'.$searchTerm.'%')
                          ->orWhere('users.email', 'like', '%'.$searchTerm.'%')
                          ->orWhere('users.name', 'like', '%'.$searchTerm.'%')
                          ->orWhere('users.is_active', 'like', '%'.$searchTerm.'%')
                          ->orWhere('admin_permission_groups.name', 'like', '%'.$searchTerm.'%');
                        });

    $totalData  = $query->count();
    $results    = $query->orderBy($columns[$order[0]['column']], $dir)
                        ->skip($start)
                        ->take($length)
                        ->get(['users.id', 'users.email', 'users.name', 'users.is_active', DB::raw('admin_permission_groups.name as permission_name')]);

    $data = array ();
    foreach ($results as $result) 
    {
      $row = [];
      $row['id']              = $result->id;
      $row['email']           = $result->email;
      $row['name']            = $result->name;
      $row['permission_name'] = $result->permission_name;
      $row['is_active']       = (($result->is_active == config('global.ACTIVE'))?true:false);

      $data [] = $row; 
    }

    $tableContent = array (
      "draw" => intval ( $request->input ( 'draw' ) ), 
      "recordsTotal" => intval ( $totalData ),
      "recordsFiltered" => intval ( $totalData ),
      "data" => $data
    );
    return $tableContent;
  }

  public function getAdminUser(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'    => 'required|exists:users,id,user_type,{config("global.ADMIN_USER")},deleted_at,NULL'
    ]);

    $user = User::find($request->id);
 
    return response()->json([
      'code'    => 200,
      'message' => __('labels.user_fetched'),
      'data'    => $user,
    ]);
  }

  public function changeAdminUserState(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'    => 'required|exists:users,id,user_type,'.config("global.ADMIN_USER").',deleted_at,NULL',
      'state' => 'required',
    ]);

    $user = User::find($request->id);
    
    $state = ($request->state == true)?config("global.ACTIVE"):config("global.DISABLED");


    User::where('id', $request->id)->update(['is_active' => $state]);
 
    return response()->json([
      'code'    => 200,
      'message' => ($state == config("global.ACTIVE"))?__('labels.user_activated'):__('labels.user_disabled'),
    ]);
  }

  public function storeAdmin(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name'             => ['required', new AlphaSpace],
      'email'            => 'required|max:128|unique:users,email,'.$request->id.'|email',
      'password'         => 'required_if:id,|nullable|min:8|max:32',
      'is_active'        => 'required',
      'permission_id'    => 'required|exists:admin_permission_groups,id,deleted_at,NULL'
    ]);
  
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    if(strpos($request->name, ' ') !== false)
    {
      $name = explode(' ', $request->name);
      $request->firstname = $name[0];
      unset($name[0]);
      $request->surname   = implode(' ', $name);
    }
    else
    {
      $request->firstname = $request->name;
      $request->surname = '';
    }
 
    $data = array(
      'user_type'       => config('global.ADMIN_USER'),
      'firstname'       => $request->firstname,  
      'surname'         => $request->surname,  
      'name'            => $request->name,      
      'email'           => $request->email,
      'permission_id'   => $request->permission_id,
      'is_active'       => ($request->is_active == true)?config("global.ACTIVE"):config("global.DISABLED"),
      'ip_address'      => $request->ip(),
    );

    if($request->password != '')
    {
      $data['password'] = Hash::make($request->password);
    }
 
    if($request->id == '')
    {
      $user = User::create($data);
      return response()->json([
        'code'    => 200,
        'message' => __('labels.user_successfully_added'),
      ]);
    }
    else
    {
      User::where('id', $request->id)->update($data);
      return response()->json([
        'code'    => 200,
        'message' => __('labels.user_successfully_updated'),
      ]);
    }
  }

  public function permissionsJson()
  {
    $permission = AdminPermissionGroups::get();
    return response()->json([
      'code'    => 200,
      'data'    => $permission,
    ]);
  }
}
