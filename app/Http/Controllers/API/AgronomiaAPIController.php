<?php
namespace App\Http\Controllers\API;

use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Rules\AlphaSpace;
use App\AgronomiaProducts;
use App\Notifications;
use App\AdminMessages;
use App\Http\Controllers\BaseController;
use App\Orders;
use App\User;
use Hash;
use Auth;
use Mail;

class AgronomiaAPIController extends BaseController
{
  /* REGISTERATION */
  public function register(Request $request)
  {
    // VALIDATION
    $validator = Validator::make($request->all(), [
      'business_name' => 'required|string|max:128',
      'cuit'      => 'required|string|max:128',
      'province'  => 'required|exists:provinces,id,deleted_at,NULL',
      'locality'  => 'required|exists:localities,id,deleted_at,NULL,province_id,'.$request->province,
      'firstname' => ['required', 'string', 'max:128', new AlphaSpace],
      'surname'   => ['required', 'string', 'max:128', new AlphaSpace],
      'email'     => 'required|string|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
      'mobile_prefix' => [
        'required', 'string',
        'regex:/^[0-9]{1,4}$/',
      ],
      'mobile' => [
        'required', 'string',
        'regex:/^[0-9]{1,8}$/',
        'unique:users,mobile,NULL,id,deleted_at,NULL',
      ],
      'password' => 'required|string|min:8|max:32',
      'terms_and_conditions' => 'required'
    ]);
 
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }
    // END OF VALIDATION
 
    // CUSTOM ID GENERATION
    $prefix = config('global.PREFIX.AGRONOMIA_USER');
    $count  = User::withTrashed()->where('user_type', '=', config('global.BUSINESS_USER'))->count();
    $custom_id = $prefix.sprintf('%0'.config('global.PREFIX.ORDER_LEADING_ZERO').'d', ($count + 1));
    // END OF CUSTOM ID GENERATION
 
    // USER CREATION
    $data = array(
      'custom_id'     => $custom_id,
      'user_type'     => config('global.BUSINESS_USER'),
      'business_name' => $request->business_name,
      'cuit'          => $request->cuit,
      'province'      => $request->province,
      'locality'      => $request->locality,
      'firstname'     => $request->firstname,  
      'surname'       => $request->surname,  
      'name'          => $request->firstname.' '.$request->surname,  
      'email'         => $request->email,
      'mobile_prefix' => $request->mobile_prefix,
      'mobile'        => $request->mobile,
      'password'      => Hash::make($request->password),
      'is_active'     => config('global.DISABLED'),
      'ip_address'    => $request->ip(),
      'fcm_token'     => $request->fcm_token,
      'device_type'   => $request->device_type,
      'step_2'        => 0,
      'verified'      => 0,
    );
    $user = User::create($data);
    // END OF USER CREATION

    return response()->json([
      'code'    => 200,
      'message' => trans('api.registration_successfully_completed'),
    ]);
  }
  /* END OF REGISTERATION */
  
  /* START OF ADD PRODUCT IN AGRONOMIA PRODUCTS */
  public function addProduct(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'product_id'          => 'required|exists:products,id,deleted_at,NULL',
      'available_quanity'   => 'required|numeric',
      'sales_range_in_km'   => 'required|numeric',
      'agronomia_price'     => 'required|numeric',
      'bonus_applicable'    => 'required|in:'.config('global.TRUE').','.config('global.FALSE'),
      'bonus_price'         => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable|max:'.$request->agronomia_price,
      'min_bonus_quanitity' => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable',
      'is_active'           => 'required|in:'.config('global.ACTIVE').','.config('global.DISABLED'),
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }

    //USER
    $agronomia_id = Auth::user()->id;

    //CHECK IF ALREADY EXIST
    if(AgronomiaProducts::where(['product_id' => $request->product_id, 'agronomia_id' => $agronomia_id])->count() > 0)
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.product_is_already_added_in_your_selling_list'),
      ]);
    }

    $data = array(
      'agronomia_id'        => $agronomia_id,
      'product_id'          => $request->product_id,
      'available_quanity'   => $request->available_quanity,
      'sales_range_in_km'   => $request->sales_range_in_km,
      'agronomia_price'     => $request->agronomia_price,
      'bonus_applicable'    => $request->bonus_applicable,
      'bonus_price'         => ($request->bonus_applicable == config('global.TRUE'))?$request->bonus_price:null,
      'min_bonus_quanitity' => ($request->bonus_applicable == config('global.TRUE'))?$request->min_bonus_quanitity:null,
      'is_active'           => $request->is_active,
    );

    AgronomiaProducts::create($data);

    return response()->json([
        'code'    => 200,
        'message' => trans('api.product_added_successfully'),
      ]);
  }
  /* END OF ADD PRODUCT IN AGRONOMIA PRODUCTS */

  /* START OF AGRONOMIA PRODUCT LIST */
  public function products(Request $request)
  {
    $search = ($request->search)?$request->search:false;
    $length = ($request->length && $request->start != '')?$request->length:false;
    $start  = ($length && $request->start && $request->start != '')?$request->start:false;
    $order  = $request->order;
    $columns = $request->columns;
    if(empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }

    $i = 0;

    $agronomia_id   = Auth::user()->id;
    $category_id    = ($request->category_id && $request->category_id != '')?$request->category_id:false;
    $subcategory_id = ($request->subcategory_id && $request->subcategory_id != '')?$request->subcategory_id:false;
    $searchTerm     = (isset($search['value']) && $search['value'] != "")?$search['value']:false;

    $query          =  AgronomiaProducts::join('products', 'products.id', '=', 'agronomia_products.product_id')
                                        ->join('product_sub_categories', 'product_sub_categories.id', '=', 'products.subcategory_id')
                                        ->join('product_categories', 'product_categories.id', '=', 'product_sub_categories.category_id')
                                        ->where(['agronomia_products.agronomia_id' => $agronomia_id])
                                        ->where(['products.deleted_at' => null, 'products.is_active' => config('global.ACTIVE')])
                                        ->where(['product_sub_categories.deleted_at' => null, 'product_sub_categories.is_active' => config('global.ACTIVE')])
                                        ->where(['product_categories.deleted_at' => null, 'product_categories.is_active' => config('global.ACTIVE')])
                                        ->when($category_id, function($query, $category_id){
                                          return $query->where('product_categories.id', $category_id);
                                        })
                                        ->when($subcategory_id, function($query, $subcategory_id){
                                          return $query->where('product_sub_categories.id', $subcategory_id);
                                        })
                                        ->when($searchTerm, function($q) use ($searchTerm){
                                          return $q->where(function($sub_q) use ($searchTerm) {
                                            $sub_q->where('products.name', 'like', '%'.$searchTerm.'%')
                                                  ->orWhere('agronomia_products.available_quanity', 'like', '%'.$searchTerm.'%')
                                                  ->orWhere('agronomia_products.agronomia_price', 'like', '%'.$searchTerm.'%');
                                          });
                                        });

    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                            return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                          })
                          ->when($start, function($q) use ($start){
                            return $q->skip($start);
                          })
                          ->when($length, function($q) use ($length){
                            return $q->take($length);
                          })
                          ->get(['agronomia_products.id', 'agronomia_products.product_id', 'products.name', 'products.description', 'products.image', 'agronomia_products.agronomia_price', 'agronomia_products.available_quanity', 'agronomia_products.is_active']);

    $data = [];

    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['id']                = $result->id;
        $row['product_id']        = $result->product_id;
        $row['name']              = $result->name;
        $row['description']       = $result->description;
        $row['image']             = asset('storage/app/public/uploads/products/'.$result->image);
        $row['agronomia_price']   = number_format($result->agronomia_price, 2, ',', '') . ' USD';
        $row['available_quanity'] = (string)$result->available_quanity;
        $row['is_active']         = $result->is_active;

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.products_fetched_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_products_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'    => [],
      ]);
    }
  }
  /* END OF AGRONOMIA PRODUCT LIST */

  /* START OF AGRONOMIA PRODUCT DETAIL */
  public function productDetail(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:agronomia_products,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    //USER
    $agronomia_id      = Auth::user()->id;
    $agronomia_product = AgronomiaProducts::find($request->id);

    //CHECK IF AUTHORISED USER
    if($agronomia_product->agronomia_id != $agronomia_id)
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }

    return response()->json([
        'code'    => 200,
        'message' => trans('api.product_fetched_successfully'),
        'data'    => [
                      'id'                  => $agronomia_product->id,
                      'product_id'          => $agronomia_product->product_id,
                      'name'                => $agronomia_product->products->name,
                      'description'         => $agronomia_product->products->description,
                      'image'               => asset('public/uploads/products/'.$agronomia_product->products->image),
                      'admin_price'         => (string)$agronomia_product->products->price,
                      'unit_of_measure'     => $agronomia_product->products->unit_of_measure,
                      'ingredients'         => $agronomia_product->products->ingredients,
                      'available_quanity'   => (string)$agronomia_product->available_quanity,
                      'sales_range_in_km'   => (string)$agronomia_product->sales_range_in_km,
                      'sales_range_in_km'   => (string)$agronomia_product->sales_range_in_km,
                      'agronomia_price'     => (string)$agronomia_product->agronomia_price,
                      'bonus_applicable'    => $agronomia_product->bonus_applicable,
                      'bonus_price'         => ($agronomia_product->bonus_price)?(string)$agronomia_product->bonus_price:'',
                      'min_bonus_quanitity' => ($agronomia_product->bonus_price)?(string)$agronomia_product->min_bonus_quanitity:'',
                      'is_active'           => $agronomia_product->is_active,
                     ]
      ]);
  }
  /* END OF AGRONOMIA PRODUCT DETAIL */

  /* START OF AGRONOMIA PRODUCT EDIT */
  public function editProduct(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id'                  => 'required|exists:agronomia_products,id,deleted_at,NULL',
      'product_id'          => 'required|exists:products,id,deleted_at,NULL',
      'available_quanity'   => 'required|numeric',
      'sales_range_in_km'   => 'required|numeric',
      'agronomia_price'     => 'required|numeric',
      'bonus_applicable'    => 'required|in:'.config('global.TRUE').','.config('global.FALSE'),
      'bonus_price'         => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable|max:'.$request->agronomia_price,
      'min_bonus_quanitity' => 'required_if:bonus_applicable,'.config('global.TRUE').'|numeric|nullable',
      'is_active'           => 'required|in:'.config('global.ACTIVE').','.config('global.DISABLED'),
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }

    //USER
    $agronomia_id      = Auth::user()->id;
    $agronomia_product = AgronomiaProducts::find($request->id);

    //CHECK IF AUTHORISED USER
    if($agronomia_product->agronomia_id != $agronomia_id)
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }

    //CHECK IF ALREADY EXIST
    if(AgronomiaProducts::where(['product_id' => $request->product_id, 'agronomia_id' => $agronomia_id])->where('id', '!=', $request->id)->count() > 0)
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.product_is_already_added_in_your_selling_list'),
      ]);
    }

    $data = array(
      'product_id'          => $request->product_id,
      'available_quanity'   => $request->available_quanity,
      'sales_range_in_km'   => $request->sales_range_in_km,
      'bonus_applicable'    => $request->bonus_applicable,
      'agronomia_price'     => $request->agronomia_price,
      'bonus_price'         => ($request->bonus_applicable == config('global.TRUE'))?$request->bonus_price:null,
      'min_bonus_quanitity' => ($request->bonus_applicable == config('global.TRUE'))?$request->min_bonus_quanitity:null,
      'is_active'           => $request->is_active,
    );

    $agronomia_product->update($data);

    return response()->json([
        'code'    => 200,
        'message' => trans('api.product_updated_successfully'),
      ]);
  }
  /* END OF AGRONOMIA PRODUCT EDIT */

  /* START OF AGRONOMIA PRODUCT DELETE */
  public function deleteProduct(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:agronomia_products,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    //USER
    $agronomia_id      = Auth::user()->id;
    $agronomia_product = AgronomiaProducts::find($request->id);

    //CHECK IF AUTHORISED USER
    if($agronomia_product->agronomia_id != $agronomia_id)
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }

    $agronomia_product->delete();

    return response()->json([
        'code'    => 200,
        'message' => trans('api.product_deleted_successfully')
      ]);
  }
  /* END OF AGRONOMIA PRODUCT DELETE */

  /* START OF APPLY INCREASE / DISCOUNT IN PRODUCTS */
  public function applyIncreaseDiscount(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'agronomia_product_ids' => 'required',
      'action_type'           => 'required|in:'.config('global.INCREASE_PRICE').','.config('global.APPLY_DISCOUNT'),
      'percentage'            => 'required|numeric|min:0|max:100',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    //USER
    $agronomia_id          = Auth::user()->id;

    // FILTER AUTHORISED PRODUCTS
    $agronomia_product_ids = AgronomiaProducts::whereIn('id', $request->agronomia_product_ids)->where(['agronomia_id' => $agronomia_id])->pluck('id');

    //CHECK IF PRODUCTS AVAILABLE
    if(!empty($agronomia_product_ids) && $agronomia_product_ids->count() > 0)
    {
      if($request->action_type == config('global.INCREASE_PRICE'))
      {
        AgronomiaProducts::whereIn('id', $request->agronomia_product_ids)
                          ->update(['agronomia_price' => DB::raw('(agronomia_price + ((agronomia_price * '.$request->percentage.') / 100))'),
                                    'bonus_price'     => DB::raw('(bonus_price + ((bonus_price * '.$request->percentage.') / 100))')
                                  ]);
        return response()->json([
          'code'    => 200,
          'message' => trans('api.price_increased_successfully'),
        ]);
      }
      else if($request->action_type == config('global.APPLY_DISCOUNT'))
      {
        AgronomiaProducts::whereIn('id', $request->agronomia_product_ids)
                          ->update(['agronomia_price' => DB::raw('(agronomia_price - ((agronomia_price * '.$request->percentage.') / 100))'),
                                    'bonus_price'     => DB::raw('(bonus_price - ((bonus_price * '.$request->percentage.') / 100))')
                                  ]);
        return response()->json([
          'code'    => 200,
          'message' => trans('api.discount_applied_successfully'),
        ]);
      }
    }

    return response()->json([
        'code'    => 404,
        'message' => trans('api.no_products_selected_to_do_action')
      ]);
  }
  /* END OF APPLY INCREASE / DISCOUNT IN PRODUCTS */

  /* START OF APPROVE / REJECT ORDER */
  public function approveRejectOrder(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'order_id'     => 'required|exists:orders,id,deleted_at,NULL',
      'order_status' => 'required|in:'.config('global.APPROVED_ORDER_STATUS').','.config('global.CANCELLED_ORDER_STATUS')
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $order = Orders::find($request->order_id);
    $user  = Auth::user();

    // IF ORDER IS NOT OF CURRENT AGRONOMIA
    if($order->agronomia_id != $user->id)
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }

    // IF ORDER IS NOT PENDING
    if($order->order_status != config('global.PENDING_ORDER_STATUS'))
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.order_is_not_pending'),
      ]);
    }

    $data['order_status']  = $request->order_status;
    $data['approval_date'] = date('Y-m-d H:i:s');

    if($request->order_status == config('global.APPROVED_ORDER_STATUS'))
    {
      // REDUCE THE PRODUCT AVAILABLE QUANTITY
      $agronomiaProduct = AgronomiaProducts::where(['product_id' => $order->product_id, 'agronomia_id' => $order->agronomia_id])->first();
      $newAvailableQuantity = $agronomiaProduct->available_quanity - $order->ordered_quantity;
      $updateQuantity = array(
        'available_quanity' => $newAvailableQuantity
      );
      $agronomiaProduct->update($updateQuantity);

      //ENABLE ALL CHAT STATUS
      $data['producer_chat_status']  = config('global.ACTIVE'); // PRODUCER CAN NOT SEND MESSAGE IF DISABLED, AGRONOMIA HAVE RIGHT TO CHANGE IT
      $data['agronomia_chat_status'] = config('global.ACTIVE'); // AGRONOMIA CAN NOT SEND MESSAGE IF DISABLED, PRODUCER HAVE RIGHT TO CHANGE IT
      $data['master_chat_status']    = config('global.ACTIVE'); // NO ONE CAN SEND MESSAGE IF DISABLED, ONLY ADMIN HAVE RIGHT TO CHANGE IT

      $order->update($data);
      Notifications::create(['user_id' => $order->producer_id, 'type' => config('global.NOTIFICATION.ORDER_APPROVED'), 'message' => $order->id]);
      Notifications::create(['user_id' => $order->agronomia_id, 'type' => config('global.NOTIFICATION.ORDER_APPROVED'), 'message' => $order->id]);
      AdminMessages::create(['type' => config('global.ADMIN_MESSAGE.ADMIN_NOTIFICATION'), 'notification_type' => config('global.NOTIFICATION.ORDER_APPROVED'), 'message' => $order->id]);

      /* SEND MAIL TO PRODUCER */
      \Mail::to($order->producer->email)->send(new \App\Mail\OrderApproved($order, 'producer'));

      /* SEND MAIL TO AGRONOMIA */
      \Mail::to($user->email)->send(new \App\Mail\OrderApproved($order, 'agronomia'));

      /* SEND PUSH NOTIFICATION TO PRODUCER */
      $this->sendFcmPushNotification(
        [ User::find($order->producer_id)->toArray() ],
        [ 'message'  => str_replace(':order_no', $order->custom_order_no, __('labels.push_notification.producer.order_approved')) ],
        [
          'type'    => 'order',
          'order_id' => $order->id
        ]);

      /* SEND PUSH NOTIFICATION TO AGRONOMIA */
      $this->sendFcmPushNotification(
        [ $user ],
        [ 'message'  => str_replace(':order_no', $order->custom_order_no, __('labels.push_notification.agronomia.order_approved')) ],
        [
          'type'    => 'order',
          'order_id' => $order->id
        ]);

      return response()->json([
        'code'    => 200,
        'message' => trans('api.order_approved')
      ]);
      
    }
    elseif($request->order_status == config('global.CANCELLED_ORDER_STATUS'))
    {
      $order->update($data);
      Notifications::create(['user_id' => $order->producer_id, 'type' => config('global.NOTIFICATION.ORDER_CANCELLED'), 'message' => $order->id]);
      Notifications::create(['user_id' => $order->agronomia_id, 'type' => config('global.NOTIFICATION.ORDER_CANCELLED'), 'message' => $order->id]);
      AdminMessages::create(['type' => config('global.ADMIN_MESSAGE.ADMIN_NOTIFICATION'), 'notification_type' => config('global.NOTIFICATION.ORDER_CANCELLED'), 'message' => $order->id]);
      
      /* SEND MAIL TO PRODUCER */
      \Mail::to($order->producer->email)->send(new \App\Mail\OrderRejected($order, 'producer'));

      /* SEND MAIL TO AGRONOMIA */
      \Mail::to($user->email)->send(new \App\Mail\OrderRejected($order, 'agronomia'));

      /* SEND PUSH NOTIFICATION PRODUCER */
      $this->sendFcmPushNotification(
        [ User::find($order->producer_id)->toArray() ],
        [ 'message'  => str_replace(':order_no', $order->custom_order_no, __('labels.push_notification.producer.order_rejected')) ],
        [
          'type'    => 'order',
          'order_id' => $order->id
        ]);

      /* SEND PUSH NOTIFICATION TO AGRONOMIA */
      $this->sendFcmPushNotification(
        [ $user ],
        [ 'message'  => str_replace(':order_no', $order->custom_order_no, __('labels.push_notification.agronomia.order_rejected')) ],
        [
          'type'    => 'order',
          'order_id' => $order->id
        ]);
      
      return response()->json([
        'code'    => 200,
        'message' => trans('api.order_rejected')
      ]);
    }


  }
  /* END OF APPROVE / REJECT ORDER */
}
