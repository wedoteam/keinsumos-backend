<?php
namespace App\Http\Controllers\API;

use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Events\OrderChat\MessageRead;
use App\Events\OrderChat\MessageSent;

use App\AdminPermissionGroupRights;
use App\AgronomiaPaymentMethods;
use App\Provinces;
use App\Localities;
use App\Menubar;
use App\UserReviews;
use App\ProductCategories;
use App\PaymentMethods;
use App\Notifications;
use App\AdminMessages;
use App\CustomPages;
use App\UserAvtars;
use App\Products;
use App\Messages;
use App\Orders;
use App\User;
use App\Faq;
use App\ProductSubCategories;
use App\ProductBands;
use App\ProductDiameters;
use App\ProductLengths;
use App\ProductoGenerals;
use App\ProductTreatments;
use App\UserGeolocation;
use App\Rules\AlphaSpace;
use Hash;
use Auth;
use Mail;
use Session;
use Carbon\Carbon;

class CommonAPIController extends BaseController
{
  /* step 2 registeration */
  public function registerStep2(Request $request)
  {
    $user = Auth::user();

    if($user->user_type == config('global.CUSTOMER_USER')) // VALIDATION FOR PRODUCER
    {
      $validator = Validator::make($request->all(), [
        'firstname'     => ['required', 'string', 'max:128', new AlphaSpace],
        'surname'       => ['required', 'string', 'max:128', new AlphaSpace],
        'mobile_prefix' => [
          'required', 'string',
          'regex:/^[0-9]{1,4}$/',
        ],
        'mobile'        => [
          'required', 'string',
          'regex:/^[0-9]{1,8}$/',
          'unique:users,mobile,'.$request->id.',id,deleted_at,NULL',
        ],
        'renspa_no' => [
          'nullable', 'max:128',
          'regex:/^[0-9]{2}[.][0-9]{3}[.][0-9]{1}[.][0-9]{1,}$/'
        ],
        'province' => ['required', 'exists:provinces,id,deleted_at,NULL'],
        'locality' => [
          'required',
          'exists:localities,id,deleted_at,NULL,province_id,'.$request->province
        ]
      ]);
    }
    else if($user->user_type == config('global.BUSINESS_USER'))
    {
      $validator = Validator::make($request->all(), [
        'sales_range_in_km' => ['required', 'numeric'],
        'latitude'          => ['required', 'numeric', 'min:-90', 'max:90'],
        'longitude'         => ['required', 'numeric', 'min:-180', 'max:180'],
        'payment_methods'   => ['required', 'present'],
        'payment_methods.*' => [
          'exists:payment_methods,id,is_active,'.config('global.ACTIVE').',deleted_at,NULL'
        ],
      ]);
    }
    
    if($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }

    if($user->user_type == config('global.CUSTOMER_USER')) // VALIDATION FOR PRODUCER
    {
      $user->update([
        'firstname'     => $request->firstname,
        'surname'       => $request->surname,
        'name'          => $request->firstname.' '.$request->surname,
        'mobile_prefix' => $request->mobile_prefix,
        'mobile'        => $request->mobile,
        'renspa_no'     => ($request->renspa_no) ?? null,
        'province'      => $request->province,
        'locality'      => $request->locality,
        'step_2'        => 1, // yes
        'verified'      => ($request->renspa_no) ? 1 : 0,
      ]);
    }
    else if($user->user_type == config('global.BUSINESS_USER'))
    {
      $user->update([
        'latitude'          => $request->latitude,
        'longtitude'        => $request->longitude,
        'sales_range_in_km' => $request->sales_range_in_km,
        'step_2'            => 1, // yes
        'verified'          => 1, // yes
      ]);

      AgronomiaPaymentMethods::where(['agronomia_id' => $user->id])->delete();

      $i = 0;
      $paymentMethods = array_unique($request->payment_methods);
      foreach($paymentMethods as $paymentMethod)
      {
        $paymentMethodData[$i]['agronomia_id']      = $user->id;
        $paymentMethodData[$i]['payment_method_id'] = $paymentMethod;
        $paymentMethodData[$i]['created_at'] = date('Y-m-d H:i:s');
        $paymentMethodData[$i]['updated_at'] = date('Y-m-d H:i:s');
        $i++;
      }
      AgronomiaPaymentMethods::insert($paymentMethodData);
    }

    return response()->json([
      'code'    => 200,
      'message' => __('api.registeration_completed'),
      'data'    => $user,
    ]);
  }
  /* end of step 2 registeration */

  /* START OF USER LOGIN */
  public function login(Request $request)
  {
    $validator = Validator::make($request->all(), [
     'email'   => 'required',
     'password' => 'required',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $user = User::where('email', $request->email)->get()->first();

    if(!empty($user) && $user->count() > 0) //IF EMAIL MATCHES
    { 
      if($user->user_type != config('global.ADMIN_USER')) //CHECK IF NOT ADMIN
      {
        if($user->is_active == config('global.ACTIVE')) //IF ACTIVE
        {
          $credentials = request(['email', 'password']);

          if(!$token = auth('api')->attempt($credentials)) //IF AUTHENTICATE
          {
            return response()->json([
              'code'    => 404,
              'message' => trans('login_messages.please_enter_valid_password'),
            ]);
          }
          else
          {
            $last_login_at = $user->last_login_at;

            // update fcm token
            $data = array(
                'fcm_token'   => $request->fcm_token,
                'device_type' => $request->device_type,
              'last_login_at' => Carbon::now()->toDateTimeString(),
              'last_login_ip' => $request->getClientIp()
            );
            $user->update($data);

            return response()->json([
              'code'    => 200,
              'message' => trans('api.successfully_logged_in'),
              'data'    => [
                'id'             => $user->id,
                'token'          => $token,
                'user_type'      => $user->user_type,
                'last_login_at'  => (string)$last_login_at
              ],
            ]);
          }
        }
        else
        {
          return response()->json([
            'code'    => 404,
            'message' => trans('api.the_user_is_in_approval_process'),
          ]);
        }
      }
      else
      {
        return response()->json([
          'code'    => 404,
          'message' => trans('login_messages.unauthorized_access'),
        ]);
      }
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.account_not_found'),
      ]);
    }
  }
  /* END OF USER LOGIN */

  public function web_auth()
  {
    if(Auth::check()) 
    {    
      $user = Auth::user();
      if($user->is_active = config('global.ACTIVE'))
      {
        $user = Auth::user();
        return response()->json($user);
      }
      Auth::logout();
    }
    return response()->json(false);
  }

  public function userAuth()
  {
    if(Auth::check()) 
    {
      $user = Auth::user();
      if(($user->user_type == config('global.BUSINESS_USER') || $user->user_type == config('global.CUSTOMER_USER')) && $user->is_active = config('global.ACTIVE'))
      {
        $user = Auth::user();
        return response()->json($user);
      }
      Auth::logout();
    }
    return response()->json(false);
  }

   /* START OF FORGET PASSWORD */
  public function forgetPassword(Request $request)
  {
    $validator = Validator::make($request->all(), [
     'email'    => 'required|email',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $user = User::where('email', $request->email)->get()->first();

    if(!empty($user) && $user->count() > 0)
    { 
      if($user->is_active == config('global.ACTIVE'))
      {
        Password::sendResetLink(['email' => $request->email]);
        return response()->json([
          'code'    => 200,
          'message' => trans('api.password_reset_check_your_mail_for_new_password'),
        ]);
      }
      else
      {
        return response()->json([
          'code'    => 404,
          'message' => trans('api.the_user_is_in_approval_process'),
        ]);
      }
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.account_not_found'),
      ]);
    }
  }
  /* END OF FORGET PASSWORD */

  // MENUBAR
  public function menubar()
  {
    $user  = Auth::user();
    $menus = Menubar::where(['type' => $user->user_type, 'is_active' => config('global.ACTIVE')])
                    ->orderBy('order_priority', 'ASC')
                    ->get();
          
    //CHECK PERMISSION TYPE AND ACCESSABLE MENU, IF LOGGED IN USER IS ADMIN
    $userRights = [];
    if($user->user_type == config('global.ADMIN_USER'))
    {
      $userRights = AdminPermissionGroupRights::where('group_id', '=', $user->permission_id)->get()->pluck('menu_id')->toArray();
    }

    $json = [];
    if(!empty($menus) && $menus->count() > 0)
    {
      foreach($menus as $menu)
      {
        $row['id']    = $menu->id;
        $row['icon']  = $menu->icon;
        $row['name']  = $menu->name;
        $row['route'] = $menu->route;
        $row['class'] = '';
        
        if($user->user_type != config('global.ADMIN_USER') || in_array($menu->id, $userRights))
        {
          array_push($json, $row);
        }
      }
    }

    return response()->json([
      'code'    => 200,
      'message' => __('labels.data_retrived'),
      'data'    => $json
    ]);
  }
  // MENUBAR

  /* START OF MY ACCOUNT */
  public function myAccount()
  {
    $user = Auth::user();

    $data = [
      'id'               => $user->id,
      'user_type'        => $user->user_type,
      'firstname'        => (string)$user->firstname,
      'surname'          => (string)$user->surname,
      'email'            => $user->email,
      'business_name'    => $user->business_name,
      'cuit'             => $user->cuit,
      'mobile_prefix'    => (string)$user->mobile_prefix,
      'mobile'           => (string)$user->mobile,
      'province'         => (string)$user->province,
      'province_name'    => ($user->province)?$user->user_province->name:"",
      'locality'         => ($user->locality)?(string)$user->locality:"",
      'locality_name'    => ($user->locality)?$user->user_locality->name:"",
      'avtar_id'         => ($user->avtar_id)?(string)$user->avtar_id:'',
      'avtar_path'       => $user->avtar_path,
      'step_2'           => (bool)$user->step_2,
      'verified'         => (bool)$user->verified,
    ];

    if($user->user_type == config('global.BUSINESS_USER'))
    {
      $data['latitude']          = $user->latitude;
      $data['longtitude']        = $user->longtitude;
      $data['sales_range_in_km'] = (string)$user->sales_range_in_km;
      $data['payment_methods']   = [];

      $paymentMethods = PaymentMethods::leftJoin('agronomia_payment_methods', function($join) use($user) {
                                                    return $join->on('agronomia_payment_methods.payment_method_id', '=', 'payment_methods.id')
                                                                ->where(['agronomia_payment_methods.agronomia_id' => $user->id, 'agronomia_payment_methods.deleted_at' => null]);
                                                 }) 
                                               ->where('payment_methods.is_active', config('global.ACTIVE'))
                                               ->get(['payment_methods.*', DB::raw('agronomia_payment_methods.id as agronomia_payment_method_id')]);
      if($paymentMethods->count() > 0)
      {
        foreach($paymentMethods as $paymentMethod)
        {
          $row['id']         = $paymentMethod->id;
          $row['name']       = $paymentMethod->name;
          $row['is_allowed'] = ($paymentMethod->agronomia_payment_method_id == null)?false:true;
          array_push($data['payment_methods'], $row);
        }
      }
    }

    if($user->user_type == config('global.CUSTOMER_USER'))
    {

      $data['renspa_no']   = (string)$user->renspa_no;
      $data['geolocation'] = [];

      $locations = UserGeolocation::where('user_id', '=', $user->id)
                                  ->get(['id', 'name', 'latitude', 'longtitude']);

      if(!empty($locations))
      {
        foreach($locations as $location)
        {
          array_push($data['geolocation'], [
            'id'         => (string)$location->id,
            'name'       => $location->name,
            'latitude'   => $location->latitude,
            'longtitude' => $location->longtitude,
          ]);
        }
      }
    }

    return response()->json([
      'code'     => 200,
      'message' => trans('api.user_account_retrived_successfully'),
      'data'     => $data,
    ]);
  }
  /* END OF MY ACCOUNT */

  /* START OF UPDATE PROFILE */
  public function updateProfile(Request $request)
  {
    $user = Auth::user();

    if($user->user_type == config('global.ADMIN_USER')) // VALIDATION FOR ADMIN
    {
      $validator = Validator::make($request->all(), [
        'password' => 'confirmed|nullable|min:8|max:32',
      ]);
    }
    else if($user->user_type == config('global.CUSTOMER_USER')) // VALIDATION FOR PRODUCER
    {
      $step_2_condition = ($user->step_2) ? 'required' : 'nullable';
      $validator = Validator::make($request->all(), [
        'mobile_prefix' => [
          $step_2_condition, 'string',
          'regex:/^[0-9]{1,4}$/',
        ],
        'mobile'        => [
          $step_2_condition, 'string',
          'regex:/^[0-9]{1,8}$/',
          'unique:users,mobile,'.$user->id.',id',
        ],
        'renspa_no' => [
          'nullable', 'max:128',
          'regex:/^[0-9]{2}[.][0-9]{3}[.][0-9]{1}[.][0-9]{1,}$/'
        ],
        'password'                 => 'confirmed|nullable|min:8|max:32',
        'geolocation'              => [$step_2_condition, 'present'],
        'geolocation.*.id'         => 'nullable|exists:user_geolocation,id,user_id,'.$user->id.',deleted_at,NULL',
        'geolocation.*.name'       => 'required|max:128',
        'geolocation.*.latitude'   => 'required|numeric|min:-90|max:90',
        'geolocation.*.longtitude' => 'required|numeric|min:-180|max:180',
      ]);
    }
    else if($user->user_type == config('global.BUSINESS_USER')) // VALIDATION FOR AGRONOMIA
    {
      $step_2_condition = ($user->step_2) ? 'required' : 'nullable';
      $validator = Validator::make($request->all(), [
        //unique:table_name,column_name,primary_key,primary_column,where_column,where_value
        'mobile_prefix'    => 'required|string|max:8',
        'mobile'           => 'required|string|max:16|unique:users,mobile,'.$user->id.',id',
        'latitude'         => [$step_2_condition, 'numeric', 'min:-90', 'max:90'],
        'longtitude'       => [$step_2_condition, 'numeric', 'min:-180', 'max:180'],
        'avtar_id'         => 'required|exists:user_avtars,id,deleted_at,NULL',
        'payment_methods'  => [$step_2_condition, 'present'],
        'payment_methods.*'=> 'exists:payment_methods,id,is_active,'.config('global.ACTIVE').',deleted_at,NULL',
        'password'         => 'confirmed|nullable|min:8|max:32',
      ]);
    }
    
    if($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }

    $data = [];
    if($user->user_type != config('global.ADMIN_USER'))
    {
      $data['mobile_prefix'] = $request->mobile_prefix;
      $data['mobile']        = $request->mobile;

      if($user->user_type == config('global.CUSTOMER_USER')
          && $user->verified == '0'
          && $request->renspa_no != '')
      {
        $data['renspa_no'] = $request->renspa_no;
        if($user->step_2 == '1')
        {
          $data['verified']  = 1;
        }
      }

      if($user->user_type == config('global.BUSINESS_USER'))
      {
        $data['latitude']   = $request->latitude;
        $data['longtitude'] = $request->longtitude;
        $data['avtar_id']   = $request->avtar_id;
        if($request->latitude != '' && $request->longtitude != ''
            && is_array($request->payment_methods) && count($request->payment_methods)  > 0)
        {
          $data['step_2']   = 1;
          $data['verified'] = 1;
        }
      }
    }

    if($request->password != '')
    {
      $data['password'] = Hash::make($request->password);
    }

    $user->update($data);

    // IF AGRONOMIA
    if($user->user_type == config('global.BUSINESS_USER'))
    {
      //DELETE ALL PAYMENT METHODS
      AgronomiaPaymentMethods::where(['agronomia_id' => $user->id])->delete();

      //IF PAYMENT METHODS EXIST
      if(is_array($request->payment_methods) && !empty($request->payment_methods))
      {
        $i = 0;
        $paymentMethods = array_unique($request->payment_methods);
        foreach($paymentMethods as $paymentMethod)
        {
          $paymentMethodData[$i]['agronomia_id']      = $user->id;
          $paymentMethodData[$i]['payment_method_id'] = $paymentMethod;
          //ADDED IT MANUALLY, AS BULK INSERT DOESN'T SUPPORT TIMESTAMP
          $paymentMethodData[$i]['created_at'] = date('Y-m-d H:i:s');
          $paymentMethodData[$i]['updated_at'] = date('Y-m-d H:i:s');
          $i++;
        }
        AgronomiaPaymentMethods::insert($paymentMethodData);
      }
    }

    //IF PRODUCER
    if($user->user_type == config('global.CUSTOMER_USER'))
    {
      $geolocations = $request->geolocation;
      foreach($geolocations as $geolocation)
      {
        $single_location['user_id']    = $user->id;
        $single_location['name']       = $geolocation['name'];
        $single_location['latitude']   = $geolocation['latitude'];
        $single_location['longtitude'] = $geolocation['longtitude'];
        if($geolocation['id'])
        {
          UserGeolocation::where('id', '=', $geolocation['id'])
                         ->update($single_location);
        }
        else
        {
          UserGeolocation::insert($single_location);
        }
      }
    }

    return response()->json([
      'code'    => 200,
      'message' => trans('api.profile_updated_successfully'),
      'data'    => [
        'id'               => $user->id,
        'user_type'        => $user->user_type,
        'firstname'        => (string)$user->firstname,
        'surname'          => (string)$user->surname,
        'email'            => $user->email,
        'business_name'    => $user->business_name,
        'cuit'             => $user->cuit,
        'mobile_prefix'    => (string)$user->mobile_prefix,
        'mobile'           => (string)$user->mobile,
        'province'         => (string)$user->province,
        'province_name'    => ($user->province)?$user->user_province->name:"",
        'locality'         => ($user->locality)?(string)$user->locality:"",
        'locality_name'    => ($user->locality)?$user->user_locality->name:"",
        'avtar_id'         => ($user->avtar_id)?(string)$user->avtar_id:'',
        'avtar_path'       => $user->avtar_path,
        'step_2'           => (bool)$user->step_2,
        'verified'         => (bool)$user->verified,
      ],
    ]);
  }
  /* END OF UPDATE PROFILE */

  /* START OF USER AVTARS LIST */
  public function userAvtars()
  {
    $avtars = UserAvtars::get();

    $data = [];
    foreach($avtars as $avtar)
    {
      array_push($data, ['id' => $avtar->id, 'filepath' => $avtar->filepath]);
    }

    return response()->json([
      'code'    => 200,
      'message' => trans('api.profile_updated_successfully'),
      'data'    => $data
    ]);
  }
  /* END OF USER AVTARS LIST */

  /* START OF USER DETAIL */
  public function userDetail(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:users,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }
    
    $user = User::where('id', '=', $request->id)
                ->whereIn('user_type', [config('global.BUSINESS_USER'), config('global.CUSTOMER_USER')])
                ->first();

    if(!empty($user))
    {
      $data = [
        'id'               => $user->id,
        'custom_id'        => $user->custom_id,
        'user_type'        => $user->user_type,
        'user_type_label'  => $user->user_type_label,
        'firstname'        => $user->firstname,
        'surname'          => $user->surname,
        'business_name'    => $user->business_name,
        'email'            => $user->email,
        'mobile_prefix'    => $user->mobile_prefix,
        'mobile'           => $user->mobile,
        'renspa_no'        => ($user->renspa_no)?$user->renspa_no:'',
        'cuit'             => $user->cuit,
        'province'         => $user->province,
        'province_name'    => ($user->province)?$user->user_province->name:'',
        'locality'         => $user->locality,
        'locality_name'    => ($user->locality)?$user->user_locality->name:'',
        'latitude'         => ($user->latitude != null)?$user->latitude:'',
        'longtitude'       => ($user->longtitude != null)?$user->longtitude:'',
        'worst_credit_bureau'          => (string)$user->worst_credit_bureau,
        'user_score'                   => (string)$user->user_score,
        'bad_cheques'                  => (string)$user->bad_cheques,
        'unpaid_employer_contribution' => (string)$user->unpaid_employer_contribution,
        'vat_condition'                => (string)$user->vat_condition,
        'avtar_id'         => ($user->avtar_id != null)?$user->avtar_id:'',
        'avtar_path'       => $user->avtar_path,
        'is_active'        => $user->is_active,
        'sales_range_in_km'=> (string)$user->sales_range_in_km,
        'verified'         => $user->verified,
        'boolean_state'    => $user->boolean_state,
      ];

      if($user->user_type == config('global.BUSINESS_USER'))
      {
        $paymentMethods = PaymentMethods::leftJoin('agronomia_payment_methods', function($join) use($user) {
                                                    return $join->on('agronomia_payment_methods.payment_method_id', '=', 'payment_methods.id')
                                                                ->where(['agronomia_payment_methods.agronomia_id' => $user->id, 'agronomia_payment_methods.deleted_at' => null]);
                                                  }) 
                                                ->where('payment_methods.is_active', config('global.ACTIVE'))
                                                ->get(['payment_methods.*', DB::raw('agronomia_payment_methods.id as agronomia_payment_method_id')]);

        $data['payment_methods'] = [];
        if($paymentMethods->count() > 0)
        {
          foreach($paymentMethods as $paymentMethod)
          {
            $row['id']         = $paymentMethod->id;
            $row['name']       = $paymentMethod->name;
            $row['is_allowed'] = ($paymentMethod->agronomia_payment_method_id == null)?false:true;
            array_push($data['payment_methods'], $row);
          }
        }
      }

      if($user->user_type == config('global.CUSTOMER_USER'))
      {
        $geolocations = UserGeolocation::where('user_id', $user->id)
                                       ->get(['id', 'name', 'latitude', 'longtitude']);

        $data['geolocation'] = [];
        if($geolocations->count() > 0)
        {
          foreach($geolocations as $geolocation)
          {
            $row['id']         = $geolocation->id;
            $row['name']       = $geolocation->name;
            $row['latitude']   = $geolocation->latitude;
            $row['longtitude'] = $geolocation->longtitude;
            array_push($data['geolocation'], $row);
          }
        }
      }

      return response()->json([
        'code'    => 200,
        'message' => trans('api.user_retrived'),
        'data'    => $data,
      ]);
    }

    return response()->json([
      'code'    => 404,
      'message' => trans('api.user_not_found'),
    ]);
                    
  }
  /* END OF USER DETAIL */

  /* START OF PRODUCT CATEGORY LISTING*/
  public function productCategories()
  {
    $categories = ProductCategories::where(['is_active' => config('global.ACTIVE')])->with('ProductSubCategories')->get();
    if(!empty($categories) && $categories->count() > 0)
    {
      $json = [];
      foreach($categories as $category)
      {
        $row['id']          = $category->id;
        $row['name']        = $category->name;
        $row['description'] = $category->description;
        $row['image']       = ($category->image != '')?asset('public/uploads/product_categories/'.$category->image):'';
        $row['sub_categories']  = [];

        if(!empty($category->ProductSubCategories) && $category->ProductSubCategories->count() > 0)
        {
          foreach($category->ProductSubCategories as $sub_category)
          {
            $sub_row['id']          = $sub_category->id;
            $sub_row['name']        = $sub_category->name;
            $sub_row['description'] = $sub_category->description;
            $sub_row['image']       = ($sub_category->image != '')?asset('public/uploads/product_subcategories/'.$sub_category->image):'';

            array_push($row['sub_categories'], $sub_row);
          }
        }
        $row['sub_categories'] = $row['sub_categories'];

        array_push($json, $row);
      }
      return response()->json([
        'code'    => 200,
        'message' => trans('api.product_categories_fetched_successfully'),
        'data'    => $json,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_product_categories_found'),
        'data'    => [],
      ]);
    }
  }
  /* END OF PRODUCT CATEGORY LISTING*/

  /* START OF PRODUCT CATEGORY LISTING*/
  public function productoGenerals()
  {
    $rows = ProductoGenerals::all();
    if(!empty($rows) && $rows->count() > 0) {
      $json = [];
      foreach($rows as $row) {
        $json[] = ['id' => $row->id, 'name' => $row->name];
      }
      return response()->json([
        'code'    => 200,
        'message' => trans('api.product_categories_fetched_successfully'),
        'data'    => $json,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_product_categories_found'),
        'data'    => [],
      ]);
    }
  }
  /* END OF PRODUCT CATEGORY LISTING*/

  /* START OF PRODUCT LISTING*/
  public function products(Request $request)
  {
    $search  = ($request->search)?$request->search:false;
    $length  = ($request->length && $request->start != '')?$request->length:false;
    $start   = ($length && $request->start && $request->start != '')?$request->start:false;
    $order   = $request->order;
    $columns = $request->columns;
    if(empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] === '' || $order[0]['dir'] === '')
    {
      $order = false;
    }

    $user           = Auth::user();
    $is_active      = ($user->user_type != config('global.ADMIN_USER'))?config('global.ACTIVE'):false;
    if($user->user_type == config('global.ADMIN_USER') && $request->is_active)
    {
      $is_active = config('global.ACTIVE');
    }
    $category_id    = ($request->category_id && $request->category_id != '')?$request->category_id:false;
    $subcategory_id = ($request->subcategory_id && $request->subcategory_id != '')?$request->subcategory_id:false;
    $brand          = ($request->brand && !empty($request->brand))?$request->brand:false;
    $band           = ($request->band && !empty($request->band))?$request->band:false;
    $treatments     = ($request->treatments && !empty($request->treatments))?$request->treatments:false;
    $ingredients    = ($request->ingredients && !empty($request->ingredients))?$request->ingredients:false;
    $grade          = ($request->grade && !empty($request->grade))?$request->grade:false;
    $diameter       = ($request->diameter && !empty($request->diameter))?$request->diameter:false;
    $product_length = ($request->product_length && !empty($request->product_length))?$request->product_length:false;
    $gasoline_grade = ($request->gasoline_grade && !empty($request->gasoline_grade))?$request->gasoline_grade:false;
    $searchTerm     = (isset($search['value']) && $search['value'] != "")?$search['value']:false;

    // FIND SUB CATEGORY
    if($subcategory_id)
    {
      $subcategory = ProductSubCategories::find($subcategory_id);
      $category_id = (!empty($subcategory))?$subcategory->category_id:false;
    }

    // FIND CATEGORY
    if($category_id)
    {
      $category = ProductCategories::find($category_id); 
    }
    
    $query = Products::join(DB::raw('product_sub_categories'), function($join) {
                        $join->on('product_sub_categories.id', '=', 'products.subcategory_id')
                          ->where('product_sub_categories.deleted_at', '=', null);
                       })
                     ->join(DB::raw('product_categories'), function($join) {
                        $join->on('product_categories.id', '=', 'product_sub_categories.category_id')
                          ->where('product_categories.deleted_at', '=', null);
                       })
                     ->when($is_active, function($query, $is_active){
                        return $query->where('products.is_active', $is_active);
                       })
                     ->when($category_id, function($query, $category_id){
                        return $query->where('product_categories.id', $category_id);
                       })
                     ->when($subcategory_id, function($query, $subcategory_id){
                        return $query->where('product_sub_categories.id', $subcategory_id);
                       })
                     ->when($brand, function($query, $brand){
                        return $query->whereIn('products.brand', $brand);
                       })
                     ->when($band, function($query, $band){
                        return $query->whereIn('products.band', $band);
                      })
                     ->when($treatments, function($query, $treatments){
                        return $query->whereIn('products.treatments', $treatments);
                      })
                     ->when($ingredients, function($query, $ingredients){
                        return $query->whereIn('products.ingredients', $ingredients);
                      })
                     ->when($grade, function($query, $grade){
                        return $query->whereIn('products.grade', $grade);
                      })
                     ->when($diameter, function($query, $diameter){
                        return $query->whereIn('products.diameter', $diameter);
                      })
                     ->when($product_length, function($query, $product_length){
                        return $query->whereIn('products.length', $product_length);
                      })
                     ->when($gasoline_grade, function($query, $gasoline_grade){
                        return $query->whereIn('products.gasoline_grade', $gasoline_grade);
                      })
                     ->when($searchTerm, function($q) use ($searchTerm){
                        return $q->where(function($sub_q) use ($searchTerm) {
                          $sub_q->where('products.name', 'like', '%'.$searchTerm.'%')
                                ->orWhere('product_categories.name', 'like', '%'.$searchTerm.'%')
                                ->orWhere('product_sub_categories.name', 'like', '%'.$searchTerm.'%');
                        });
                      });
    
    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                    return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                  })
                  ->when($start, function($q) use ($start){
                    return $q->skip($start);
                  })
                  ->when($length, function($q) use ($length){
                    return $q->take($length);
                  })
                  ->get(['products.*']);

    $data = [];

    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['id']               = $result->id;
        $row['custom_id']               = $result->custom_id;
        $row['name']             = $result->name;
        $row['description']      = $result->description;
        $row['unit_of_measure']  = $result->unit_of_measure;
        $row['ingredients']      = ($result->ingredients)?$result->ingredients:'';
        $row['category_name']    = $result->productSubCategories->productCategories->name;
        $row['subcategory_name'] = $result->productSubCategories->name;
        $row['image']            = asset('public/uploads/products/'.$result->image);
        $row['is_active']        = $result->is_active;
        $row['state_boolean']    = ($result->is_active == config('global.ACTIVE'))?true:false;
        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.products_fetched_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'category_name'   => (!empty($category))?$category->name:'',
        'subcategory_name'=> (!empty($subcategory))?$subcategory->name:'',
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_products_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'category_name'   => (!empty($category))?$category->name:'',
        'subcategory_name'=> (!empty($subcategory))?$subcategory->name:'',
        'data'    => [],
      ]);
    }
  }
  /* END OF PRODUCT LISTING*/

  public function searchProductsbyName(Request $request, $search = '') {
    
    if (!$search || strlen($search) < 3) {
      return response()->json([
        'code'    => 404,
        'message' => 'Invalid search term',
      ]);
    }

    $query = 'SELECT `products`.`id`, `products`.`name`, 
                IF( `products`.`name` LIKE "'.$search.'", 20, IF(`products`.`name` LIKE "%'.$search.'%", 15, 0) )
                + IF(`product_categories`.`name` LIKE "%'.$search.'%", 10, 0)
                + IF(`product_sub_categories`.`name` LIKE "%'.$search.'%", 5, 0)
                + IF(`products`.`description` LIKE "%'.$search.'%", 1, 0)
              AS `weight`
              FROM `products`
              JOIN `product_sub_categories` ON `product_sub_categories`.`id` = `products`.`subcategory_id`
              JOIN `product_categories` ON `product_categories`.`id` = `product_sub_categories`.`category_id`
              WHERE ( `products`.`name` LIKE "%'.$search.'%"
                      OR `products`.`description` LIKE "%'.$search.'%"
                      OR `product_categories`.`name` LIKE "%'.$search.'%"
                      OR `product_sub_categories`.`name` LIKE "%'.$search.'%" )
              GROUP BY `products`.`id`
              ORDER BY `weight` DESC, `products`.`name` ASC';
    $products = DB::select($query);
    return response()->json([
      'code'    => 200,
      'message' => trans('api.product_fetched_successfully'),
      'data'    => $products
    ]);
  }

  public function searchProductoGenerals(Request $request) {
    $search = $request->search;
    $generals = ProductoGenerals::where([['name', 'LIKE', '%'.$search.'%']])->get();
    return response()->json([
      'code'    => 200,
      'message' => '',
      'data'    => $generals
    ]);
  }
  /* START OF PRODUCT DETAIL */
  public function productDetail(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'product_id' => 'required|exists:products,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $product = Products::find($request->product_id);
    $data    = [
                'id'              => $product->id,
                'custom_id'       => $product->custom_id,
                'name'            => $product->name,
                'category_id'     => $product->productSubCategories->category_id,
                'category_name'     => $product->productSubCategories->productCategories->name,
                'subcategory_id'  => $product->subcategory_id,
                'subcategory_name'     => $product->productSubCategories->name,
                'producto_general_id'  => $product->producto_general_id,
                'producto_general_name'     => ($product->productoGenerals) ? $product->productoGenerals->name : '',
                'description'     => $product->description,
                'unit_of_measure' => $product->unit_of_measure,
                'image'           => asset('public/uploads/products/'.$product->image),
                'brand'           => [
                                      'value'         => ($product->brand)?(string)$product->brand:'',
                                      'is_applicable' => boolval($product->brand),
                                     ],
                'band'            => [
                                      'value'         => ($product->band)?(string)$product->brand:'',
                                      'is_applicable' => boolval($product->band),
                                      'name'          => ($product->band)?$product->product_band->name:''
                                     ],
                'treatments'      => [
                                      'value'         => ($product->treatments)?(string)$product->treatments:'',
                                      'is_applicable' => boolval($product->treatments),
                                      'name'          => ($product->treatments)?$product->product_treatment->name:''
                                     ],
                'ingredients'     => [
                                      'value'         => ($product->ingredients)?(string)$product->ingredients:'',
                                      'is_applicable' => boolval($product->ingredients),
                                     ],
                'attribute_1'     => [
                                      'value'         => ($product->ingredients)?(string)$product->attribute_1:'',
                                      'is_applicable' => boolval($product->attribute_1),
                                     ],
                'grade'           => [
                                      'value'         => ($product->grade)?(string)$product->grade:$product->grade,
                                      'is_applicable' => boolval($product->grade),
                                     ],
                'diameter'        => [
                                      'value'         => ($product->diameter)?(string)$product->diameter:'',
                                      'is_applicable' => boolval($product->diameter),
                                      'name'          => ($product->diameter)?$product->product_diameter->name:''
                                     ],
                'length'          => [
                                      'value'         => ($product->length)?(string)$product->length:'',
                                      'is_applicable' => boolval($product->length),
                                      'name'          => ($product->length)?$product->product_length->name:''
                                     ],
                'gasoline_grade'  => [
                                      'value'         => ($product->gasoline_grade)?(string)$product->gasoline_grade:'',
                                      'is_applicable' => boolval($product->gasoline_grade),
                                     ],
                'attribute_2'     => [
                                      'value'         => ($product->attribute_2)?(string)$product->attribute_2:'',
                                      'is_applicable' => boolval($product->attribute_2),
                                     ],
                'price'           => $product->price,
                'state'           => ($product->is_active == config('global.ACTIVE'))?true:false,
               ];

    return response()->json([
      'code'    => 200,
      'message' => trans('api.product_fetched_successfully'),
      'data'    => (object)$data,
    ]);
  }
  /* END OF PRODUCT DETAIL */

  /* START OF PAYMENT METHOD LIST */
  public function paymentMethods()
  {
    $paymentMethods = PaymentMethods::where(['is_active' => config('global.ACTIVE')])->get();

    if(!empty($paymentMethods) && $paymentMethods->count() > 0)
    {
      $json = [];
      foreach($paymentMethods as $paymentMethod)
      {
        $row['id']          = $paymentMethod->id;
        $row['name']        = $paymentMethod->name;
        $row['description'] = $paymentMethod->description;

        array_push($json, $row);
      }
      return response()->json([
        'code'    => 200,
        'message' => trans('api.payment_method_fetched_successfully'),
        'data'    => $json,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_payment_method_found'),
        'data'    => [],
      ]);
    }
  }
  /* END OF PAYMENT METHOD LIST */

  /* START OF ORDERS LIST */
  public function orders(Request $request)
  {
    $search  = ($request->search)?$request->search:false;
    $length  = ($request->length && $request->start != '')?$request->length:false;
    $start   = ($length && $request->start && $request->start != '')?$request->start:false;
    $columns = $request->columns;
    $order   = $request->order;
    if(empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }

    $user = Auth::user();

    $orderStatus = ($request->order_status && $request->order_status != '')?$request->order_status:false; //ORDER STATUS
    $agronomiaID = ($user->user_type == config('global.BUSINESS_USER'))?$user->id:false; //ORDER STATUS
    $producerID  = ($user->user_type == config('global.CUSTOMER_USER'))?$user->id:false; //ORDER STATUS
    $searchTerm  = (isset($search['value']) && $search['value'] != "")?$search['value']:false;
    
    $query       = Orders::join('products', function($join) {
                            $join->on('products.id', '=', 'orders.product_id')
                                 ->where('products.deleted_at', '=', null);
                           })
                         ->join('payment_methods', function($join) {
                            $join->on('payment_methods.id', '=', 'orders.payment_method_id')
                                 ->where('payment_methods.deleted_at', '=', null);
                           })
                         ->join(DB::raw('users as agronomia'), function($join) {
                            $join->on('agronomia.id', '=', 'orders.agronomia_id')
                                 ->where('agronomia.deleted_at', '=', null);
                           })
                         ->join(DB::raw('users as producer'), function($join) {
                            $join->on('producer.id', '=', 'orders.producer_id')
                                 ->where('producer.deleted_at', '=', null);
                          })
                         ->when($orderStatus, function ($q) use ($orderStatus) {
                            return $q->where('orders.order_status', '=', $orderStatus);
                          })
                         ->when($agronomiaID, function ($q) use ($agronomiaID) {
                            return $q->where('orders.agronomia_id', '=', $agronomiaID);
                          })
                         ->when($producerID, function ($q) use ($producerID) {
                            return $q->where('orders.producer_id', '=', $producerID);
                          })
                        ->when($searchTerm, function ($q) use ($searchTerm) {
                          $q->where(function ($sub_query) use ($searchTerm) {
                            $sub_query->where('orders.custom_order_no', 'like', '%'.$searchTerm.'%')  
                                      ->orWhere('producer.business_name', 'like', '%'.$searchTerm.'%')
                                      ->orWhere('agronomia.business_name', 'like', '%'.$searchTerm.'%')
                                      ->orWhere('products.name', 'like', '%'.$searchTerm.'%')
                                      ->orWhere('payment_methods.name', 'like', '%'.$searchTerm.'%');
                          });
                        });

    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                            return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                          })
                        ->when($start, function($q) use ($start){
                          return $q->skip($start);
                        })
                        ->when($length, function($q) use ($length){
                          return $q->take($length);
                        })
                        ->get(['orders.*']);

    $data = [];
    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['id']              = $result->id;
        $row['order_date']      = YMDtoDMY($result->created_at);
        $row['custom_order_no'] = $result->custom_order_no;

        // IF CURRENT USER IS NOT PRODUCER
        if($user->user_type != config('global.CUSTOMER_USER'))
        {
          $row['producer_id']            = $result->producer_id;
          $row['producer_business_name'] = $result->producer->business_name;
          $row['producer_custom_id']     = $result->producer->custom_id;
        }

        // IF CURRENT USER IS NOT BUSINESS
        if($user->user_type != config('global.BUSINESS_USER'))
        {
          $row['agronomia_id']            = $result->agronomia_id;
          $row['agronomia_business_name'] = $result->agronomia->business_name;
          $row['agronomia_custom_id']     = $result->agronomia->custom_id;
        }

        $row['product_id']            = $result->product_id;
        $row['product_name']          = $result->product->name;
        $row['unit_of_measure']       = $result->product->unit_of_measure;
        $row['price']                 = $result->bonus_price;
        $row['ordered_quantity']      = $result->ordered_quantity;
        $row['total_price']           = number_format($result->total_price, 2, ',', '');
        $row['payment_method_id']     = $result->payment_method_id;
        $row['payment_method_name']   = $result->paymentMethod->name;
        $row['date_of_delivery']      = YMDtoDMY($result->date_of_delivery);
        $row['order_status']          = $result->order_status;
        $row['approval_date']         = YMDtoDMY($result->approval_date);
        $row['order_status_label']    = $result->order_status_label;
        $row['producer_chat_status']  = $result->producer_chat_status;  // PRODUCER CAN NOT SEND MESSAGE IF DISABLED, AGRONOMIA HAVE RIGHT TO CHANGE IT
        $row['agronomia_chat_status'] = $result->agronomia_chat_status; // AGRONOMIA CAN NOT SEND MESSAGE IF DISABLED, PRODUCER HAVE RIGHT TO CHANGE IT
        $row['master_chat_status']    = $result->master_chat_status; // NO ONE CAN SEND MESSAGE IF DISABLED, ONLY ADMIN HAVE RIGHT TO CHANGE IT

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.orders_retrived_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_orders_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'    => [],
      ]);
    }
  }
  /* END OF ORDERS LIST */

  /* START OF ORDER DETAIL */
  public function orderDetail(Orders $order)
  {
    $user = Auth::user();

    // if not authorised user
    if($order->agronomia_id != $user->id && $order->producer_id != $user->id)
    {
      return response()->json(['code'    => 400, 'message' => trans('api.its_not_your_order')]);
    }
    
    $data = [
      'id' => (string)$order->id,
      'custom_order_no' => (string)$order->custom_order_no,
      'product' => (string)$order->product->name,
      'producer' => [
        'id' => (string)$order->producer_id,
        'custom_id' => (string)$order->producer->custom_id,
      ],
      'agronomia' => [
        'id' => (string)$order->agronomia_id,
        'custom_id' => (string)$order->agronomia->custom_id,
      ],
      'ordered_quantity' => (string)$order->ordered_quantity,
      'unit_of_measure' => (string)$order->product->unit_of_measure,
      'total_price' => (string)$order->total_price,
      'payment_method' => (string)$order->paymentMethod->name,
      'delivery_location' => (string)$order->delivery_location->name,
      'date_of_delivery' => (string)$order->date_of_delivery,
      'order_status' => (string)$order->order_status,
      'created_at' => (string)$order->created_at,
    ];

    return response()->json([
      'code'    => 200,
      'message' => trans('api.order_retrived'),
      'data'    => $data,
    ]);             
  }
  /* END OF ORDER DETAIL */

  /* START OF ENABLE / DISABLE CHAT */
  public function enableDisableChat(Request $request)
  {
    $user = Auth::user();
    
    $validator = Validator::make($request->all(), [
      'order_id'     => 'required|exists:orders,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $order = Orders::find($request->order_id);
    $user  = Auth::user();

    // IF ORDER IS NOT APPROVED
    if($order->order_status != config('global.APPROVED_ORDER_STATUS'))
    {
      return response()->json([
        'code'    => 404,
        'message' => __('api.order_is_not_approved'),
      ]);
    }

    // IF LOGGED IN USER IS ADMIN OR AGRONOMIA OR PRODUCER OF SELECTED USER
    if($user->user_type == config('global.ADMIN_USER') || $order->agronomia_id == $user->id || $order->producer_id == $user->id)
    {
      if($user->user_type == config('global.BUSINESS_USER')) //IF AGRONOMIA, ENABLE/DISABLE PRODUCER CHAT STATUS
      {
        $data['producer_chat_status']  = ($order->producer_chat_status == config('global.ACTIVE'))?config('global.DISABLED'):config('global.ACTIVE');
        $message = ($order->producer_chat_status == config('global.ACTIVE'))?__('api.chat_disabled'):__('api.chat_enabled');
      }
      else if($user->user_type == config('global.CUSTOMER_USER')) //IF PRODUCER, ENABLE/DISABLE AGRONOMIA CHAT STATUS
      {
        $data['agronomia_chat_status'] = ($order->agronomia_chat_status == config('global.ACTIVE'))?config('global.DISABLED'):config('global.ACTIVE');
        $message = ($order->agronomia_chat_status == config('global.ACTIVE'))?__('api.chat_disabled'):__('api.chat_enabled');
      }
      else //IF ADMIN, ENABLE/DISABLE MASTER CHAT STATUS
      {
        $data['master_chat_status']    = ($order->master_chat_status == config('global.ACTIVE'))?config('global.DISABLED'):config('global.ACTIVE');
        $message = ($order->master_chat_status == config('global.ACTIVE'))?__('api.chat_disabled'):__('api.chat_enabled');
      }
      $order->update($data);
      
      return response()->json([
        'code'    => 200,
        'message' => $message,
      ]);
    }
    else 
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }
    
  }
  /* END OF ENABLE / DISABLE CHAT */

  /* START OF CHAT MESSAGES : RETRIVE */
  public function orderChatMessages(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $primaryUser   = Auth::user();
    $order         = Orders::find($request->order_id);

    //IF LOGGED IN USER TYPE IS ADMIN OR BELONGS TO ORDER AGRONOMIA OR ORDER PRODUCER
    if($primaryUser->user_type == config('global.ADMIN_USER') || $order->producer_id == $primaryUser->id || $order->agronomia_id == $primaryUser->id)
    {
      $results = Messages::where('order_id', $order->id)
                          ->orderBy('id', 'ASC')
                          ->get();

      $messages = [];
      foreach($results as $result)
      {
        $row['id']          = $result->id;
        $row['sender_id']   = $result->sender_id;
        $row['receiver_id'] = $result->receiver_id;
        $row['text']        = $result->text;
        $row['is_read']     = $result->is_read;
        $row['created_at']  = YMDtoDMY($result->created_at);
        array_push($messages, $row);
      }

      $data['messages'] = $messages;

      //IF LOGGED IN USER IS NOT ADMIN
      if($primaryUser->user_type != config('global.ADMIN_USER'))
      {
        //FIND SECONDARY USER
        if($primaryUser->user_type == config('global.CUSTOMER_USER'))
        {
          $secondaryUser = User::find($order->agronomia_id); //IF LOGGED IN USER IS PRODUCER, THEN SECONDARY USER WILL BE AGRONOMIA
        }
        else  //IF LOGGED IN USER IS AGRONOMIA, THEN SECONDARY USER WILL BE PRODUCER
        {
          $secondaryUser = User::find($order->producer_id);
        }

        Messages::where(['receiver_id' => $primaryUser->id, 'order_id' => $order->id])->update(['is_read' => config('global.MESSAGE_READ')]);
        MessageRead::dispatch($order->id, $primaryUser->id);

        $data['sender_id']    = $secondaryUser->id;
        $data['sender_name']  = $secondaryUser->name;
        $data['sender_avtar'] = $secondaryUser->avtar_path;
      }
      else
      {
        $data['sender_id']    = $order->agronomia_id;
        $data['sender_name']  = $order->agronomia->business_name;
        $data['sender_avtar'] = $order->agronomia->avtar_path;
        
        $data['receiver_id']    = $order->producer_id;
        $data['receiver_name']  = $order->producer->business_name;
        $data['receiver_avtar'] = $order->producer->avtar_path;
      }
      
      return response()->json([
        'code'    => 200,
        'message' => trans('api.messages_retrived_successfully'),
        'data'    => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }
  }
  /* END OF CHAT MESSAGES : RETRIVE */

  /* START OF CHAT SEND MESSAGES */
  public function sendOrderChatMessage(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
      'text'     => 'required',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $sender   = Auth::user();
    $order    = Orders::find($request->order_id);

    // RETURN ERROR IF ORDER IS NOT APPROVED
    if($order->order_status != config('global.APPROVED_ORDER_STATUS'))
    {
      return response()->json([
        'code'    => 200,
        'message' => trans('api.order_is_not_approved_cant_send_message')
      ]);
    }

    //IF (SENDER == PRODUCER && ORDER PRODUCTER ID == SENDER) OR (SENDER == AGRONOMIA && ORDER AGRONOMIA ID == SENDER)
    if(($sender->user_type == config('global.CUSTOMER_USER') && $order->producer_id == $sender->id) || ($sender->user_type == config('global.BUSINESS_USER') && $order->agronomia_id == $sender->id))
    {
      //IF CHAT IS NOT ENABLED
      if($order->master_chat_status != config('global.ACTIVE'))
      {
        return response()->json([
          'code'    => 404,
          'message' => __('api.message_not_sent_chat_is_disabled_by_admin'),
        ]);
      }
      else if($sender->user_type == config('global.CUSTOMER_USER') && $order->producer_chat_status != config('global.ACTIVE'))
      {
        return response()->json([
          'code'    => 404,
          'message' => __('api.message_not_sent_chat_is_disabled_by_agronomia'),
        ]);
      }
      else if($sender->user_type == config('global.BUSINESS_USER') && $order->agronomia_chat_status != config('global.ACTIVE'))
      {
        return response()->json([
          'code'    => 404,
          'message' => __('api.message_not_sent_chat_is_disabled_by_producer'),
        ]);
      }

      //FIND RECEIVER
      if($sender->user_type == config('global.CUSTOMER_USER'))
      {
        $receiver = User::find($order->agronomia_id); //IF SENDER IS PRODUCER, THEN RECEIVER WILL BE AGRONOMIA
      }
      else  //IF SENDER IS AGRONOMIA, THEN RECEIVER WILL BE PRODUCER
      {
        $receiver = User::find($order->producer_id);
      }

      $data = [
              'order_id'    => $request->order_id,
              'sender_id'   => $sender->id,
              'receiver_id' => $receiver->id,
              'text'        => $request->text
            ];

      $response = Messages::create($data);
      MessageSent::dispatch($response);

      /* SEND PUSH NOTIFICATION TO RECEIVER */
      $this->sendFcmPushNotification(
        [ User::find($receiver->id)->toArray() ],
        [ 'message'  => str_replace(':order_no', $order->custom_order_no, __('labels.push_notification.chat_message')) ],
        [
          'type'    => 'chat',
          'order_id' => $order->id
        ]);

      /* SEND MAIL TO RECEIVER */
      \Mail::to($receiver->email)->send(new \App\Mail\OrderChatMessage($order, $receiver));

      return response()->json([
        'code'    => 200,
        'message' => trans('api.message_sent_successfully')
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }
  }
  /* END OF CHAT SEND MESSAGES */

  /* START OF REVIEW ORDER */
  public function reviewOrder(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'order_id' => 'required|exists:orders,id,deleted_at,NULL',
      'ratings'  => 'required|integer|min:1|max:5',
      'comment'  => 'required',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $sender   = Auth::user();
    $order    = Orders::find($request->order_id);

    // RETURN ERROR IF ORDER IS NOT APPROVED
    if($order->order_status != config('global.APPROVED_ORDER_STATUS'))
    {
      return response()->json([
        'code'    => 200,
        'message' => trans('api.order_is_not_approved_cant_review_it')
      ]);
    }

    //IF (SENDER == PRODUCER && ORDER PRODUCTER ID == SENDER) OR (SENDER == AGRONOMIA && ORDER AGRONOMIA ID == SENDER)
    if(($sender->user_type == config('global.CUSTOMER_USER') && $order->producer_id == $sender->id) || ($sender->user_type == config('global.BUSINESS_USER') && $order->agronomia_id == $sender->id))
    {
      //FIND RECEIVER
      if($sender->user_type == config('global.CUSTOMER_USER'))
      {
        $receiver = User::find($order->agronomia_id); //IF SENDER IS PRODUCER, THEN RECEIVER WILL BE AGRONOMIA
      }
      else  //IF SENDER IS AGRONOMIA, THEN RECEIVER WILL BE PRODUCER
      {
        $receiver = User::find($order->producer_id);
      }

      // CHECK IF ALREADY ADDED
      $review  = UserReviews::where(['user_id' => $sender->id, 'order_id' => $request->order_id]);
      $data = [
        'order_id'    => $request->order_id,
        'user_id'     => $sender->id,
        'receiver_id' => $receiver->id,
        'comment'     => $request->comment,
        'ratings'     => $request->ratings,
        'is_approved' => config('global.REVIEW_STATUS.PENDING'),
      ];

      if($review->count() > 0) //UPDATE IF ALREADY ADDED
      {
        $review->update($data);
      }
      else
      {
        UserReviews::create($data);
      }

      return response()->json([
        'code'    => 200,
        'message' => trans('api.order_reviewed')
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }
  }
  /* END OF REVIEW ORDER */

  /* START OF REVIEWS LIST */
  public function reviews(Request $request)
  {
    $search  = ($request->search)?$request->search:false;
    $length  = ($request->length && $request->start != '')?$request->length:false;
    $start   = ($length && $request->start && $request->start != '')?$request->start:false;
    $columns = ($request->columns)?$request->columns:false;
    $order   = ($columns && $request->order)?$request->order:false;
    if($order && empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }
    $user        = Auth::user();
    $agronomiaID = ($user->user_type == config('global.BUSINESS_USER'))?$user->id:false; //ORDER STATUS
    $producerID  = ($user->user_type == config('global.CUSTOMER_USER'))?$user->id:false; //ORDER STATUS
    $searchTerm  = (isset($search['value']) && $search['value'] != "")?$search['value']:false;
    
    $query       = Orders::where('orders.order_status', '=', config('global.APPROVED_ORDER_STATUS'))
                          ->leftJoin('user_reviews', function($join) use ($user) {
                              $join->on('user_reviews.order_id', '=', 'orders.id')
                                   ->where('user_reviews.user_id', '=', $user->id);
                           })
                          ->join(DB::raw('users as agronomia'), 'agronomia.id', '=', 'orders.agronomia_id')
                          ->join(DB::raw('users as producer'), 'producer.id', '=', 'orders.producer_id')
                          ->when($agronomiaID, function ($q) use ($agronomiaID) {
                            return $q->where('orders.agronomia_id', '=', $agronomiaID);
                          })
                          ->when($producerID, function ($q) use ($producerID) {
                            return $q->where('orders.producer_id', '=', $producerID);
                          })
                          ->when($searchTerm, function($q) use ($searchTerm){
                            return $q->where('orders.custom_order_no', 'like', '%'.$searchTerm.'%')
                            ->orWhere('producer.business_name', 'like', '%'.$searchTerm.'%')
                            ->orWhere('agronomia.business_name', 'like', '%'.$searchTerm.'%')
                            ->orWhere('user_reviews.comment', 'like', '%'.$searchTerm.'%');
                          });

    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                            return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                          })
                        ->when($start, function($q) use ($start){
                          return $q->skip($start);
                        })
                        ->when($length, function($q) use ($length){
                          return $q->take($length);
                        })
                        ->get(['orders.*', DB::raw('user_reviews.id as review_id'), 'user_reviews.ratings', 'user_reviews.comment', 'user_reviews.is_approved']);

    $data = [];
    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['order_id'] = $result->id;
        $row['custom_order_no'] = $result->custom_order_no;

        // IF CURRENT USER IS NOT PRODUCER
        if($user->user_type != config('global.CUSTOMER_USER'))
        {
          $row['producer_id']            = $result->producer_id;
          $row['producer_business_name'] = $result->producer->business_name;
          $row['producer_custom_id']     = $result->producer->custom_id;
        }

        // IF CURRENT USER IS NOT BUSINESS
        if($user->user_type != config('global.BUSINESS_USER'))
        {
          $row['agronomia_id']            = $result->agronomia_id;
          $row['agronomia_business_name'] = $result->agronomia->business_name;
          $row['agronomia_custom_id']      = $result->agronomia->custom_id;
        }

        $row['review_id']   = ($result->review_id)?(string)$result->review_id:'';
        $row['ratings']     = ($result->ratings)?(string)$result->ratings:'';
        $row['comment']     = ($result->comment)?(string)$result->comment:'';
        $row['is_approved'] = ($result->comment)?(string)$result->is_approved:'';
        $row['qualify']     = (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime($result->approval_date.'+'.config('global.REVIEW_HOURS').' hours')))?true:false;

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.reviews_retrived_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_approved_orders_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'    => [],
      ]);
    }
  }
  /* END OF REVIEWS LIST */

  public function myReviews(Request $request)
  {
    $user = Auth::user();

    if($user->user_type != config('ADMIN_USER'))
    {
      $agronomiaID = ($user->user_type == config('global.BUSINESS_USER'))?$user->id:false; //ORDER STATUS
      $producerID  = ($user->user_type == config('global.CUSTOMER_USER'))?$user->id:false; //ORDER STATUS

      $approved_orders  = Orders::where('orders.order_status', '=', config('global.APPROVED_ORDER_STATUS'))
                                ->when($agronomiaID, function ($q) use ($agronomiaID) {
                                    return $q->where('orders.agronomia_id', '=', $agronomiaID);
                                  })
                                ->when($producerID, function ($q) use ($producerID) {
                                    return $q->where('orders.producer_id', '=', $producerID);
                                  })
                                ->count();

      $cancelled_orders = Orders::where('orders.order_status', '=', config('global.CANCELLED_ORDER_STATUS'))
                                ->when($agronomiaID, function ($q) use ($agronomiaID) {
                                    return $q->where('orders.agronomia_id', '=', $agronomiaID);
                                  })
                                ->when($producerID, function ($q) use ($producerID) {
                                    return $q->where('orders.producer_id', '=', $producerID);
                                  })
                                ->count();
      
      $average_rating   = UserReviews::where(['is_approved' => config('global.REVIEW_STATUS.APPROVED'), 'receiver_id' => $user->id])->avg('ratings');

      $reviews          = UserReviews::where(['is_approved' => config('global.REVIEW_STATUS.APPROVED'), 'receiver_id' => $user->id])
                                     ->join('users', 'users.id', '=', 'user_reviews.user_id')
                                     ->get(['user_reviews.id', 'user_reviews.order_id', 'user_reviews.user_id', DB::raw('users.business_name as user_business_name'), 'user_reviews.ratings', 'user_reviews.comment']);
          
      return response()->json([
        'code'            => 200,
        'message'         => trans('api.reviews_retrived_successfully'),
        'data'            => [
                              'approved_orders'  => $approved_orders,
                              'cancelled_orders' => $cancelled_orders,
                              'average_rating'   => number_format($average_rating, 1, '.', ''),
                              'reviews'          => $reviews,
                             ],
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }
  }

  public function getReviewsByUser(Request $request)
  {
    $user_id = $request->user_id;
    $user = User::find($user_id);

    if ($user->user_type != config('ADMIN_USER'))
    {
      $agronomiaID = ($user->user_type == config('global.BUSINESS_USER')) ? $user->id : false; //ORDER STATUS
      $producerID  = ($user->user_type == config('global.CUSTOMER_USER')) ? $user->id : false; //ORDER STATUS

      $approved_orders  = Orders::where('orders.order_status', '=', config('global.APPROVED_ORDER_STATUS'))
                                ->when($agronomiaID, function ($q) use ($agronomiaID) {
                                    return $q->where('orders.agronomia_id', '=', $agronomiaID);
                                  })
                                ->when($producerID, function ($q) use ($producerID) {
                                    return $q->where('orders.producer_id', '=', $producerID);
                                  })
                                ->count();

      $cancelled_orders = Orders::where('orders.order_status', '=', config('global.CANCELLED_ORDER_STATUS'))
                                ->when($agronomiaID, function ($q) use ($agronomiaID) {
                                    return $q->where('orders.agronomia_id', '=', $agronomiaID);
                                  })
                                ->when($producerID, function ($q) use ($producerID) {
                                    return $q->where('orders.producer_id', '=', $producerID);
                                  })
                                ->count();
      
      $average_rating   = UserReviews::where(['is_approved' => config('global.REVIEW_STATUS.APPROVED'), 'receiver_id' => $user->id])->avg('ratings');

      $reviews          = UserReviews::where(['is_approved' => config('global.REVIEW_STATUS.APPROVED'), 'receiver_id' => $user->id])
                                     ->join('users', 'users.id', '=', 'user_reviews.user_id')
                                     ->get(['user_reviews.id', 'user_reviews.order_id', 'user_reviews.user_id', DB::raw('users.business_name as user_business_name'), 'user_reviews.ratings', 'user_reviews.comment']);
          
      return response()->json([
        'code'            => 200,
        'message'         => trans('api.reviews_retrived_successfully'),
        'data'            => [
                              'approved_orders'  => $approved_orders,
                              'cancelled_orders' => $cancelled_orders,
                              'average_rating'   => number_format($average_rating, 1, '.', ''),
                              'reviews'          => $reviews,
                             ],
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('login_messages.unauthorized_access'),
      ]);
    }
  }

  /* DASHBOARD DATA START */
  public function dashboardData(Request $request)
  {
    $user = Auth::user();
    
    if($user->user_type == config('global.ADMIN_USER') || $user->user_type == config('global.BUSINESS_USER'))
    {
      $fromDate  = ($request->fromDate != '')?substr($request->fromDate, 0, 10):false;
      $toDate    = ($request->toDate != '')?substr($request->toDate, 0, 10):false;
      $agronomia = ($user->user_type == config('global.BUSINESS_USER'))?$user->id:false;

      //MOST SELLING PRODUCTS
      $data['mostSellingProducts']  = Orders::join('products', 'products.id', '=', 'orders.product_id')
                                            ->when($fromDate, function ($q) use ($fromDate) {
                                                return $q->whereDate('orders.created_at', '>=', $fromDate);
                                              })
                                            ->when($toDate, function ($q) use ($toDate) {
                                                return $q->whereDate('orders.created_at', '<=', $toDate);
                                              })
                                            ->when($agronomia, function ($q) use ($agronomia) {
                                                return $q->where('orders.agronomia_id', '=', $agronomia);
                                              })
                                            ->where('orders.order_status', config('global.APPROVED_ORDER_STATUS'))
                                            ->groupBy('products.id')
                                            ->orderBy('ordered_quantity', 'DESC')
                                            ->take(10)
                                            ->get(['products.id', 'products.name', DB::raw('SUM(orders.ordered_quantity) as ordered_quantity')]);
                                    
      //MOST SELLING PROVINCES
      $data['mostSellingProvinces'] = Orders::join('users', 'users.id', '=', 'orders.producer_id')
                                            ->join('provinces', 'provinces.id', '=', 'users.province')
                                            ->when($fromDate, function ($q) use ($fromDate) {
                                                return $q->whereDate('orders.created_at', '>=', $fromDate);
                                              })
                                            ->when($toDate, function ($q) use ($toDate) {
                                                return $q->whereDate('orders.created_at', '<=', $toDate);
                                              })
                                            ->when($agronomia, function ($q) use ($agronomia) {
                                                return $q->where('orders.agronomia_id', '=', $agronomia);
                                              })
                                            ->where('orders.order_status', config('global.APPROVED_ORDER_STATUS'))
                                            ->groupBy('provinces.id')
                                            ->orderBy('ordered_quantity', 'DESC')
                                            ->take(10)
                                            ->get([DB::raw('provinces.name as province'), DB::raw('SUM(orders.ordered_quantity) as ordered_quantity')]);
      
      //BEST SELLING CATEGORIES
      $data['bestSellingCategories'] = Orders::join('products', 'products.id', '=', 'orders.product_id')
                                             ->join('product_sub_categories', 'product_sub_categories.id', '=', 'products.subcategory_id')
                                             ->join('product_categories', 'product_categories.id', '=', 'product_sub_categories.category_id')
                                             ->when($fromDate, function ($q) use ($fromDate) {
                                                return $q->whereDate('orders.created_at', '>=', $fromDate);
                                               })
                                             ->when($toDate, function ($q) use ($toDate) {
                                                return $q->whereDate('orders.created_at', '<=', $toDate);
                                               })
                                             ->when($agronomia, function ($q) use ($agronomia) {
                                                return $q->where('orders.agronomia_id', '=', $agronomia);
                                               })
                                             ->where('orders.order_status', config('global.APPROVED_ORDER_STATUS'))
                                             ->groupBy('product_categories.id')
                                             ->orderBy('ordered_quantity', 'DESC')
                                             ->take(10)
                                             ->get(['product_categories.id', 'product_categories.name', DB::raw('SUM(orders.ordered_quantity) as ordered_quantity')]);
      //APPROVED ORDERS
      $approved_orders  = Orders::where(['order_status' => config('global.APPROVED_ORDER_STATUS')])
                                ->when($fromDate, function ($q) use ($fromDate) {
                                    return $q->whereDate('orders.created_at', '>=', $fromDate);
                                  })
                                ->when($toDate, function ($q) use ($toDate) {
                                    return $q->whereDate('orders.created_at', '<=', $toDate);
                                  })
                                ->when($agronomia, function ($q) use ($agronomia) {
                                    return $q->where('orders.agronomia_id', '=', $agronomia);
                                  })
                                ->count();
                                
      $cancelled_orders = Orders::where(['order_status' => config('global.CANCELLED_ORDER_STATUS')])
                                ->when($fromDate, function ($q) use ($fromDate) {
                                    return $q->whereDate('orders.created_at', '>=', $fromDate);
                                  })
                                ->when($toDate, function ($q) use ($toDate) {
                                    return $q->whereDate('orders.created_at', '<=', $toDate);
                                  })
                                ->when($agronomia, function ($q) use ($agronomia) {
                                    return $q->where('orders.agronomia_id', '=', $agronomia);
                                  })
                                ->count();


      $data['approvedVsCancelledchartData'] = [
                                                [
                                                  'label' => __('labels.approved_orders_lbl'),
                                                  'count' => $approved_orders
                                                ],
                                                [
                                                  'label' => __('labels.cancelled_orders_lbl'),
                                                  'count' => $cancelled_orders
                                                ]
                                              ];

      return response()->json([
        'code'    => 200,
        'message' => __('labels.data_retrived'),
        'data'    => $data,
      ]);
    }

    return response()->json([
      'code'    => 404,
      'message' => __('login_messages.unauthorized_access')
    ]);
  }
  /* DASHBOARD DATA END */

  /* START OF CUSTOM PAGE */
  public function customPage(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'page_id' => 'required|exists:custom_pages,id,deleted_at,NULL',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $page = CustomPages::find($request->page_id);
    $data = [
            'name'    => $page->name,
            'content' => '<![CDATA['.$page->content.']]>',
           ];

    return response()->json([
      'code'    => 200,
      'message' => trans('api.page_content_fetched_successfully'),
      'data'    => (object)$data,
    ]);
  }
  /* END OF CUSTOM PAGE */

  /* START OF FAQ */
  public function faq(Request $request)
  {
    $search  = ($request->search)?$request->search:false;
    $length  = ($request->length && $request->start != '')?$request->length:false;
    $start   = ($length && $request->start && $request->start != '')?$request->start:false;
    $order   = $request->order;
    $columns = $request->columns;
    if(empty($order) || !isset($order[0]) || !isset($order[0]['column']) || !isset($order[0]['dir']) || $order[0]['column'] == '' || $order[0]['dir'] == '')
    {
      $order = false;
    }

    $user           = Auth::user();
    $is_active      = (!empty($user) > 0 && $user->user_type == config('global.ADMIN_USER'))?false:config('global.ACTIVE');
    $searchTerm     = (isset($search['value']) && $search['value'] != "")?$search['value']:false;

    $query = Faq::when($is_active, function($query, $is_active){
                    return $query->where('faq.is_active', $is_active);
                  })
                ->when($searchTerm, function($q) use ($searchTerm){
                    return $q->where(function($sub_q) use ($searchTerm) {
                      $sub_q->where('faq.question', 'like', '%'.$searchTerm.'%')
                            ->orWhere('faq.reply', 'like', '%'.$searchTerm.'%');
                    });
                  });
    
    $totalData  = $query->count();

    $results    = $query->when($order, function($q) use ($order, $columns){
                    return $q->orderBy($columns[$order[0]['column']], $order[0]['dir']);
                  })
                  ->when($start, function($q) use ($start){
                    return $q->skip($start);
                  })
                  ->when($length, function($q) use ($length){
                    return $q->take($length);
                  })
                  ->get();

    $data = [];

    if(!empty($results) && $results->count() > 0)
    {
      foreach($results as $result)
      {
        $row['id']             = $result->id;
        $row['question']       = $result->question;
        $row['reply']          = $result->reply;
        $row['order_priority'] = $result->order_priority;
        $row['is_active']      = $result->is_active;
        $row['state']          = ($result->is_active == config('global.ACTIVE'))?true:false;

        array_push($data, $row);
      }

      return response()->json([
        'code'            => 200,
        'message'         => trans('api.data_retrived_successfully'),
        'draw'            => $request->draw,
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalData,
        'data'            => $data,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 404,
        'message' => trans('api.no_data_found'),
        'draw'            => $request->draw,
        'recordsTotal'    => 0,
        'recordsFiltered' => 0,
        'data'    => [],
      ]);
    }
  }
  /* END OF FAQ */

  /* UNREAD NOTIFICATION COUNT */
  public function unreadNotificationCount()
  {
    $user = Auth::user();
    
    // FIND ALL THE ADMIN MESSAGES WHICH ARE ALREADY RECEIVED
    $notifications = Notifications::where('user_id', '=', $user->id)
                                  ->where('admin_message_id', '!=', null)
                                  ->get()
                                  ->pluck('admin_message_id');
    $notificationIds = ($notifications->count() > 0)?$notifications:false;

    $agronomia_id = ($user->user_type == config('global.BUSINESS_USER'))?$user->id:false;
    $producer_id = ($user->user_type == config('global.CUSTOMER_USER'))?$user->id:false;
    
    $messages = AdminMessages::where('created_at', '>=', $user->created_at) // exclude old messages
    ->when($notificationIds, function ($query) use ($notificationIds) {
      //EXCLUDE ALL NOTIFICATIONS, WHICH ARE ALREADY RECEIVED
      $query->whereNotIn('id', $notificationIds);
    })
    ->when($agronomia_id, function ($when_query) use ($agronomia_id) {
      $when_query->where(function ($where_query) use ($agronomia_id) {
        $where_query->where(function ($sub_query) use ($agronomia_id) {
          $sub_query->where('type', '=', config('global.ADMIN_MESSAGE.AGRONOMIA'))
          ->orWhere('type', '=', config('global.ADMIN_MESSAGE.ALL_USERS'))          
          ->orWhere(function ($specific_user_query) use ($agronomia_id){
            $specific_user_query->where('type', '=', config('global.ADMIN_MESSAGE.SPECIFIC_USER'))
            ->where('user_id', '=', $agronomia_id);
          });
        });
      });
    })
    ->when($producer_id, function ($when_query) use ($producer_id) {
      $when_query->where(function ($where_query) use ($producer_id) {
        $where_query->where(function ($sub_query) use ($producer_id) {
          $sub_query->where('type', '=', config('global.ADMIN_MESSAGE.PRODUCER'))
          ->orWhere('type', '=', config('global.ADMIN_MESSAGE.ALL_USERS'))
          ->orWhere(function ($specific_user_query) use ($producer_id){
            $specific_user_query->where('type', '=', config('global.ADMIN_MESSAGE.SPECIFIC_USER'))
            ->where('user_id', '=', $producer_id);
          });
        });
      });
    })
    ->get();

    if($messages->count() > 0)
    {
      foreach($messages as $message)
      {
        $data = [
          'user_id'          => $user->id,
          'admin_message_id' => $message->id,
          'type'             => ($message->type == config('global.ADMIN_MESSAGE.ADMIN_NOTIFICATION'))?$message->notification_type:config('global.NOTIFICATION.ADMIN_MESSAGE'),
          'message'          => $message->message,
        ];
        
        Notifications::create($data);
      }
    }

    $unreadNotification = Notifications::where(['user_id' => $user->id, 'is_read' => config('global.MESSAGE_UNREAD')])->count();
    
    return response()->json([
      'code'    => 200,
      'message' => ($unreadNotification > 0)?__('api.you_have_new_notifications'):__('api.no_new_notification'),
      'data'    => $unreadNotification,
    ]);
  }
  /* UNREAD NOTIFICATION COUNT END */

  /* NOTIFICATION LIST START */
  public function notifications()
  {
    $user = Auth::user();
    $notifications = Notifications::where(['user_id' => $user->id])
                                  ->orderBy('id', 'DESC')
                                  ->get();

    $data = [];
    if($notifications->count() > 0)
    {
      foreach($notifications as $notification)
      {
        $row = [
          'id'           => $notification->id,
          'message_type' => $notification->message_type,
          'message'      => $notification->message_text,
          'is_read'      => $notification->is_read,
          'click_action' => 'doNothing',
          'data' => ['order_id' => ''],
        ];

        if($notification->type == '2' || $notification->type == '3' || $notification->type == '4' || $notification->type == '5')
        {
          $row['click_action'] = 'openOrderDetail';
          $row['data']['order_id'] = (string)$notification->message;
        }

        array_push($data, $row);
      }
    }
    
    Notifications::where(['user_id' => $user->id])->update(['is_read' => config('global.MESSAGE_READ')]);
    
    return response()->json([
      'code'    => 200,
      'message' => __('api.notifications_retirived_successfully'),
      'data'    => $data,
    ]);
  }
  /* NOTIFICATION LIST END */

  /* START OF LOGOUT FUNCTION */
  public function logout()
  {
    Auth::logout();
    return response()->json([
      'code'    => 200,
      'message' => __('api.user_logged_out'),
    ]);
  }
  /* END OF LOGOUT FUNCTION */

  /* PRODUCT ATTRIBUTES */
  public function ProductAttributes()
  {
    $data['bands']      = ProductBands::all()->toArray();
    $data['diameters']  = ProductDiameters::all()->toArray();
    $data['lengths']    = ProductLengths::all()->toArray();
    $data['treatments'] = ProductTreatments::all()->toArray();

    return response()->json([
      'code'    => 200,
      'data'    => $data,
    ]);
  }
  /* END: PRODUCT ATTRIBUTES */

  /* START OF PRODUCT FILTER API */
  public function productFilterAttributes(Request $request)
  {
    $data = [];
    $subcategory_id          = ($request->subcategory_id)?$request->subcategory_id:false;
    $selectedBrands          = ($request->brand)?$request->brand:false;
    $selectedBands           = ($request->band)?$request->band:false;
    $selectedTreatments      = ($request->treatments)?$request->treatments:false;
    $selectedIngredients     = ($request->ingredients)?$request->ingredients:false;
    $selectedGrades          = ($request->grade)?$request->grade:false;
    $selectedDiameters       = ($request->diameter)?$request->diameter:false;
    $selectedProductLengths  = ($request->product_length)?$request->product_length:false;
    $selectedGasolineGrades  = ($request->gasoline_grade)?$request->gasoline_grade:false;
    $openedFilter            = ($request->opened_filter)?$request->opened_filter:'';
    
    $brands = Products::distinct()
              ->where('products.brand', '!=', '')
              ->where('products.brand', '!=', null)
              ->where('products.is_active', '=', config('global.ACTIVE'))
              ->when($subcategory_id, function($query) use ($subcategory_id){
                $query->where('products.subcategory_id', '=', $subcategory_id);
              })
              ->when($selectedBands, function($query) use ($selectedBands, $selectedBrands) {
                $query->where(function($subQuery) use ($selectedBands, $selectedBrands) {
                  $subQuery->whereIn('products.band', $selectedBands)
                           ->when($selectedBrands, function($query, $selectedBrands){
                            $query->orWhereIn('products.brand', $selectedBrands);
                           });      
                });
              })
              ->when($selectedTreatments, function($query) use ($selectedTreatments, $selectedBrands) {
                $query->where(function($subQuery) use ($selectedTreatments, $selectedBrands) {
                  $subQuery->whereIn('products.treatments', $selectedTreatments)
                           ->when($selectedBrands, function($query, $selectedBrands){
                            $query->orWhereIn('products.brand', $selectedBrands);
                           });      
                });
              })
              ->when($selectedIngredients, function($query) use ($selectedIngredients, $selectedBrands) {
                $query->where(function($subQuery) use ($selectedIngredients, $selectedBrands) {
                  $subQuery->whereIn('products.ingredients', $selectedIngredients)
                           ->when($selectedBrands, function($query, $selectedBrands){
                            $query->orWhereIn('products.brand', $selectedBrands);
                           });      
                });
              })
              ->when($selectedGrades, function($query) use ($selectedGrades, $selectedBrands) {
                $query->where(function($subQuery) use ($selectedGrades, $selectedBrands) {
                  $subQuery->whereIn('products.grade', $selectedGrades)
                           ->when($selectedBrands, function($query, $selectedBrands){
                            $query->orWhereIn('products.brand', $selectedBrands);
                           });      
                });
              })
              ->when($selectedDiameters, function($query) use ($selectedDiameters, $selectedBrands) {
                $query->where(function($subQuery) use ($selectedDiameters, $selectedBrands) {
                  $subQuery->whereIn('products.diameter', $selectedDiameters)
                           ->when($selectedBrands, function($query, $selectedBrands){
                            $query->orWhereIn('products.brand', $selectedBrands);
                           });      
                });
              })
              ->when($selectedProductLengths, function($query) use ($selectedProductLengths, $selectedBrands) {
                $query->where(function($subQuery) use ($selectedProductLengths, $selectedBrands) {
                  $subQuery->whereIn('products.length', $selectedProductLengths)
                           ->when($selectedBrands, function($query, $selectedBrands){
                            $query->orWhereIn('products.brand', $selectedBrands);
                           });      
                });
              })
              ->when($selectedGasolineGrades, function($query) use ($selectedGasolineGrades, $selectedBrands) {
                $query->where(function($subQuery) use ($selectedGasolineGrades, $selectedBrands) {
                  $subQuery->whereIn('products.gasoline_grade', $selectedGasolineGrades)
                           ->when($selectedBrands, function($query, $selectedBrands){
                            $query->orWhereIn('products.brand', $selectedBrands);
                           });      
                });
              })
              ->orderBy('products.brand', 'ASC')
              ->get([DB::raw('products.brand as label'), DB::raw('products.brand as value'), DB::raw('products.brand as value')]);

    if(!empty($brands) && $brands->count() > 0)
    {
      $selected = 0;
      foreach($brands as $key => $singleBrand)
      {
        $brands[$key]->is_selected = false;
        if($selectedBrands && in_array($singleBrand->value, $selectedBrands))
        {
          $brands[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.brand'),
        'name'      => 'brand',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'brand')?true:false,
        'attributes'=> $brands,
      ];
    }

    $bands =  Products::distinct()
              ->join('products_bands', 'products_bands.id', '=', 'products.band')
              ->where('products_bands.name', '!=', '')
              ->where('products.is_active', '=', config('global.ACTIVE'))
              ->when($subcategory_id, function($query) use ($subcategory_id){
                $query->where('products.subcategory_id', '=', $subcategory_id);
              })
              ->when($selectedBrands, function($query) use ($selectedBrands, $selectedBands) {
                $query->where(function($subQuery) use ($selectedBrands, $selectedBands) {
                  $subQuery->whereIn('products.brand', $selectedBrands)
                           ->when($selectedBands, function($query, $selectedBands){
                            $query->orWhereIn('products.band', $selectedBands);
                           });      
                });
              })
              ->when($selectedTreatments, function($query) use ($selectedTreatments, $selectedBands) {
                $query->where(function($subQuery) use ($selectedTreatments, $selectedBands) {
                  $subQuery->whereIn('products.treatments', $selectedTreatments)
                           ->when($selectedBands, function($query, $selectedBands){
                            $query->orWhereIn('products.band', $selectedBands);
                           });      
                });
              })
              ->when($selectedIngredients, function($query) use ($selectedIngredients, $selectedBands) {
                $query->where(function($subQuery) use ($selectedIngredients, $selectedBands) {
                  $subQuery->whereIn('products.ingredients', $selectedIngredients)
                           ->when($selectedBands, function($query, $selectedBands){
                            $query->orWhereIn('products.band', $selectedBands);
                           });      
                });
              })
              ->when($selectedGrades, function($query) use ($selectedGrades, $selectedBands) {
                $query->where(function($subQuery) use ($selectedGrades, $selectedBands) {
                  $subQuery->whereIn('products.grade', $selectedGrades)
                           ->when($selectedBands, function($query, $selectedBands){
                            $query->orWhereIn('products.band', $selectedBands);
                           });      
                });
              })
              ->when($selectedDiameters, function($query) use ($selectedDiameters, $selectedBands) {
                $query->where(function($subQuery) use ($selectedDiameters, $selectedBands) {
                  $subQuery->whereIn('products.diameter', $selectedDiameters)
                           ->when($selectedBands, function($query, $selectedBands){
                            $query->orWhereIn('products.band', $selectedBands);
                           });      
                });
              })
              ->when($selectedProductLengths, function($query) use ($selectedProductLengths, $selectedBands) {
                $query->where(function($subQuery) use ($selectedProductLengths, $selectedBands) {
                  $subQuery->whereIn('products.length', $selectedProductLengths)
                           ->when($selectedBands, function($query, $selectedBands){
                            $query->orWhereIn('products.band', $selectedBands);
                           });      
                });
              })
              ->when($selectedGasolineGrades, function($query) use ($selectedGasolineGrades, $selectedBands) {
                $query->where(function($subQuery) use ($selectedGasolineGrades, $selectedBands) {
                  $subQuery->whereIn('products.gasoline_grade', $selectedGasolineGrades)
                           ->when($selectedBands, function($query, $selectedBands){
                            $query->orWhereIn('products.band', $selectedBands);
                           });      
                });
              })
              ->orderBy('products_bands.name', 'ASC')
              ->get([DB::raw('products_bands.name as label'), DB::raw('products_bands.id as value')]);

    if(!empty($bands) && $bands->count() > 0)
    {
      $selected = 0;
      foreach($bands as $key => $singleBand)
      {
        $bands[$key]->is_selected = false;
        if($selectedBands && in_array($singleBand->value, $selectedBands))
        {
          $bands[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.band'),
        'name'      => 'band',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'band')?true:false,
        'attributes'=> $bands,
      ];
    }
  
    $treatments = Products::distinct()
                  ->join('products_treatments', 'products_treatments.id', '=', 'products.treatments')
                  ->where('products_treatments.name', '!=', '')
                  ->where('products.is_active', '=', config('global.ACTIVE'))
                  ->when($subcategory_id, function($query) use ($subcategory_id){
                    $query->where('products.subcategory_id', '=', $subcategory_id);
                  })
                  ->when($selectedBrands, function($query) use ($selectedBrands, $selectedTreatments) {
                    $query->where(function($subQuery) use ($selectedBrands, $selectedTreatments) {
                      $subQuery->whereIn('products.brand', $selectedBrands)
                               ->when($selectedTreatments, function($query, $selectedTreatments){
                                 $query->orWhereIn('products.treatments', $selectedTreatments);
                               });      
                    });
                  })
                  ->when($selectedBands, function($query) use ($selectedBands, $selectedTreatments) {
                    $query->where(function($subQuery) use ($selectedBands, $selectedTreatments) {
                      $subQuery->whereIn('products.band', $selectedBands)
                               ->when($selectedTreatments, function($query, $selectedTreatments){
                                 $query->orWhereIn('products.treatments', $selectedTreatments);
                               });      
                    });
                  })
                  ->when($selectedIngredients, function($query) use ($selectedIngredients, $selectedTreatments) {
                    $query->where(function($subQuery) use ($selectedIngredients, $selectedTreatments) {
                      $subQuery->whereIn('products.ingredients', $selectedIngredients)
                               ->when($selectedTreatments, function($query, $selectedTreatments){
                                 $query->orWhereIn('products.treatments', $selectedTreatments);
                               });      
                    });
                  })
                  ->when($selectedGrades, function($query) use ($selectedGrades, $selectedTreatments) {
                    $query->where(function($subQuery) use ($selectedGrades, $selectedTreatments) {
                      $subQuery->whereIn('products.grade', $selectedGrades)
                               ->when($selectedTreatments, function($query, $selectedTreatments){
                                 $query->orWhereIn('products.treatments', $selectedTreatments);
                               });      
                    });
                  })
                  ->when($selectedDiameters, function($query) use ($selectedDiameters, $selectedTreatments) {
                    $query->where(function($subQuery) use ($selectedDiameters, $selectedTreatments) {
                      $subQuery->whereIn('products.diameter', $selectedDiameters)
                               ->when($selectedTreatments, function($query, $selectedTreatments){
                                 $query->orWhereIn('products.treatments', $selectedTreatments);
                               });      
                    });
                  })
                  ->when($selectedProductLengths, function($query) use ($selectedProductLengths, $selectedTreatments) {
                    $query->where(function($subQuery) use ($selectedProductLengths, $selectedTreatments) {
                      $subQuery->whereIn('products.length', $selectedProductLengths)
                               ->when($selectedTreatments, function($query, $selectedTreatments){
                                 $query->orWhereIn('products.treatments', $selectedTreatments);
                               });      
                    });
                  })
                  ->when($selectedGasolineGrades, function($query) use ($selectedGasolineGrades, $selectedTreatments) {
                    $query->where(function($subQuery) use ($selectedGasolineGrades, $selectedTreatments) {
                      $subQuery->whereIn('products.gasoline_grade', $selectedGasolineGrades)
                               ->when($selectedTreatments, function($query, $selectedTreatments){
                                 $query->orWhereIn('products.treatments', $selectedTreatments);
                               });      
                    });
                  })
                  ->orderBy('products_treatments.name', 'ASC')
                  ->get([DB::raw('products_treatments.name as label'), DB::raw('products_treatments.id as value')]);

    if(!empty($treatments) && $treatments->count() > 0)
    {
      $selected = 0;
      foreach($treatments as $key => $singleTreatment)
      {
        $treatments[$key]->is_selected = false;
        if($selectedTreatments && in_array($singleTreatment->value, $selectedTreatments))
        {
          $treatments[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.treatments'),
        'name'      => 'treatments',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'treatments')?true:false,
        'attributes'=> $treatments,
      ];
    }

    $ingredients =  Products::distinct()
                    ->where('products.ingredients', '!=', '')
                    ->where('products.ingredients', '!=', null)
                    ->where('products.is_active', '=', config('global.ACTIVE'))
                    ->when($subcategory_id, function($query) use ($subcategory_id){
                      $query->where('products.subcategory_id', '=', $subcategory_id);
                    })
                    ->when($selectedBrands, function($query) use ($selectedBrands, $selectedIngredients) {
                      $query->where(function($subQuery) use ($selectedBrands, $selectedIngredients) {
                        $subQuery->whereIn('products.brand', $selectedBrands)
                                 ->when($selectedIngredients, function($query, $selectedIngredients){
                                   $query->orWhereIn('products.ingredients', $selectedIngredients);
                                 });      
                      });
                    })
                    ->when($selectedBands, function($query) use ($selectedBands, $selectedIngredients) {
                      $query->where(function($subQuery) use ($selectedBands, $selectedIngredients) {
                        $subQuery->whereIn('products.band', $selectedBands)
                                 ->when($selectedIngredients, function($query, $selectedIngredients){
                                   $query->orWhereIn('products.ingredients', $selectedIngredients);
                                 });      
                      });
                    })
                    ->when($selectedTreatments, function($query) use ($selectedTreatments, $selectedIngredients) {
                      $query->where(function($subQuery) use ($selectedTreatments, $selectedIngredients) {
                        $subQuery->whereIn('products.treatments', $selectedTreatments)
                                 ->when($selectedIngredients, function($query, $selectedIngredients){
                                   $query->orWhereIn('products.ingredients', $selectedIngredients);
                                 });      
                      });
                    })
                    ->when($selectedGrades, function($query) use ($selectedGrades, $selectedIngredients) {
                      $query->where(function($subQuery) use ($selectedGrades, $selectedIngredients) {
                        $subQuery->whereIn('products.grade', $selectedGrades)
                                 ->when($selectedIngredients, function($query, $selectedIngredients){
                                   $query->orWhereIn('products.ingredients', $selectedIngredients);
                                 });      
                      });
                    })
                    ->when($selectedDiameters, function($query) use ($selectedDiameters, $selectedIngredients) {
                      $query->where(function($subQuery) use ($selectedDiameters, $selectedIngredients) {
                        $subQuery->whereIn('products.diameter', $selectedDiameters)
                                 ->when($selectedIngredients, function($query, $selectedIngredients){
                                   $query->orWhereIn('products.ingredients', $selectedIngredients);
                                 });      
                      });
                    })
                    ->when($selectedProductLengths, function($query) use ($selectedProductLengths, $selectedIngredients) {
                      $query->where(function($subQuery) use ($selectedProductLengths, $selectedIngredients) {
                        $subQuery->whereIn('products.length', $selectedProductLengths)
                                 ->when($selectedIngredients, function($query, $selectedIngredients){
                                   $query->orWhereIn('products.ingredients', $selectedIngredients);
                                 });      
                      });
                    })
                    ->when($selectedGasolineGrades, function($query) use ($selectedGasolineGrades, $selectedIngredients) {
                      $query->where(function($subQuery) use ($selectedGasolineGrades, $selectedIngredients) {
                        $subQuery->whereIn('products.gasoline_grade', $selectedGasolineGrades)
                                 ->when($selectedIngredients, function($query, $selectedIngredients){
                                   $query->orWhereIn('products.ingredients', $selectedIngredients);
                                 });      
                      });
                    })
                    ->orderBy('products.ingredients', 'ASC')
                    ->get([DB::raw('products.ingredients as label'), DB::raw('products.ingredients as value')]);

    if(!empty($ingredients) && $ingredients->count() > 0)
    {
      $selected = 0;
      foreach($ingredients as $key => $singleIngredient)
      {
        $ingredients[$key]->is_selected = false;
        if($selectedIngredients && in_array($singleIngredient->value, $selectedIngredients))
        {
          $ingredients[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.active_principle'),
        'name'      => 'ingredients',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'ingredients')?true:false,
        'attributes'=> $ingredients,
      ];
    }

    $grades = Products::distinct()
              ->where('products.grade', '!=', '')
              ->where('products.grade', '!=', null)
              ->where('products.is_active', '=', config('global.ACTIVE'))
              ->when($subcategory_id, function($query) use ($subcategory_id){
                $query->where('products.subcategory_id', '=', $subcategory_id);
              })
              ->when($selectedBrands, function($query) use ($selectedBrands, $selectedGrades) {
                $query->where(function($subQuery) use ($selectedBrands, $selectedGrades) {
                  $subQuery->whereIn('products.brand', $selectedBrands)
                           ->when($selectedGrades, function($query, $selectedGrades){
                            $query->orWhereIn('products.grade', $selectedGrades);
                           });      
                });
              })
              ->when($selectedBands, function($query) use ($selectedBands, $selectedGrades) {
                $query->where(function($subQuery) use ($selectedBands, $selectedGrades) {
                  $subQuery->whereIn('products.band', $selectedBands)
                           ->when($selectedGrades, function($query, $selectedGrades){
                             $query->orWhereIn('products.grade', $selectedGrades);
                           });      
                });
              })
              ->when($selectedTreatments, function($query) use ($selectedTreatments, $selectedGrades) {
                $query->where(function($subQuery) use ($selectedTreatments, $selectedGrades) {
                  $subQuery->whereIn('products.treatments', $selectedTreatments)
                           ->when($selectedGrades, function($query, $selectedGrades){
                             $query->orWhereIn('products.grade', $selectedGrades);
                           });      
                });
              })
              ->when($selectedIngredients, function($query) use ($selectedIngredients, $selectedGrades) {
                $query->where(function($subQuery) use ($selectedIngredients, $selectedGrades) {
                  $subQuery->whereIn('products.ingredients', $selectedIngredients)
                           ->when($selectedGrades, function($query, $selectedGrades){
                             $query->orWhereIn('products.grade', $selectedGrades);
                           });      
                });
              })
              ->when($selectedDiameters, function($query) use ($selectedDiameters, $selectedGrades) {
                $query->where(function($subQuery) use ($selectedDiameters, $selectedGrades) {
                  $subQuery->whereIn('products.diameter', $selectedDiameters)
                           ->when($selectedGrades, function($query, $selectedGrades){
                             $query->orWhereIn('products.grade', $selectedGrades);
                           });      
                });
              })
              ->when($selectedProductLengths, function($query) use ($selectedProductLengths, $selectedGrades) {
                $query->where(function($subQuery) use ($selectedProductLengths, $selectedGrades) {
                  $subQuery->whereIn('products.length', $selectedProductLengths)
                           ->when($selectedGrades, function($query, $selectedGrades){
                             $query->orWhereIn('products.grade', $selectedGrades);
                           });      
                });
              })
              ->when($selectedGasolineGrades, function($query) use ($selectedGasolineGrades, $selectedGrades) {
                $query->where(function($subQuery) use ($selectedGasolineGrades, $selectedGrades) {
                  $subQuery->whereIn('products.gasoline_grade', $selectedGasolineGrades)
                           ->when($selectedGrades, function($query, $selectedGrades){
                             $query->orWhereIn('products.grade', $selectedGrades);
                           });      
                });
              })
              ->orderBy('products.grade', 'ASC')
              ->get([DB::raw('products.grade as label'), DB::raw('products.grade as value')]);

    if(!empty($grades) && $grades->count() > 0)
    {
      $selected = 0;
      foreach($grades as $key => $singleGrade)
      {
        $grades[$key]->is_selected = false;
        if($selectedGrades && in_array($singleGrade->value, $selectedGrades))
        {
          $grades[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.grade'),
        'name'      => 'grade',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'grade')?true:false,
        'attributes'=> $grades,
      ];
    }

    $diameters =  Products::distinct()
                  ->join('products_diameters', 'products_diameters.id', '=', 'products.diameter')
                  ->where('products_diameters.name', '!=', '')
                  ->where('products.is_active', '=', config('global.ACTIVE'))
                  ->when($subcategory_id, function($query) use ($subcategory_id){
                    $query->where('products.subcategory_id', '=', $subcategory_id);
                  })
                  ->when($selectedBrands, function($query) use ($selectedBrands, $selectedDiameters) {
                    $query->where(function($subQuery) use ($selectedBrands, $selectedDiameters) {
                      $subQuery->whereIn('products.brand', $selectedBrands)
                               ->when($selectedDiameters, function($query, $selectedDiameters){
                                $query->orWhereIn('products.diameter', $selectedDiameters);
                               });      
                    });
                  })
                  ->when($selectedBands, function($query) use ($selectedBands, $selectedDiameters) {
                    $query->where(function($subQuery) use ($selectedBands, $selectedDiameters) {
                      $subQuery->whereIn('products.band', $selectedBands)
                               ->when($selectedDiameters, function($query, $selectedDiameters){
                                 $query->orWhereIn('products.diameter', $selectedDiameters);
                               });      
                    });
                  })
                  ->when($selectedTreatments, function($query) use ($selectedTreatments, $selectedDiameters) {
                    $query->where(function($subQuery) use ($selectedTreatments, $selectedDiameters) {
                      $subQuery->whereIn('products.treatments', $selectedTreatments)
                               ->when($selectedDiameters, function($query, $selectedDiameters){
                                 $query->orWhereIn('products.diameter', $selectedDiameters);
                               });      
                    });
                  })
                  ->when($selectedIngredients, function($query) use ($selectedIngredients, $selectedDiameters) {
                    $query->where(function($subQuery) use ($selectedIngredients, $selectedDiameters) {
                      $subQuery->whereIn('products.ingredients', $selectedIngredients)
                               ->when($selectedDiameters, function($query, $selectedDiameters){
                                 $query->orWhereIn('products.diameter', $selectedDiameters);
                               });      
                    });
                  })
                  ->when($selectedGrades, function($query) use ($selectedGrades, $selectedDiameters) {
                    $query->where(function($subQuery) use ($selectedGrades, $selectedDiameters) {
                      $subQuery->whereIn('products.grade', $selectedGrades)
                               ->when($selectedDiameters, function($query, $selectedDiameters){
                                 $query->orWhereIn('products.diameter', $selectedDiameters);
                               });      
                    });
                  })
                  ->when($selectedProductLengths, function($query) use ($selectedProductLengths, $selectedDiameters) {
                    $query->where(function($subQuery) use ($selectedProductLengths, $selectedDiameters) {
                      $subQuery->whereIn('products.length', $selectedProductLengths)
                               ->when($selectedDiameters, function($query, $selectedDiameters){
                                 $query->orWhereIn('products.diameter', $selectedDiameters);
                               });      
                    });
                  })
                  ->when($selectedGasolineGrades, function($query) use ($selectedGasolineGrades, $selectedDiameters) {
                    $query->where(function($subQuery) use ($selectedGasolineGrades, $selectedDiameters) {
                      $subQuery->whereIn('products.gasoline_grade', $selectedGasolineGrades)
                               ->when($selectedDiameters, function($query, $selectedDiameters){
                                 $query->orWhereIn('products.diameter', $selectedDiameters);
                               });      
                    });
                  })
                  ->orderBy('products_diameters.id', 'ASC')
                  ->get([DB::raw('products_diameters.name as label'), DB::raw('products_diameters.id as value')]);
    if(!empty($diameters) && $diameters->count() > 0)
    {
      $selected = 0;
      foreach($diameters as $key => $singleDiameter)
      {
        $diameters[$key]->is_selected = false;
        if($selectedDiameters && in_array($singleDiameter->value, $selectedDiameters))
        {
          $diameters[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.diameter'),
        'name'      => 'diameter',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'diameter')?true:false,
        'attributes'=> $diameters,
      ];
    }
    
    $product_length = Products::distinct()
                      ->join('products_lengths', 'products_lengths.id', '=', 'products.length')
                      ->where('products_lengths.name', '!=', '')
                      ->where('products.is_active', '=', config('global.ACTIVE'))
                      ->when($subcategory_id, function($query) use ($subcategory_id){
                        $query->where('products.subcategory_id', '=', $subcategory_id);
                      })
                      ->when($selectedBrands, function($query) use ($selectedBrands, $selectedProductLengths) {
                        $query->where(function($subQuery) use ($selectedBrands, $selectedProductLengths) {
                          $subQuery->whereIn('products.brand', $selectedBrands)
                                   ->when($selectedProductLengths, function($query, $selectedProductLengths){
                                    $query->orWhereIn('products.length', $selectedProductLengths);
                                   });      
                        });
                      })
                      ->when($selectedBands, function($query) use ($selectedBands, $selectedProductLengths) {
                        $query->where(function($subQuery) use ($selectedBands, $selectedProductLengths) {
                          $subQuery->whereIn('products.band', $selectedBands)
                                   ->when($selectedProductLengths, function($query, $selectedProductLengths){
                                     $query->orWhereIn('products.length', $selectedProductLengths);
                                   });      
                        });
                      })
                      ->when($selectedTreatments, function($query) use ($selectedTreatments, $selectedProductLengths) {
                        $query->where(function($subQuery) use ($selectedTreatments, $selectedProductLengths) {
                          $subQuery->whereIn('products.treatments', $selectedTreatments)
                                   ->when($selectedProductLengths, function($query, $selectedProductLengths){
                                     $query->orWhereIn('products.length', $selectedProductLengths);
                                   });      
                        });
                      })
                      ->when($selectedIngredients, function($query) use ($selectedIngredients, $selectedProductLengths) {
                        $query->where(function($subQuery) use ($selectedIngredients, $selectedProductLengths) {
                          $subQuery->whereIn('products.ingredients', $selectedIngredients)
                                   ->when($selectedProductLengths, function($query, $selectedProductLengths){
                                     $query->orWhereIn('products.length', $selectedProductLengths);
                                   });      
                        });
                      })
                      ->when($selectedGrades, function($query) use ($selectedGrades, $selectedProductLengths) {
                        $query->where(function($subQuery) use ($selectedGrades, $selectedProductLengths) {
                          $subQuery->whereIn('products.grade', $selectedGrades)
                                   ->when($selectedProductLengths, function($query, $selectedProductLengths){
                                     $query->orWhereIn('products.length', $selectedProductLengths);
                                   });      
                        });
                      })
                      ->when($selectedDiameters, function($query) use ($selectedDiameters, $selectedProductLengths) {
                        $query->where(function($subQuery) use ($selectedDiameters, $selectedProductLengths) {
                          $subQuery->whereIn('products.diameter', $selectedDiameters)
                                   ->when($selectedProductLengths, function($query, $selectedProductLengths){
                                     $query->orWhereIn('products.length', $selectedProductLengths);
                                   });      
                        });
                      })
                      ->when($selectedGasolineGrades, function($query) use ($selectedGasolineGrades, $selectedProductLengths) {
                        $query->where(function($subQuery) use ($selectedGasolineGrades, $selectedProductLengths) {
                          $subQuery->whereIn('products.gasoline_grade', $selectedGasolineGrades)
                                   ->when($selectedProductLengths, function($query, $selectedProductLengths){
                                     $query->orWhereIn('products.length', $selectedProductLengths);
                                   });      
                        });
                      })
                      ->orderBy('products_lengths.id', 'ASC')
                      ->get([DB::raw('products_lengths.name as label'), DB::raw('products_lengths.id as value')]);

    if(!empty($product_length) && $product_length->count() > 0)
    {
      $selected = 0;
      foreach($product_length as $key => $singleProductLength)
      {
        $product_length[$key]->is_selected = false;
        if($selectedProductLengths && in_array($singleProductLength->value, $selectedProductLengths))
        {
          $product_length[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.length'),
        'name'      => 'product_length',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'product_length')?true:false,
        'attributes'=> $product_length,
      ];
    }
    
    $gasoline_grades =  Products::distinct()
                        ->where('products.gasoline_grade', '!=', '')
                        ->where('products.gasoline_grade', '!=', null)
                        ->where('products.is_active', '=', config('global.ACTIVE'))
                        ->when($subcategory_id, function($query) use ($subcategory_id){
                          $query->where('products.subcategory_id', '=', $subcategory_id);
                        })
                        ->when($selectedBrands, function($query) use ($selectedBrands, $selectedGasolineGrades) {
                          $query->where(function($subQuery) use ($selectedBrands, $selectedGasolineGrades) {
                            $subQuery->whereIn('products.brand', $selectedBrands)
                                     ->when($selectedGasolineGrades, function($query, $selectedGasolineGrades){
                                      $query->orWhereIn('products.gasoline_grade', $selectedGasolineGrades);
                                     });      
                          });
                        })
                        ->when($selectedBands, function($query) use ($selectedBands, $selectedGasolineGrades) {
                          $query->where(function($subQuery) use ($selectedBands, $selectedGasolineGrades) {
                            $subQuery->whereIn('products.band', $selectedBands)
                                     ->when($selectedGasolineGrades, function($query, $selectedGasolineGrades){
                                       $query->orWhereIn('products.gasoline_grade', $selectedGasolineGrades);
                                     });      
                          });
                        })
                        ->when($selectedTreatments, function($query) use ($selectedTreatments, $selectedGasolineGrades) {
                          $query->where(function($subQuery) use ($selectedTreatments, $selectedGasolineGrades) {
                            $subQuery->whereIn('products.treatments', $selectedTreatments)
                                     ->when($selectedGasolineGrades, function($query, $selectedGasolineGrades){
                                       $query->orWhereIn('products.gasoline_grade', $selectedGasolineGrades);
                                     });      
                          });
                        })
                        ->when($selectedIngredients, function($query) use ($selectedIngredients, $selectedGasolineGrades) {
                          $query->where(function($subQuery) use ($selectedIngredients, $selectedGasolineGrades) {
                            $subQuery->whereIn('products.ingredients', $selectedIngredients)
                                     ->when($selectedGasolineGrades, function($query, $selectedGasolineGrades){
                                       $query->orWhereIn('products.gasoline_grade', $selectedGasolineGrades);
                                     });      
                          });
                        })
                        ->when($selectedGrades, function($query) use ($selectedGrades, $selectedGasolineGrades) {
                          $query->where(function($subQuery) use ($selectedGrades, $selectedGasolineGrades) {
                            $subQuery->whereIn('products.grade', $selectedGrades)
                                     ->when($selectedGasolineGrades, function($query, $selectedGasolineGrades){
                                       $query->orWhereIn('products.gasoline_grade', $selectedGasolineGrades);
                                     });      
                          });
                        })
                        ->when($selectedDiameters, function($query) use ($selectedDiameters, $selectedGasolineGrades) {
                          $query->where(function($subQuery) use ($selectedDiameters, $selectedGasolineGrades) {
                            $subQuery->whereIn('products.diameter', $selectedDiameters)
                                     ->when($selectedGasolineGrades, function($query, $selectedGasolineGrades){
                                       $query->orWhereIn('products.gasoline_grade', $selectedGasolineGrades);
                                     });      
                          });
                        })
                        ->when($selectedProductLengths, function($query) use ($selectedProductLengths, $selectedGasolineGrades) {
                          $query->where(function($subQuery) use ($selectedProductLengths, $selectedGasolineGrades) {
                            $subQuery->whereIn('products.length', $selectedProductLengths)
                                     ->when($selectedGasolineGrades, function($query, $selectedGasolineGrades){
                                       $query->orWhereIn('products.gasoline_grade', $selectedGasolineGrades);
                                     });      
                          });
                        })
                        ->orderBy('products.gasoline_grade', 'ASC')
                        ->get([DB::raw('products.gasoline_grade as label'), DB::raw('products.gasoline_grade as value')]);

    if(!empty($gasoline_grades) && $gasoline_grades->count() > 0)
    {
      $selected = 0;
      foreach($gasoline_grades as $key => $singleGasolineGrade)
      {
        $gasoline_grades[$key]->is_selected = false;
        if($selectedGasolineGrades && in_array($singleGasolineGrade->value, $selectedGasolineGrades))
        {
          $gasoline_grades[$key]->is_selected = true;
          $selected++;
        }
      }
      $data[] = [
        'label'     => __('labels.gasoline_grade'),
        'name'      => 'gasoline_grade',
        'selected'  => $selected,
        'is_opened' => ($openedFilter == 'gasoline_grade')?true:false,
        'attributes'=> $gasoline_grades,
      ];
    }

    foreach($data as $key_1 => $filter) {
      foreach($filter['attributes'] as $key_2 => $attribute) {
        $data[$key_1]['attributes'][$key_2]['value'] = strval($attribute['value']);
      }
    }

    return response()->json([
      'code'    => 200,
      'data'    => $data,
    ]);
  }
  /* END OF PRODUCT FILTER API */

  
  /* PROVINCES */
  public function provinces(Request $request)
  {
    $term = ($request->term)?$request->term:false;
    $provinces = Provinces::where('is_active', '=', config('global.ACTIVE'))
    ->when($term, function($query) use ($term){
      return $query->where('name', 'like', '%'.$term.'%');
    })
    ->orderBy('name', 'ASC')
    ->get(['id', 'name']);

    return response()->json([
      'code'    => 200,
      'message' => trans('api.provinces_fetched_successfully'),
      'data'    => $provinces,
    ]);
  }
  /* END OF PROVINCES */

  /* LOCALITIES */
  public function localities(Request $request)
  {
    $term = ($request->term)?$request->term:false;
    $validator = Validator::make($request->all(), [
      'province_id' => 'required|exists:provinces,id,deleted_at,NULL',
    ]);
 
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'data'    => [],
      ]);
    }

    $province_id = $request->province_id;
    $localities  = Localities::where('province_id', '=', $province_id)
    ->where('is_active', '=', config('global.ACTIVE'))
    ->when($term, function($query) use ($term){
      return $query->where('name', 'like', '%'.$term.'%');
    })
    ->orderBy('name', 'ASC')
    ->get(['id', 'name']);

    return response()->json([
      'code'    => 200,
      'message' => trans('api.localities_fetched_successfully'),
      'data'    => $localities,
    ]);
  }
  /* END OF LOCALITIES */

  /* VALIDATE CUIT */
  public function validateCUIT(Request $request)
  {
    $cuit = ($request->cuit)?$request->cuit:false;

    $flag = false;
    if(is_numeric($cuit) && strlen($cuit) == 11)
    {
      $cadena = str_split($cuit);
      $result = $cadena[0]*5;
      $result += $cadena[1]*4;
      $result += $cadena[2]*3;
      $result += $cadena[3]*2;
      $result += $cadena[4]*7;
      $result += $cadena[5]*6;
      $result += $cadena[6]*5;
      $result += $cadena[7]*4;
      $result += $cadena[8]*3;
      $result += $cadena[9]*2;

      $div = intval($result/11);
      $resto = $result - ($div*11);
      
      if($resto==0)
      {
        if($resto==$cadena[10])
        {
          $flag = true;
        }
        else
        {
          $flag = false;
        }
      }
      elseif($resto==1)
      {
        if($cadena[10]==9 AND $cadena[0]==2 AND $cadena[1]==3)
        {
          $flag = true;
        }
        elseif($cadena[10]==4 AND $cadena[0]==2 AND $cadena[1]==3)
        {
          $flag = true;
        }
      }
      elseif($cadena[10]==(11-$resto))
      {
        $flag = true;
      }
      else
      {
        $flag = false;
      }
    }

    

    $data['is_valid'] = $flag;
                          
    return response()->json([
      'code'    => 200,
      'message' => ($flag)?__('api.cuit_is_valid'):__('api.invalid_cuit'),
      'data'    => $data,
    ]);
  }
  /* END OF VALIDATE CUIT */

  // Contact form
  public function contactForm(Request $request) {
    $data = $request->data;
    $validator = Validator::make($data, [
      'name' => 'required',
      'email' => 'required|email',
      'phone' => 'required',
      'message' => 'required',
    ]);
 
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'data'    => [],
      ]);
    }

    $form['name'] = $data['name'];
    $form['email'] = $data['email'];
    $form['phone'] = $data['phone'];
    $form['message'] = $data['message'];

    \Mail::to(config('global.APP_CONTACTFORM_EMAIL'))->send(new \App\Mail\ContactForm($form));
    return response()->json([
      'code'    => 200,
      'message' => __('labels.mail_messages.contact_success_message'),
    ]);

  }

  public function updateFCMToken(Request $request) {
    // VALIDATION
    $validator = Validator::make($request->all(), [
      'fcm_token' => 'required',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }

    $user = $request->user();
    $user->update(['fcm_token' => $request->fcm_token]);
    return response()->json([
      'code'    => 200,
      'message' => '',
    ]);
  }
}
