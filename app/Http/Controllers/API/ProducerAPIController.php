<?php
namespace App\Http\Controllers\API;

use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\AgronomiaProducts;
use App\Notifications;
use App\AdminMessages;
use App\Http\Controllers\BaseController;
use App\Orders;
use App\ProductoGenerals;
use App\UserGeolocation;
use App\Products;
use App\User;
use Hash;
use Auth;
use Mail;

class ProducerAPIController extends BaseController
{

  /* REGISTERATION */
  public function register(Request $request)
  {
    // VALIDATION
    $validator = Validator::make($request->all(), [
      'business_name' => 'required|max:128',
      'cuit'          => 'required|max:128',
      'email'         => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
      'password'      => 'required|min:8|max:32',
      'terms_and_conditions' => 'required'
    ]);
 
    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
        'errors'  => FirstErrorOfEachField($validator->messages()),
      ]);
    }
    // END OF VALIDATION
 
    // CUSTOM ID GENERATION
    $prefix = config('global.PREFIX.PRODUCER_USER');
    $count  = User::withTrashed()->where('user_type', '=', config('global.CUSTOMER_USER'))->count();
    $custom_id = $prefix.sprintf('%0'.config('global.PREFIX.ORDER_LEADING_ZERO').'d', ($count + 1));
    // END OF CUSTOM ID GENERATION
 
    // USER CREATION
    $data = array(
      'custom_id'     => $custom_id,
      'user_type'     => config('global.CUSTOMER_USER'),
      'business_name' => $request->business_name,
      'cuit'          => $request->cuit,
      'email'         => $request->email,
      'password'      => Hash::make($request->password),
      'is_active'     => config('global.DISABLED'),
      'ip_address'    => $request->ip(),
      'fcm_token'     => $request->fcm_token,
      'device_type'   => $request->device_type,
      'step_2'        => 0,
      'verified'      => 0,
    );
    $user = User::create($data);
    // END OF USER CREATION

    return response()->json([
      'code'    => 200,
      'message' => __('api.registration_successfully_completed'),
    ]);
  }
  /* END OF REGISTERATION */

  /* START OF GEOLOCATION */
  public function geolocation()
  {
    $user = Auth::user();
    $geolocations = UserGeolocation::where(['user_id' => $user->id])->get();

    if(!empty($geolocations) && $geolocations->count() > 0)
    {
      $json = [];
      foreach($geolocations as $geolocation)
      {
        $row['id']          = $geolocation->id;
        $row['name']        = $geolocation->name;
        array_push($json, $row);
      }
      return response()->json([
        'code'    => 200,
        'message' => __('api.location_fetched_successfully'),
        'data'    => $json,
      ]);
    }
    else
    {
      return response()->json([
        'code'    => 200,
        'message' => trans('api.no_location_found'),
        'data'    => [],
      ]);
    }
  }
  /* END OF GEOLOCATION */

  /* START OF CREATE GEOLOCATION */
  public function createGeolocation(Request $request)
  {
    // VALIDATION
    $validator = Validator::make($request->all(), [
      'latitude'  => 'required|numeric|min:-90|max:90',
      'longitude' => 'required|numeric|min:-180|max:180',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 400,
        'message' => $validator->messages()->first(),
      ]);
    }
    // END OF VALIDATION

    $user = Auth::user();
    $geolocations = UserGeolocation::where(['user_id' => $user->id])->get();

    if(!empty($geolocations) && $geolocations->count() > 0)
    {
      return response()->json([
        'code'    => 400,
        'message' => trans('api.first_location_is_already_added_in_your_account'),
      ]);
    }

    UserGeolocation::create([
      'user_id'    => $user->id,
      'name'       => 'Primera ubicación',
      'latitude'   => $request->latitude,
      'longtitude' => $request->longitude,
    ]);

    return response()->json([
      'code'    => 200,
      'message' => trans('api.first_location_is_added'),
    ]);
  }
  /* END OF CREATE GEOLOCATION */

  /* START OF VENDOR SEARCH */
  public function vendorSearch(Request $request)
  {
    $producer  = Auth::user();

    $validator = Validator::make($request->all(), [
      'product_id'        => 'required|exists:products,id,deleted_at,NULL',
      'required_quantity' => 'required|numeric',
      'payment_method'    => 'required|exists:payment_methods,id',
      'geolocation'       => 'required|exists:user_geolocation,id,user_id,'.$producer->id.',deleted_at,NULL',
      'date_of_delivery'  => 'required|regex:/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}$/',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    $payment_method = $request->payment_method;
    
    $agronomias = AgronomiaProducts::where('agronomia_products.product_id', '=', $request->product_id)
                                   ->join('users', function($join) {
                                      $join->on('users.id', '=', 'agronomia_products.agronomia_id')
                                           ->where('users.is_active', '=', config('global.ACTIVE'))
                                           ->where('users.deleted_at', '=', null);
                                     })
                                   ->join('agronomia_payment_methods', function($join) {
                                      $join->on('agronomia_payment_methods.agronomia_id', '=', 'users.id')
                                           ->where('agronomia_payment_methods.deleted_at', '=', null);
                                   })
                                   ->where('agronomia_payment_methods.payment_method_id', '=', $payment_method)
                                   ->where('agronomia_products.available_quanity', '>=', $request->required_quantity)
                                   ->get(['agronomia_products.*']);

    $producer_location = UserGeolocation::find($request->geolocation);

    if(!empty($agronomias) && $agronomias->count() > 0)
    {
      $json = [];
      foreach($agronomias as $agronomia)
      {
        $sourceLat  = $agronomia->agronomia->latitude;
        $sourceLong = $agronomia->agronomia->longtitude;
        $destLat    = $producer_location->latitude;
        $destLong   = $producer_location->longtitude;

        //CHECK IF AGRONOMIA IS IN DISTANCE
        if(distanceInKM($sourceLat, $sourceLong, $destLat, $destLong) <= $agronomia->sales_range_in_km)
        {
          if($agronomia->bonus_applicable == config('global.TRUE') && $request->required_quantity >= $agronomia->min_bonus_quanitity)
          {
            $total_price = $agronomia->bonus_price * $request->required_quantity;
          }
          else
          {
            $total_price = $agronomia->agronomia_price * $request->required_quantity;
          }

          $row['id']                  = $agronomia->agronomia_id;
          $row['business_name']       = $agronomia->agronomia->business_name;
          $row['province_name']       = $agronomia->agronomia->user_province->name;
          $row['locality_name']       = $agronomia->agronomia->user_locality->name;
          $row['avtar_path']          = $agronomia->agronomia->avtar_path;
          $row['ingredients']         = $agronomia->products->ingredients;
          $row['unit_of_measure']     = $agronomia->products->unit_of_measure;
          $row['price']               = (string)$agronomia->agronomia_price;
          $row['bonus_applicable']    = $agronomia->bonus_applicable;
          $row['bonus_price']         = ($agronomia->bonus_price)?(string)$agronomia->bonus_price:'';
          $row['min_bonus_quanitity'] = ($agronomia->min_bonus_quanitity)?(string)$agronomia->min_bonus_quanitity:'';
          $row['total_price']         = number_format($total_price, 2, ',', '');
          $row['total_price_numeric'] = $total_price;
          $row['distance']            = (string)$agronomia->agronomia->distance; //IN KM
          $row['rating']              = number_format($agronomia->agronomia->rating, 1, '.', '');
          $row['delivery_date']       = $request->date_of_delivery;
          $row['is_verified']         = (bool)$agronomia->agronomia->verified;
          $row['payment_methods']     = [];
          $agronomia_payment_methods  = $agronomia->agronomia->agronomiaPaymentMethods;
          foreach($agronomia_payment_methods as $payment_method)
          {
            if($payment_method->payment_method && $payment_method->payment_method->is_active == config('global.ACTIVE'))
            {
              array_push($row['payment_methods'], [
                                                    'id' => $payment_method->payment_method->id,
                                                    'name' => $payment_method->payment_method->name
                                                  ]);
            }
          }
          array_push($json, $row);
        }
      }

      if(!empty($json) && count($json) > 0)
      {
        foreach ($json as $key => $row) {
          $total_price_numeric[$key]  = $row['total_price_numeric'];
        }
        array_multisort($total_price_numeric, SORT_ASC, $json);

        return response()->json([
          'code'    => 200,
          'message' => trans('api.vendors_fetched_successfully'),
          'data'    => $json,
        ]);
      }
    }

    return response()->json([
      'code'    => 404,
      'message' => trans('api.no_vendors_found'),
      'data'    => [],
    ]);
  }
  /* END OF VENDOR SEARCH */

  public function getProductoGeneralName($id) {
    $productGeneral = ProductoGenerals::find($id)->toArray();
    return response()->json([
      'code'    => 200,
      'message' => '',
      'data'    => $productGeneral,
    ]);
  }

  public function listProductoGenerals(Request $request)
  {
    $producer  = Auth::user();

    $validator = Validator::make($request->all(), [
      'producto_general_id' => 'required|exists:producto_generals,id,deleted_at,NULL',
      'required_quantity' => 'required|numeric',
      'payment_method'    => 'required|exists:payment_methods,id',
      'geolocation'       => 'required|exists:user_geolocation,id,user_id,'.$producer->id.',deleted_at,NULL',
      'date_of_delivery'  => 'required|regex:/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}$/',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }

    DB::enableQueryLog();

    $agronomia_products = AgronomiaProducts::where('agronomia_products.is_active', config('global.ACTIVE'))
                          ->join('products', function($join) {
                            $join->on('agronomia_products.product_id', '=', 'products.id')
                                 ->where('products.is_active', '=', config('global.ACTIVE'))
                                 ->where('products.deleted_at', '=', null);
                            })
                          ->join('users', function($join) {
                            $join->on('users.id', '=', 'agronomia_products.agronomia_id')
                                 ->where('users.is_active', '=', config('global.ACTIVE'))
                                 ->where('users.deleted_at', '=', null);
                            })
                          ->join('agronomia_payment_methods', function($join) {
                            $join->on('agronomia_payment_methods.agronomia_id', '=', 'users.id')
                                 ->where('agronomia_payment_methods.deleted_at', '=', null);
                            })
                          ->where('products.producto_general_id', $request->producto_general_id)
                          ->where('agronomia_products.available_quanity', '>=', $request->required_quantity)
                          ->where('agronomia_payment_methods.payment_method_id', '=', $request->payment_method)
                          ->get(['agronomia_products.*', 'products.id as product_id', 'products.name as product_name', 'users.verified']);


    $producer_location = UserGeolocation::find($request->geolocation);
    $json = [];
    foreach($agronomia_products as $agronomia_product)
    {
      $sourceLat  = $agronomia_product->agronomia->latitude;
      $sourceLong = $agronomia_product->agronomia->longtitude;
      $destLat    = $producer_location->latitude;
      $destLong   = $producer_location->longtitude;

      //CHECK IF AGRONOMIA IS IN DISTANCE
      if(distanceInKM($sourceLat, $sourceLong, $destLat, $destLong) <= $agronomia_product->sales_range_in_km)
      {
        if($agronomia_product->bonus_applicable == config('global.TRUE') && $request->required_quantity >= $agronomia_product->min_bonus_quanitity)
        {
          $total_price = $agronomia_product->bonus_price * $request->required_quantity;
        }
        else
        {
          $total_price = $agronomia_product->agronomia_price * $request->required_quantity;
        }

        $row['agronomia_id']        = $agronomia_product->agronomia_id;
        $row['product_id']          = $agronomia_product->product_id;
        $row['product_name']        = $agronomia_product->product_name;
        $row['province_name']       = $agronomia_product->agronomia->user_province->name;
        $row['locality_name']       = $agronomia_product->agronomia->user_locality->name;
        $row['avtar_path']          = $agronomia_product->agronomia->avtar_path;
        $row['unit_of_measure']     = $agronomia_product->products->unit_of_measure;
        $row['price']               = (string)$agronomia_product->agronomia_price;
        $row['bonus_applicable']    = $agronomia_product->bonus_applicable;
        $row['bonus_price']         = ($agronomia_product->bonus_price)?(string)$agronomia_product->bonus_price:'';
        $row['min_bonus_quanitity'] = ($agronomia_product->min_bonus_quanitity)?(string)$agronomia_product->min_bonus_quanitity:'';
        $row['total_price']         = number_format($total_price, 2, ',', '');
        $row['total_price_numeric'] = $total_price;
        $row['distance']            = (string)$agronomia_product->agronomia->distance; //IN KM
        $row['rating']              = number_format($agronomia_product->agronomia->rating, 1, '.', '');
        $row['delivery_date']       = $request->date_of_delivery;
        $row['is_verified']         = (bool)$agronomia_product->verified;
        $row['payment_methods']     = [];
        $agronomia_payment_methods  = $agronomia_product->agronomia->agronomiaPaymentMethods;
        foreach($agronomia_payment_methods as $payment_method)
        {
          if($payment_method->payment_method && $payment_method->payment_method->is_active == config('global.ACTIVE'))
          {
            array_push($row['payment_methods'], [
                                                  'id' => $payment_method->payment_method->id,
                                                  'name' => $payment_method->payment_method->name
                                                ]);
          }
        }
        array_push($json, $row);
      }
    }

    if(!empty($json)) {
      // SORT BY PRICE: LOW to HIGH
      foreach ($json as $key => $row)
      {
        $total_price_numeric[$key] = $row['total_price_numeric'];
      }
      array_multisort($total_price_numeric, SORT_ASC, $json);

      // RESPONSE 
      return response()->json([
        'code'    => 200,
        'message' => trans('api.products_found_for_producto_general'),
        'data'    => $json,
      ]);
    }
    return response()->json([
      'code'    => 404,
      'message' => trans('api.no_vendors_found'),
      'data'    => [],
    ]);
  }

  public function placeOrder(Request $request)
  {
    $user = Auth::user();

    $validator = Validator::make($request->all(), [
      'agronomia_id'      => 'required|exists:users,id,deleted_at,NULL',
      'product_id'        => 'required|exists:products,id,deleted_at,NULL',
      'required_quantity' => 'required|numeric',
      'payment_method'    => 'required|exists:payment_methods,id',
      'geolocation'       => 'required|exists:user_geolocation,id,user_id,'.$user->id.',deleted_at,NULL',
      'date_of_delivery'  => 'required|regex:/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}$/',
    ]);

    if ($validator->fails()) {
      return response()->json([
        'code'    => 404,
        'message' => $validator->messages()->first(),
      ]);
    }
    
    $payment_method = $request->payment_method;

    // REFETCHING PRICES FOR SECURITY REASONS
    $orderPricing = AgronomiaProducts::where('agronomia_products.product_id', $request->product_id)
    ->join('agronomia_payment_methods', function($join) use ($payment_method) {
      return $join->on('agronomia_payment_methods.agronomia_id', '=', 'agronomia_products.agronomia_id')
        ->where('agronomia_payment_methods.payment_method_id', $payment_method)
        ->where('agronomia_payment_methods.deleted_at', null);
    })
    ->join('products', 'products.id', '=', 'agronomia_products.product_id')
    ->join('product_sub_categories', 'product_sub_categories.id', '=', 'products.subcategory_id')
    ->join('product_categories', 'product_categories.id', '=', 'product_sub_categories.category_id')
    ->where('agronomia_products.agronomia_id', $request->agronomia_id)
    ->get(['agronomia_products.*', 'product_categories.commission_percent'])
    ->first();

    $last_order_id = Orders::withTrashed()->max('id');
    $custom_order_no = config('global.PREFIX.ORDER').sprintf('%0'.config('global.PREFIX.ORDER_LEADING_ZERO').'d', ($last_order_id + 1));

    if(!empty($orderPricing) && $orderPricing->count() > 0)
    {
      $data['custom_order_no']       = $custom_order_no;
      $data['producer_id']           = $user->id;
      $data['producer_location_id']  = $request->geolocation;
      $data['agronomia_id']          = $request->agronomia_id;
      $data['product_id']            = $request->product_id;
      $data['actual_price']          = $orderPricing->agronomia_price;
      if($orderPricing->bonus_applicable == config('global.TRUE') && $request->required_quantity >= $orderPricing->min_bonus_quanitity)
      {
        $data['bonus_price'] = $orderPricing->bonus_price;
      }
      else
      {
        $data['bonus_price'] = $orderPricing->agronomia_price;
      }
      $data['ordered_quantity']   = $request->required_quantity;
      $data['total_price']        = $data['bonus_price'] * $data['ordered_quantity'];
      $data['commission_amount']  = ($data['total_price'] * $orderPricing->commission_percent) / 100;
      $data['payment_method_id']  = $request->payment_method;
      $data['date_of_delivery']   = mysql_date_converter($request->date_of_delivery);

      $order = Orders::create($data);
      
      Notifications::create(['user_id' => $user->id, 'type' => config('global.NOTIFICATION.ORDER_PLACED'), 'message' => $order->id]);
      Notifications::create(['user_id' => $request->agronomia_id, 'type' => config('global.NOTIFICATION.ORDER_RECEIVED'), 'message' => $order->id]);
      AdminMessages::create(['type' => config('global.ADMIN_MESSAGE.ADMIN_NOTIFICATION'), 'notification_type' => config('global.NOTIFICATION.ORDER_PLACED'), 'message' => $order->id]);

      /* SEND MAIL TO AGRONOMIA */
      \Mail::to($order->agronomia->email)->send(new \App\Mail\OrderReceived($order));

      /* SEND MAIL TO PRODUCER */
      \Mail::to($user->email)->send(new \App\Mail\OrderPlaced($order));

      /* SEND PUSH NOTIFICATION TO AGRONOMIA */
      $this->sendFcmPushNotification(
        [ User::find($request->agronomia_id)->toArray() ],
        [ 'message'  => str_replace(':order_no', $order->custom_order_no, __('labels.push_notification.agronomia.order_received')) ],
        [
          'type'    => 'order',
          'order_id' => $order->id
        ]);

      /* SEND PUSH NOTIFICATION TO PRODUCER */
      $this->sendFcmPushNotification(
        [ $user ],
        [ 'message'  => str_replace(':order_no', $order->custom_order_no, __('labels.push_notification.producer.order_placed')) ],
        [
          'type'    => 'order',
          'order_id' => $order->id
        ]);

      return response()->json([
        'code'    => 200,
        'message' => trans('api.order_placed_successfully'),
        'data'    => $order,
      ]);
    }

    return response()->json([
      'code'    => 404,
      'message' => trans('api.something_went_wrong_could_not_place_order'),
      'data'    => (object)[],
    ]);
  }
}
