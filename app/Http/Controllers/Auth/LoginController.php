<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function authenticated(Request $request, $user)
    {
      $last_login_at = $user->last_login_at;

      // update last login
      $user->update([
        'last_login_at' => Carbon::now()->toDateTimeString(),
        'last_login_ip' => $request->getClientIp()
      ]);

      // if admin user
      if($user->user_type == config('global.ADMIN_USER'))
      {
        Auth::logout();
        return redirect('login')->withErrors([__('labels.invalid_access')]);
      }

      // if not active
      if($user->is_active == config('global.DISABLED'))
      {
        Auth::logout();
        return redirect('login')->withErrors([__('api.the_user_is_in_approval_process')]);
      }

      // if agronomia
      if($user->user_type == config('global.BUSINESS_USER'))
      {
        if($last_login_at == '')
        {
          return redirect('agronomia/mis-productos');
        }
        return redirect('agronomia');
      }

      // if producer
      if($user->user_type == config('global.CUSTOMER_USER'))
      {
        return redirect('producer');
      }
    }
}
