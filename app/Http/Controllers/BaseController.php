<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
  protected $FCM_KEY;
  public function __construct()
  {
    // $this->FCM_KEY   = env('FCM_KEY');
    $this->FCM_KEY = 'AAAA8cKxtbw:APA91bFHru_LLiD2f37Teqj-6TZM13dSsHxVvRu9J6UpO5RxAhLN7mYuBgjODj4enkGGhXHuEwf3A7r1-wohly4XWZH9bpKqfYBhSxUuuo1m0uzKaNl3cpGPCqi53Ajdi0wwTu_724Z4';
  }

  public function sendFcmPushNotification($users, $message, $extraData = null)
  {

    if (empty($users)) {
      return false;
    }

    //Google cloud messaging GCM-API url
    $url = 'https://fcm.googleapis.com/fcm/send';
    $ids = [];
    foreach($users as $key => $user) {
      if ($user['fcm_token']) {
        $ids[] = $user['fcm_token'];
      }
    }

    if (!empty($ids)) {
      $data = array(
        'title'   => env('APP_NAME'),
        'message' => $message['message'],
      );

      if($extraData) {
        $data = array_merge($data, $extraData);
      }

      $fields = array(
        'registration_ids' => $ids,
        'data' => $data,
        'notification' => array(
          'title' => env('APP_NAME'),
          'text' => $message['message'],
          'click_action' => 'keinsumos',
          'sound' => 'default'
        ),
        "aps" => array(
          "alert" => $message['message'],
          "badge" => 1,
          // "content-available" : 1,
        )
      );

      $headers = array(
        'Authorization: key=' . $this->FCM_KEY,
        'Content-Type: application/json'
      );

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
      $result = curl_exec($ch);
      curl_close($ch);
    }
    return true;

  }
}
