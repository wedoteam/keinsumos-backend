<?php
function distanceInKM($sourceLat, $sourceLong, $destLat, $destLong)
{
	$sourceLat 	= deg2rad($sourceLat);
  $sourceLong = deg2rad($sourceLong);
  $destLat 	  = deg2rad($destLat);
  $destLong 	= deg2rad($destLong);

  $latDelta = $destLat - $sourceLat;
  $lonDelta = $destLong - $sourceLong;

  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($sourceLat) * cos($destLat) * pow(sin($lonDelta / 2), 2)));
  return number_format(($angle * 6371000) / 1000, 2, '.', '');

  return '('.$sourceLat.','.$sourceLong.'),('.$destLat.','.$destLong.') = '.($angle * 1.6371000);
}

function mysql_date_converter($date)
{
	if(preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4}$/', $date) == 1)
	{
		$dates = explode('-', $date);
		return $dates[2].'-'.$dates[1].'-'.$dates[0];
	}
	return null;
}

function YMDtoDMY($date)
{
	if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $date) == 1)
	{
		$dates = explode('-', $date);
		return $dates[2].'-'.$dates[1].'-'.$dates[0];
  }
  else if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}$/', $date) == 1)
  {
    $dates = explode('-', $date);
    $time  = explode(' ', $dates[2]);
		return $time[0].'-'.$dates[1].'-'.$dates[0].' '.$time[1];
  }
	return null;
}

function FirstErrorOfEachField($object)
{
  $errors = $object->messages();
  $messages = [];
  foreach($errors as $key => $error)
  {
    if(!isset($messages[$key]))
    {
      $messages[$key] = $error[0];
    }
  }
  return $messages;
}

