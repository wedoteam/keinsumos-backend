<?php
namespace App\Http\Middleware;

use Closure;
use Auth;

class AgronomiaToken
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if(Auth::check()) 
    {
      $user = Auth::user();
      if($user->user_type == config('global.BUSINESS_USER'))
      {
        return $next($request);
      }
      Auth::logout();
    }
    return response()->json([
      'code'    => 404,
      'message' => trans('login_messages.unauthorized_access'),
    ]);
  }
}
