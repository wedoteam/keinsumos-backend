<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
class CheckStatus
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if(Auth::check()) 
    {
      $user = Auth::user();
      if($user->is_active == config('global.ACTIVE'))
      {
        return $next($request);
      }
      Auth::logout();
    }
    
    return response()->json([
      'code'    => 401,
      'message' => __('labels.permission_denied'),
    ]);
  }
}