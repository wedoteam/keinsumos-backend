<?php
namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminAuth
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if(Auth::check()) 
    {
      $user = Auth::user();
      if($user->user_type == config('global.ADMIN_USER') && $user->is_active == config('global.ACTIVE'))
      {
        return $next($request);
      }
      Auth::logout();
      return redirect()->route('login')->withErrors([__('api.the_user_is_in_approval_process')]);
    }
    return redirect()->route('login')->withErrors([__('api.you_have_been_logged_out')]);
  }
}
