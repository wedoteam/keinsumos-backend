<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderPlaced extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($order)
  {
    $this->order = $order;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->from(config('global.APP_EMAIL'), config('global.APP_COMPANY'))
                ->subject( str_replace(':order_no', $this->order->custom_order_no, __('labels.mail_subjects.producer.order_placed')))
                ->view('emails.producer.order_placed', ['order' => $this->order]);
  }
}
