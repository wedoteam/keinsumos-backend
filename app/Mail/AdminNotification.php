<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminNotification extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($text)
  {
    $this->text = $text;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->from(config('global.APP_EMAIL'), config('global.APP_COMPANY'))
                ->subject(__('labels.mail_subjects.admin_message'))
                ->view('emails.admin_message', ['text' => $this->text]);
  }
}
