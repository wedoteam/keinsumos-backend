<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class ContactForm extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($param)
  {
    $this->param = $param;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    $data['name']  = $this->param['name'];
    $data['email'] = $this->param['email'];
    $data['phone'] = $this->param['phone'];
    $data['body']  = strip_tags($this->param['message']);
    return $this->from(config('global.APP_EMAIL'), config('global.APP_COMPANY'))
                ->subject(__('labels.mail_subjects.contact_form'))
                ->view('emails.contact_form', $data);
  }
}
