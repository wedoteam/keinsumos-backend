<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderRejected extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($order, $user_type)
  {
    $this->order = $order;
    $this->user_type = $user_type;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    if($this->user_type == 'agronomia')
    {
      return $this->from(config('global.APP_EMAIL'), config('global.APP_COMPANY'))
                ->subject( str_replace(':order_no', $this->order->custom_order_no, __('labels.mail_subjects.agronomia.order_rejected')))
                ->view('emails.agronomia.order_rejected', ['order' => $this->order]);
    }
    else if($this->user_type == 'producer')
    {
      return $this->from(config('global.APP_EMAIL'), config('global.APP_COMPANY'))
                ->subject( str_replace(':order_no', $this->order->custom_order_no, __('labels.mail_subjects.producer.order_rejected')))
                ->view('emails.producer.order_rejected', ['order' => $this->order]);
    }
  }
}
