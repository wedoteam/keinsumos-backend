<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class ForgetPasswordMail extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($email, $token)
  {
    $this->token = $token;
    $this->user  = User::where('email', $email)->first();
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    $data['user']  = $this->user;
    $data['token'] = $this->token;
    return $this->from(config('global.APP_EMAIL'), config('global.APP_COMPANY'))
                ->subject(__('labels.mail_messages.restore_password'))
                ->view('emails.forget_password', $data);
  }
}
