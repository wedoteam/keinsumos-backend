<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderChatMessage extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($order, $receiver)
  {
    $this->order    = $order;
    $this->receiver = $receiver;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->from(config('global.APP_EMAIL'), config('global.APP_COMPANY'))
                ->subject(__('labels.mail_subjects.chat_message'))
                ->view('emails.chat_message', ['order' => $this->order, 'receiver' => $this->receiver]);
  }
}
