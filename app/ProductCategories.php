<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
	use SoftDeletes;
  protected $guarded = [];

  public function productSubCategories()
  {
  	return $this->hasMany('App\ProductSubCategories', 'category_id', 'id')->where(['is_active' => config('global.ACTIVE')]);
  }
}
