<?php
namespace App\Providers;

use DB;
use Auth;
use App\Menubar;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
  public function boot()
  {
    view()->composer('admin.index', function($view) {

      //CHECK IF USER LOGGED
      if(Auth::check()) 
      {
        $user = Auth::user();
        $menu = Menubar::where(['type' => $user->user_type, 'is_active' => config('global.ACTIVE')])->orderBy('order_priority', 'ASC')->get();

        $view->with('user', $user);
        $view->with('menu', $menu);
      }

    });
  }
}