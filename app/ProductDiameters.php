<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDiameters extends Model
{
  protected $table = 'products_diameters';
  protected $hidden = ['created_at', 'updated_at'];
  protected $guarded = [];
}