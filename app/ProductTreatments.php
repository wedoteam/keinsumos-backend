<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTreatments extends Model
{
  protected $table = 'products_treatments';
  protected $hidden = ['created_at', 'updated_at'];
  protected $guarded = [];
}