<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserAvtars extends Model
{
  use SoftDeletes;
  protected $guarded = [];
  protected $appends = ['filepath'];

  public function user()
  {
  	return $this->hasMany('App\User', 'avtar_id');
  }

  public function getFilepathAttribute()
  {
  	return asset('public/uploads/user_avtars/'.$this->filename);
  }
}
