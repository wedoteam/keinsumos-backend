<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserGeolocation extends Model
{
  use SoftDeletes;
  protected $table = 'user_geolocation';
  protected $guarded = [];
}
