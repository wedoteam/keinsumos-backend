<?php

namespace App\Exports;

use App\Orders;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;

class BillingExport implements WithHeadings, FromArray
{
  public function __construct(string $from_date, string $to_date)
  {
    $this->from_date = $from_date;
    $this->to_date = $to_date;
  }

  public function headings(): array
  {
    return [
      'ID AGRONOMÍA', 'RAZÓN SOCIAL', 'CUIT', 'TELÉFONO', 'E-MAIL', 'PROVINCIA', 'LOCALIDAD', 'PEDIDOS CONCRETADOS', 'ID DE PEDIDOS CONCRETADOS', 'DESDE', 'HASTA', 'TOTAL', 'A FACTURAR'
    ];
  }

  public function array(): array {

    $from_date = $this->from_date.' 00:00:00';
    $to_date   = $this->to_date.' 11:59:59';
    
    $rows = Orders::where('orders.order_status', '=', config('global.APPROVED_ORDER_STATUS'))
            ->where('orders.created_at', '>=', $from_date)
            ->where('orders.created_at', '<=', $to_date)
            ->groupBy('orders.agronomia_id')
            ->get(['orders.agronomia_id', DB::raw('COUNT(*) as total_orders'), DB::raw('GROUP_CONCAT(orders.custom_order_no SEPARATOR ", ") as orders'), DB::raw('SUM(orders.total_price) as total_price'), DB::raw('SUM(orders.commission_amount) as total_commission')]);

    foreach ($rows as $row) {
      $data[] = [
        'agronomia_id'     => $row->agronomia->custom_id,
        'business_name'    => $row->agronomia->business_name,
        'cuit'             => $row->agronomia->cuit,
        'mobile'           => $row->agronomia->mobile_prefix.'-'.$row->agronomia->mobile,
        'email'            => $row->agronomia->email,
        'province'         => $row->agronomia->user_province->name,
        'locality'         => $row->agronomia->user_locality->name,
        'total_orders'     => $row->total_orders,
        'orders'           => $row->orders,
        'from_date'        => YMDtoDMY($this->from_date),
        'to_date'          => YMDtoDMY($this->to_date),
        'total_price'      => $row->total_price,
        'total_commission' => $row->total_commission,
      ];
    }
    return $data;
  }
}
