<?php

namespace App\Exports;

use App\Products;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;

class ProductsExport implements WithHeadings, FromArray
{
    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }
    public function headings(): array
    {
        return [
            'id', 'Nombre del Producto', 'Id de Producto', 'Unidad de medida', 'Descripcion', 'Subcategoria Name', 'Producto General', 'Marca', 'Banda Name', 'Tratamientos Name', 'Grado', 'Diametro Name', 'Largo Name', 'Grado de nafta', 'Formulacion', 'Atributos 2', 'Principio Activo', 'Activo (0 = Inactivo, 1= Activo)'
        ];
    }

    public function array(): array {
      $products = [];
      if (!empty($this->ids)) {
        $products = Products::with(['productSubCategories', 'productoGenerals', 'product_band', 'product_treatment', 'product_diameter', 'product_length'])->whereIn('id', $this->ids)->get()->toArray();
      } else {
        $products = Products::with(['productSubCategories', 'productoGenerals', 'product_band', 'product_treatment', 'product_diameter', 'product_length'])->get()->toArray();
      }
      $data = [];
      foreach ($products as $key => $product) {
        $data[] = [
          'id' => $product['id'],
          'name' => $product['name'],
          'custom_id' => $product['custom_id'],
          'unit_of_measure' => $product['unit_of_measure'],
          'description' => $product['description'],
          'subcategory_id' => $product['product_sub_categories'] ? $product['product_sub_categories']['name'] : '',
          'producto_general_id' => $product['producto_generals'] ? $product['producto_generals']['name'] : '',
          'brand' => $product['brand'],
          'band' => $product['product_band'] ? $product['product_band']['name'] : '',
          'treatments' => $product['product_treatment'] ? $product['product_treatment']['name'] : '',
          'grade' => $product['grade'],
          'diameter' => $product['product_diameter'] ? $product['product_diameter']['name'] : '',
          'length' => $product['product_length'] ? $product['product_length']['name'] : '',
          'gasoline_grade' => $product['gasoline_grade'],
          'attribute_1' => $product['attribute_1'],
          'attribute_2' => $product['attribute_2'],
          'ingredients' => $product['ingredients'],
          'is_active' => $product['is_active'] === 1 ? "1" : "0"
        ];
      }
      return $data;
    }

    // public function query()
    // {
    //     if (!empty($this->ids)) {
    //         return Products::query()->select('id', 'name', 'custom_id', 'unit_of_measure', 'description', 'subcategory_id', 'brand', 'band', 'treatments', 'grade', 'diameter', 'length', 'gasoline_grade', 'attribute_1', 'attribute_2', 'ingredients', DB::raw('(CASE WHEN is_active = "1" THEN "1" ELSE "0" END) as is_active'))->whereIn('id', $this->ids);
    //     } else {
    //         return Products::query()->select('id', 'name', 'custom_id', 'unit_of_measure', 'description', 'subcategory_id', 'brand', 'band', 'treatments', 'grade', 'diameter', 'length', 'gasoline_grade', 'attribute_1', 'attribute_2', 'ingredients', DB::raw('(CASE WHEN is_active = "1" THEN "1" ELSE "0" END) as is_active'));
    //     }
    // }
}
