<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLengths extends Model
{
  protected $table = 'products_lengths';
  protected $hidden = ['created_at', 'updated_at'];
  protected $guarded = [];
}