<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ProductoGenerals extends Model
{
  use SoftDeletes;
  protected $guarded = [];
  protected $appends = ['unit_of_measure'];

  public function getUnitOfMeasureAttribute() {
    return $this->hasMany('App\Products','producto_general_id', 'id')->pluck('unit_of_measure')->first();
  }
}
