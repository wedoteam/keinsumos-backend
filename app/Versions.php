<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Versions extends Model
{
  protected $table = 'versions';
  protected $guarded = [];
}