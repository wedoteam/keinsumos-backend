<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendAdminMessageMailJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  public $email = '';
  public $text = '';
  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($email, $text)
  {
    $this->email = $email;
    $this->text  = $text;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    \Mail::to($this->email)->send(new \App\Mail\AdminNotification($this->text));
  }
}
