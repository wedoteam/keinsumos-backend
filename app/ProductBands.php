<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBands extends Model
{
  protected $table = 'products_bands';
  protected $hidden = ['created_at', 'updated_at'];
  protected $guarded = [];
}