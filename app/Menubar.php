<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Menubar extends Model
{
  use SoftDeletes;
  protected $table   = 'menubar';
  protected $guarded = [];
}