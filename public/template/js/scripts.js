// JavaScript Document
jQuery(document).ready(function() {
  // Mobile menu
  $('#dropdownMenuButton').click(function() {
    $('#accordionSidebar').toggleClass('active');
  });
  $('#accordionSidebar .close-button').click(function() {
    $('#accordionSidebar').toggleClass('active');
  });

  $(function() {
    $('a[href*=\\#]').on('click', function(e) {
      e.preventDefault();
      $('html, body').animate(
        {
          scrollTop: $($(this).attr('href')).offset().top
        },
        500,
        'linear'
      );
    });
  });

  jQuery('.navbar-toggler').click(function() {
    jQuery('body').toggleClass('mobile-open');
  });

  $(window).on('scroll', function() {
    if ($(this).scrollTop() > 150) $('.back-to-top-wrap').fadeIn('slow');
    else $('.back-to-top-wrap').fadeOut('slow');
  });

  $(document).on('click', '.back-to-top-wrap', function() {
    $('html, body').animate(
      {
        scrollTop: 0
      },
      800
    );
    return false;
  });

  $('.mobile-footer ul li').click(function() {
    $('.mobile-footer ul li').removeClass('active');
    $(this).addClass('active');
  });

  if (typeof fancybox !== 'undefined') {
    $('[data-fancybox]').fancybox({
      transitionEffect: 'zoom-in-out',
      animationEffect: 'zoom-in-out',
      animationDuration: 500,
      transitionDuration: 566,
      infobar: false,
      loop: true,
      keyboard: false,

			toolbar: true,
			helpers: {
				media: true
			},
      youtube: {
        autoplay: 1
      },

      buttons: [
        'zoom',
        'share',
        'slideShow',
        'fullScreen',
        'download',
        'thumbs',
        'close'
      ]
    });
  }

  $('.accordian-title').click(function() {
    $('.accordian-title')
      .not($(this))
      .removeClass('active');
    $('.accordian-text')
      .not($(this).next())
      .slideUp();
    $(this).toggleClass('active');
    $(this)
      .next()
      .slideToggle(250);
  });
});

equalheight = function(container) {
  var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
  $(container).each(function() {
    $el = $(this);
    $($el).height('auto');
    topPostion = $el.position().top;

    if (currentRowStart != topPostion) {
      for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
        rowDivs[currentDiv].outerHeight(currentTallest);
      }
      rowDivs.length = 0; // empty the array
      currentRowStart = topPostion;
      currentTallest = $el.height();
      rowDivs.push($el);
    } else {
      rowDivs.push($el);
      currentTallest =
        currentTallest < $el.height() ? $el.height() : currentTallest;
    }
    for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest);
    }
  });
};

equalheight('.register-cards .card.shadow-sm');

$(window).on('load', function() {
  equalheight('.card.shadow-sm');
});

$(window).resize(function() {
  equalheight('.register-cards .card.shadow-sm');
});

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener(
    'load',
    function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener(
          'submit',
          function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          },
          false
        );
      });
    },
    false
  );
})();
