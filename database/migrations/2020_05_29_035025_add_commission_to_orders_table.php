<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\ProductCategories;
use App\Orders;

class AddCommissionToOrdersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('orders', function (Blueprint $table) {
      $table->decimal('commission_amount', 11, 2)->after('total_price')->default(0);
    });
    $this->moveData();
  }

  public function moveData()
  {
    $orders = Orders::join('products', 'products.id', '=', 'orders.product_id')
              ->join('product_sub_categories', 'product_sub_categories.id', '=', 'products.subcategory_id')
              ->join('product_categories', 'product_categories.id', '=', 'product_sub_categories.category_id')
              ->get(['orders.id', 'orders.total_price', 'product_categories.commission_percent']);

    if(!empty($orders))
    {
      foreach($orders as $order)
      {
        $commission = ($order->total_price * $order->commission_percent) / 100;
        Orders::where('id', '=', $order->id)
              ->update(['commission_amount' => $commission]);
      }
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('orders', function (Blueprint $table) {
      //
    });
  }
}
