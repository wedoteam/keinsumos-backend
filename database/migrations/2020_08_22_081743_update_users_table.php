<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class UpdateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->string('mobile_prefix', 8)->nullable()->after('email');
      $table->unsignedInteger('sales_range_in_km')->nullable()->after('longtitude');
      $table->unsignedTinyInteger('verified')->nullable()->after('is_active')
            ->comment('0 => No, 1 => Yes');
      $table->datetime('last_login_at')->nullable()->after('device_type');
      $table->string('last_login_ip')->nullable()->after('last_login_at');
      $table->dropColumn('field_name');
      $table->dropColumn('field_type');
      $table->dropColumn('afip_condition');
      $table->dropColumn('rentas_condition');
      $table->dropColumn('iibb_no');
    });

    $users = User::all();

    foreach($users as $user)
    {
      $data = [];
      if($user->mobile != '') 
      {
        $data['mobile_prefix'] = '';
      }

      if($user->user_type == '1') // agronomia
      {
        if($user->latitude != '' && $user->longtitude != ''
            && $user->agronomiaPaymentMethods()->count() > 0)
        {
          $data['step_2'] = '1';
          $data['verified'] = '1';
        }
        else
        {
          $data['step_2'] = '0';
          $data['verified'] = '0';
        }

        if($user->agronomia_products()->count() > 0)
        {
          $data['last_login_at'] = date('Y-m-d H:i:s');
          $data['last_login_ip'] = $user->ip_address;
        }
      }

      if($user->user_type == '2') // producer
      {
        if($user->firstname != ''
            && $user->surname != ''
            && $user->mobile != ''
            && $user->province != ''
            && $user->locality != ''
            && $user->user_geolocation()->count() > 0)
        {
          $data['step_2'] = '1';
        }
        else
        {
          $data['step_2'] = '0';
        }

        if($user->renspa_no != '')
        {
          $data['verified'] = '1';
        }
        else
        {
          $data['verified'] = '0';
        }
      }

      $user->update($data);
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      //
    });
  }
}
