<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;
use App\Orders;
use App\UserGeolocation;

class CreateUserGeolocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable('user_geolocation'))
      {  
        Schema::create('user_geolocation', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->bigInteger('user_id')->unsigned();
          $table->string('name', 128);
          $table->string('latitude', 32);
          $table->string('longtitude', 32);
          $table->timestamps();
          $table->softDeletes();
          $table->foreign('user_id')->references('id')->on('users')->onDelete('restrict');
        });
        DB::statement('ALTER TABLE user_geolocation CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        DB::statement('ALTER TABLE orders ADD producer_location_id BIGINT UNSIGNED NOT NULL AFTER producer_id');
        $this->moveData();
        DB::statement('ALTER TABLE orders ADD FOREIGN KEY (producer_location_id) REFERENCES user_geolocation(id) ON DELETE RESTRICT ON UPDATE RESTRICT; ');
      }
    }

    public function moveData()
    {
      $users = User::where('user_type', '=', config('global.CUSTOMER_USER'))
                   ->where('step_2', '=', config('global.REGISTERED.COMPLETED'))
                   ->get(['id', 'latitude', 'longtitude']);

      if(!empty($users))
      {
        foreach($users as $user)
        {
          $location = UserGeolocation::create([
            'user_id'    => $user->id,
            'name'       => 'Primera ubicación',
            'latitude'   => $user->latitude,
            'longtitude' => $user->longtitude,  

          ]);

          Orders::where('producer_id', '=', $user->id)
                ->update(['producer_location_id' => $location->id]);
        }
      }

      User::where('user_type', '=', config('global.CUSTOMER_USER'))
          ->update([
              'latitude'   => null,
              'longtitude' => null,
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_geolocation');
    }
}
