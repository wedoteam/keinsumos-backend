<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('users', function (Blueprint $table) {

      $table->string('worst_credit_bureau', 255)
            ->nullable()
            ->after('sales_range_in_km');

      $table->string('user_score', 255)
            ->nullable()
            ->after('worst_credit_bureau');

      $table->string('bad_cheques', 255)
            ->nullable()
            ->after('user_score');

      $table->string('unpaid_employer_contribution', 255)
            ->nullable()
            ->after('bad_cheques');

      $table->string('vat_condition', 255)
            ->nullable()
            ->after('unpaid_employer_contribution');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      //
    });
  }
}
