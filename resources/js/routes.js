import dashboard from "./components/User/Dashboard.vue";
import add_location from "./components/User/Producer/Add_location.vue";
import complete_registeration from "./components/User/Complete_registeration.vue";
import my_account from "./components/User/My_account.vue";
import products from "./components/User/Products.vue";
import add_product from "./components/User/Add_product.vue";
import my_products from "./components/User/My_products.vue";
import product_detail from "./components/User/Product_detail.vue";
import single_product_detail from "./components/User/Producer/Single_product_detail.vue";
import vendor_list from "./components/User/Producer/Dashboard/Vendor_list.vue";
import general_productos_list from "./components/User/Producer/Dashboard/General_productos_list.vue";
import my_orders from "./components/User/My_orders.vue";
import my_sales from "./components/User/My_sales.vue";
import reviews from "./components/User/Reviews.vue";
import notifications from "./components/User/Notifications.vue";
import chat from "./components/User/Chat.vue";
import faq from "./components/User/Faq.vue";

export default [
  {
    path: "/",
    component: dashboard,
    name: "my_panel"
  },
  {
    path: "/search",
    component: dashboard,
    name: "search"
  },
  {
    path: "/agregar-ubicación",
    component: add_location,
    name: "add_location"
  },
  {
    path: "/registro-completo",
    component: complete_registeration,
    name: "complete_registeration"
  },
  {
    path: "/mi-cuenta",
    component: my_account,
    name: "my_account"
  },
  {
    path: "/productos/:subcategoryId?",
    component: products,
    name: "products"
  },
  {
    path: "/productos/añadir/:productId",
    component: add_product,
    name: "add_product"
  },
  {
    path: "/productor/producto/:id",
    component: single_product_detail,
    name: "single_product_detail"
  },
  {
    path: "/lista-de-agronomia",
    component: vendor_list,
    name: 'vendor_list'
  },
  {
    path: "/lista-de-productos",
    component: general_productos_list,
    name: 'general_productos_list'
  },
  {
    path: "/mis-productos",
    component: my_products,
    name: "my_products"
  },
  {
    path: "/mis-producto/:agronomiaProductId",
    component: add_product,
    name: "my_product",
  },
  {
    path: "/producto/añadir/:id",
    component: product_detail,
    name: "addSelectedProduct"
  },
  {
    path: "/mis-pedidos",
    component: my_orders,
    name: "my_orders"
  },
  {
    path: "/mis-ventas",
    component: my_sales,
    name: "my_sales"
  },
  {
    path: "/reseñas",
    component: reviews,
    name: "reviews"
  },
  {
    path: "/notificaciones",
    component: notifications,
    name: "notifications"
  },
  {
    path: "/chat",
    component: chat,
    name: "chat"
  },
  {
    path: "/ayuda",
    component: faq,
    name: "faq"
  },
];
