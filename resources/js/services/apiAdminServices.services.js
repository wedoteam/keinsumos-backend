import axios from 'axios';

export const getProvinces  = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}provinces`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const getLocalities  = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}localities`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const fetchPermissions  = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}permissions_json`)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const getAdminUser  = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}get_admin_user`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const changeAdminUserState  = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}change_admin_state`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const storeAdmin = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}store_admin_user`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const submitAppUser = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}store_app_user`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const changeAppUserState = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}change_app_user_state`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const login  = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}login`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const auth = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}auth`)
    .then((response) => {
      resolve(response);
    })
    .catch(err => {
      reject(err)
    });
  });
}

export const menubar = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}menubar`)
    .then((response) => {
      resolve(response.data.data);
    })
    .catch(err => {
      reject(err)
    });
  });
}

export const searchAppUsersbyID = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}search_app_user`, {custom_id: payload})
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const getProductDetail = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}product_detail`, payload)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const saveTermsAndConditions = (payload) => {
  return new Promise((resolve, reject) => {
    axios.put(`${process.env.MIX_LOCAL_ADMIN_API_URL}custom_page`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const logout = (payload) => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}logout`, payload)
    .then((response) => {
      resolve(response);
    })
    .catch(err => {
      this.$toaster.error(err);
    });
  });
}

export const getMyProfile = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}my_account`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const getAvatars = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}user_avtars`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const updateProfile = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}update_profile`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const dashboardData = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}dashboard`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const updateProductPrices = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}apply_increase_discount`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const getAgronomiaProductDetail = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}agronomia_product_detail`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const agronomiaAddProduct = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}add_agronomia_product`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const agronomiaEditProduct = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}edit_agronomia_product`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const getCategories = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}product_categories`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const getProductoGenerals = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}producto_generals`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const saveFAQ = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}save_faq`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const changeFAQState = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}change_faq_state`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const getNotifications = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}notifications`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}


export const getNotificationCounts = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}unread_notification_count`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const deleteAdminProduct = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}delete_product`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const addAdminProduct = (payload) => {
  var formData = new FormData();
  for (const key in payload) {
    formData.append(key, payload[key]);
  }
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}store_product`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const changeChatState = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}enable_disable_chat`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const changeReviewState = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}change_review_state`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const saveAdminMessage = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}store_admin_message`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const retriveOrderChatMessages = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}order_chat_messages`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const getProductAttributes = () => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}product_attributes`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}


export const getUserDetail = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}user_detail`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const uploadProductCSV = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}upload_products_csv`, payload, { headers: { 'Content-Type': 'multipart/form-data' }})
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}


export const exportProductCSV = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}export_products_csv`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}


export const removeProductCSV = () => {
  return new Promise(() => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}remove_products_csv`)
      .then((response) => {
        resolve();
      });
  });
}

export const getTnc = () => {
  return new Promise((resolve) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}get_tnc`)
      .then((response) => {
        resolve(response.data);
      });
  });
}

export const searchProductoGeneralbyName = (search_term) => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_ADMIN_API_URL}search_producto_general/${search_term}`)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const exportBillingCSV = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_ADMIN_API_URL}export_billing_csv`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}
