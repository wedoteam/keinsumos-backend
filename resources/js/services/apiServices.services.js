import axios from 'axios';

export const getProvinces  = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}provinces`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const getLocalities  = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}localities`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const submitAppUser = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}admin/store_app_user`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const changeAppUserState = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}admin/change_app_user_state`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const auth = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}auth`)
    .then((response) => {
      resolve(response);
    })
    .catch(err => {
      reject(err)
    });
  });
}

export const menubar = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}menubar`)
    .then((response) => {
      resolve(response.data.data);
    })
    .catch(err => {
      reject(err)
    });
  });
}

export const searchProductsbyName = (product_name) => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}searchProductsbyName/${product_name}`)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const searchProductoGenerals = (searchText) => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}searchProductoGenerals/${searchText}`)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const list_producto_generals = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}producer/list_producto_generals`, payload)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const producto_general_name = (id) => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}producer/get_producto_general_name/${id}`)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const searchProducts = (payload) => {
  const bodyFormData = new FormData();
  for (const i in payload) {
    if (payload[i]) {
      bodyFormData.set(i, payload[i]);
    }
  }
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}products`, bodyFormData, {'Content-Type': 'multipart/form-data'})
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const getPaymentMethods = () => {
  return new Promise((resolve) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}payment_methods`)
      .then((response) => {
        resolve(response.data.data);
      });
    });
}

export const getGeolocation = () => {
  return new Promise((resolve) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}producer/geolocation`)
      .then((response) => {
        resolve(response.data.data);
      });
    });
}

export const createGeolocation = (payload) => {
  return new Promise((resolve) => {
  axios.post(`${process.env.MIX_LOCAL_API_URL}producer/geolocation`, payload)
    .then((response) => {
      resolve(response.data);
    });
  });
}

export const vendorSearch = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}producer/vendor_search`, payload)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const getProductDetail = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}product_detail`, payload)
      .then((response) => {
        if (response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      });
    });
}

export const placeOrder = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}producer/place_order`, payload)
      .then((response) => {
        if(response.data.code === 200) {
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const getMyOrders = () => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}orders`)
    .then((response) => {
      resolve(response.data.data);
    })
    .catch(err => {
      reject(err)
    });
  });
}

export const getOrderDetail = (id) => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}orders/${id}`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const getTermsAndConditions = () => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}custom_page`, {page_id: 1})
    .then((response) => {
      resolve(response.data.data.content.replace("<![CDATA[", "").replace("]]>", ""));
    })
    .catch(err => {
      reject(err)
    });
  });
}

export const logout = (payload) => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}logout`, payload)
    .then((response) => {
      resolve(response);
    })
    .catch(err => {
      this.$toaster.error(err);
    });
  });
}

export const getMyProfile = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}my_account`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const getAvatars = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}user_avtars`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const registerStep2 = (payload) => {
  return new Promise((resolve, reject) => {
  axios.post(`${process.env.MIX_LOCAL_API_URL}register_step_2`, payload)
    .then((response) => {
      if(response.data.code === 200) {
        resolve(response.data.message);
      } else {
        reject(response.data.message);
      }
    })
    .catch(err => {
      reject(err)
    });
  });
}

export const updateProfile = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}update_profile`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const AdminDashboardData = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}dashboard`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const AgronomiaDashboardData = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}dashboard`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const getFaq = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}faq`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}



export const getMyProducts = () => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}agronomia/my_products`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const updateProductPrices = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}agronomia/apply_increase_discount`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const getAgronomiaProductDetail = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}agronomia/product_detail`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}


export const agronomiaEditProduct = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}agronomia/edit_product`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
  });
}


export const agronomiaAddProduct = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}agronomia/add_product`, payload)
      .then((response) => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      });
  });
}


export const getCategories = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}product_categories`)
      .then((response) => {
        resolve(response.data.data);
      })
      .catch(err => {
        reject(err)
      });
    });
}


export const changeOrderStatus = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}agronomia/approve_reject_order`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}


export const getNotifications = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}notifications`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}


export const getNotificationCounts = () => {
  return new Promise((resolve, reject) => {
    axios.get(`${process.env.MIX_LOCAL_API_URL}unread_notification_count`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const getReceivedReviews = () => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}my_reviews`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}


export const getReviewsByUser = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}get_reviews_by_user`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}



export const getApprovedOrdersForMyReviews = () => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}reviews`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}


export const submitOrderRating = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}review_order`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
    });
}

export const changeChatState = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}enable_disable_chat`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const retriveOrderChatMessages = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}order_chat_messages`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const sendOrderChatMessages = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}send_order_chat_message`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}

export const getProductList = (payload) => {
  return new Promise((resolve, reject) => {
  axios.post(`${process.env.MIX_LOCAL_API_URL}products`, payload)
    .then((response) => {
      resolve(response.data);
    })
    .catch(err => {
      reject(err)
    });
  });
}


export const getProductAttributes = () => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}product_attributes`)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}


export const getUserDetail = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}user_detail`, payload)
      .then((response) => {
        if (response.data.code === 200){
          resolve(response.data.data);
        } else {
          reject(response.data);
        }
      })
      .catch(err => {
        reject(err)
      });
  });
}

export const getProductFilterAttributes = (payload) => {
  return new Promise((resolve, reject) => {
    axios.post(`${process.env.MIX_LOCAL_API_URL}product_filter_attributes`, payload)
    .then((response) => {
      resolve(response.data.data);
    })
    .catch(err => {
      resolve(err);
    });
  });
}
