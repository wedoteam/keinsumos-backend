/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("./bootstrap");
window.Vue = require("vue");

//Localization
Vue.mixin(require("./trans"));
Vue.filter('trim', function (value, length = 30, trailChar = '...') {
  if (!value) return '';
  if (value.length < length) {
    return value;
  }
  const result = `${value.substr(0, length)}${trailChar}`;
  return result;
})

import { Quill } from 'vue-quill-editor';
const Block = Quill.import('blots/block');
Block.tagName = 'DIV';
Quill.register(Block, true);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
import VueRouter from "vue-router";
Vue.use(VueRouter);

import DataTable from "laravel-vue-datatable";
Vue.use(DataTable);

import VTooltip from 'v-tooltip';
Vue.use(VTooltip);

import VueMask from 'v-mask';
Vue.use(VueMask, {
  placeholders: {
    'S': /[a-zA-Z ]/, // space and letters only
  }
});

import * as VueGoogleMaps from 'vue2-google-maps';
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBOZo1E-HgYSl0IFH9PIUFBxmA7sc7YaEY',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    //// If you want to set the version, you can do so:
    // v: '3.26',
  }, 
  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  autobindAllEvents: true,
 
  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
});

import VueScrollTo from 'vue-scrollto';
Vue.use(VueScrollTo, {
  duration: 500,
  force: true,
  x: false,
  y: true
});

import vueDebounce from 'vue-debounce';
Vue.use(vueDebounce, {
  listenTo: ['input']
});

import admin_routes from "./admin-routes";
import user_routes from "./routes";

var routes = typeof ROUTE_FILE === "undefined" ? user_routes : admin_routes;

import * as apiServices from './services/apiServices.services';
import * as apiAdminServices from './services/apiAdminServices.services';
Vue.prototype.$api = typeof ROUTE_FILE === "undefined" ? apiServices : apiAdminServices;

const router = new VueRouter({
    routes,
    base: VUE_BASE,
    mode: 'history',
});
router.beforeEach((to, from, next) => {
  if(document.querySelector('#accordionSidebar')) {
    document.querySelector('#accordionSidebar').classList.remove('active');
  }

  if (screen.width < 992) {
    setTimeout(() => {
      VueScrollTo.scrollTo('body', 100, {onDone: () => next()});
    },100);
  } else {
    next();
  }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("spinner", require("vue-simple-spinner"));

import ToggleButton from "vue-js-toggle-button";
Vue.use(ToggleButton);

import Toaster from "v-toaster";
import "v-toaster/dist/v-toaster.css";
Vue.use(Toaster, { timeout: 5000 });

require("./jquery.dataTables.js");
require("pusher-js");

import VueGoogleCharts from 'vue-google-charts'
Vue.use(VueGoogleCharts);

import VueGtag from "vue-gtag";
Vue.use(VueGtag, {
  config: { id: "UA-160791560-1" }
});

Vue.mixin({
  data: function() {
    return {
      get GtagTrackingPageLocation() {
        return BASE_URL;
      },
      get GtagTrackingPagePath() {
        return PAGE_PATH;
      },
    }
  }
});

// import VueGtm from 'vue-gtm';
// Vue.use(VueGtm, {
//   id: 'GTM-TKX4634',
//   enabled: true,
//   debug: true,
//   loadScript: true,
//   vueRouter: router,
// });

import App from "./Handler";

Vue.config.errorHandler = function(err, vm, info) {
  console.log("[Global Error Handler]: Error in " + info + ": " + err);
};

const app = new Vue({
  el: "#app",
  render: h => h(App),
  router,
});
