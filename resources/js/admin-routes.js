import login from "./components/Admin/Login.vue";
import my_account from "./components/Admin/My_account.vue";
import dashboard from "./components/Admin/Dashboard.vue";
import admin_users from "./components/Admin/Admin_users.vue";
import products from "./components/Admin/Products.vue";
import add_product from "./components/Admin/Add_product.vue";
import transactions from "./components/Admin/Transactions.vue";
import reviews from "./components/Admin/Reviews.vue";
import app_users from "./components/Admin/App_users.vue";
import notifications from "./components/Admin/Notifications.vue";
import billing from "./components/Admin/Billing.vue";
import faq from "./components/Admin/Faq.vue";
import terms_and_conditions from "./components/Admin/Terms_and_conditions.vue";
import page_not_found from "./components/404.vue";

export default [
    {
      path: "*",
      component: page_not_found,
      name: "page_not_found"
    },
    {
      path: "/iniciar-sesión",
      component: login,
      name: "admin-login"
    },
    {
      path: "/",
      component: dashboard,
      name: "dashboard",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/mi-cuenta",
      component: my_account,
      name: "my_account"
    },
    {
      path: "/usuarios-del-administratrador",
      component: admin_users,
      name: "admin_users",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/productos",
      component: products,
      name: "products",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/añadir-producto",
      component: add_product,
      name: "add-product",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/editar-producto/:id",
      component: add_product,
      name: "edit-product",
      props: true,
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/transacciones",
      component: transactions,
      name: "transactions",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/reseñas",
      component: reviews,
      name: "reviews",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/usuarios-de-la-app",
      component: app_users,
      name: "app_users",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/notificaciones",
      component: notifications,
      name: "notifications",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/facturación",
      component: billing,
      name: "billing",
      meta: { requiresAdminAuth: true }
    },
    {
      path: "/ayuda",
      component: faq,
      name: "faq",
        meta: { requiresAdminAuth: true }
    },
    {
      path: "/términos-y-condiciones",
      component: terms_and_conditions,
      name: "terms_and_conditions",
      meta: { requiresAdminAuth: true }
    },
];
