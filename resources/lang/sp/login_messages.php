<?php
return [
  'account_not_found'							=> "Cuenta no encontrada.",
  'unauthorized_access'				    => '¡Acceso no autorizado!',
  'your_account_is_disabled'			=> 'El usuario está en proceso de aprobación',
  'please_enter_valid_password'   => 'Por favor, ingrese una contraseña válida.',
  'successfully_logged_in'        => 'Con éxito, inició sesión.',
];
