<?php
return [
  'password_reset'                      => 'Restablecimiento de contraseña',
  'the_app_team' 						=> 'El equipo [[APP_NAME]]',
  'enter_the_new_password_while_login'	=> 'Ingrese la nueva contraseña mientras inicia sesión.',
  'your_sincerely'                      => 'Tuyo sinceramente,',
  'this_email_cant_received_replies' 	=> 'Este correo electrónico no puede recibir respuestas.',
  'you_received_this_email_because_we_received_a_request_for_reset_password_for_your_account' => 'Recibió este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.',
  'select_logout_below_if_you_are_ready_to_end_your_current_session' => 'Seleccione "Cerrar sesión" a continuación si está listo para finalizar su sesión actual.',
];
