<?php
return [
  'password_reset'                      => 'Password Reset',
  'the_app_team' 						=> 'The [[APP_NAME]] Team',
  'enter_the_new_password_while_login'	=> 'Enter the new password while login.',
  'your_sincerely'                      => 'Yours sincerely,',
  'this_email_cant_received_replies' 	=> 'This email can\'t receive replies.',
  'you_received_this_email_because_we_received_a_request_for_reset_password_for_your_account' => 'You received this email because we received a request for reset password for your account.',
  'select_logout_below_if_you_are_ready_to_end_your_current_session' => 'Select "Logout" below if you are ready to end your current session.',
];
