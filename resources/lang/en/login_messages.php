<?php
return [
  'account_not_found'							=> "Account not found.",
  'unauthorized_access'				    => 'Unauthorized access!',
  'your_account_is_disabled'			=> 'Your account is disabled!',
  'please_enter_valid_password'   => 'Please, enter valid password.',
  'successfully_logged_in'        => 'Successfully, logged in.',
];
