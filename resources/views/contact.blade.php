@extends('layouts.app')
  @section('title')
    <title>{{ __('labels.page_title.contact') }} | {{ config('app.name') }}</title>
    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/template/css/jquery.fancybox.css') }}" />
  @endsection 
  @section('content')
  <div class="banner-form-wrap contact-banner header_bg">

    <!-- CONTENT FOR DESKTOP -->
    <div class="desktop-content">
      <h1>{{ __('labels.frontend.we_are_here_to_help_you') }}</h1>
    </div>
    <!-- CONTENT FOR DESKTOP -->

    <!-- CONTENT FOR MOBILE -->
    <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
      <div class="mobile-banner-logo">
        <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
      </div>
      <div class="mobile-heading">
        <h1 class="text-center">{{ __('labels.frontend.we_are_here_to_help_you') }}</h1>
      </div> 
    </div>
    <!-- CONTENT FOR MOBILE -->

  </div>    
  
  <div class="success-section contact-section">
    <div class="container footer-bottom-container">  
      <div class="wizard__header-content">   
        <div class="success-content-wrap">
          <h2>{{ __('labels.frontend.contact') }}</h2>
          <h3>{{ __('labels.frontend.write_us_and_an_advisor_will_contact_you_shortly') }}</h3>
          <form method="POST" action="{{ URL::to('contacto/submit') }}" class="form-register pt-4" id="contact-form">
            @csrf
            @if($errors->any())
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="alert alert-danger">
                  {{$errors->first()}}
                </div>
              </div>
            </div>
            @endif
            @if(session('success'))
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="alert alert-success">
                  {{ session('success') }}
                </div>
              </div>
            </div>
            @endif
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="{{ __('labels.frontend.name') }}" required="" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control" id="email" placeholder="{{ __('labels.frontend.email') }}" required="" value="{{ old('email') }}">
                </div>
                <div class="form-group">
                  <input type="text" name="phone" class="form-control" id="phone" placeholder="{{ __('labels.frontend.telephone') }}" required="" value="{{ old('phone') }}">
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <textarea name="message" class="form-control" id="message" placeholder="{{ __('labels.frontend.message') }}" required="" style="resize: none;" rows="5">{{ old('message') }}</textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"></div>
              </div>
            </div>
            <div class="row pt-3 pb-3">
              <div class="col-12">
                <div class="form-group">
                  <button type="submit" class="btn btn-success">{{ __('labels.frontend.send') }}</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="float-sm">
      <div class="fl-fl float-wa">
        <i class="fab fa-whatsapp"></i>
        <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
      </div>
    </div>
  </div>
  @endsection
  @section('script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
  @endsection

