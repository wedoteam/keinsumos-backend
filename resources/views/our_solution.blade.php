@extends('layouts.advanced')
  @section('title')
  <title>{{ __('labels.page_title.our_solution') }} | {{ config('app.name') }}</title>
  @endsection 
  @section('content')
  <section >
    <div class="banner_part-wrap " style="background-image:url({{ asset('public/template/images/ke_insumos_bg.png') }});"> 
      <div class="banner_mobile_views animatedParent"  style="overflow: hidden;"> 
        <div class="mobile-content home-mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
          <div class="mobile-banner-logo">
            <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
          </div>
          <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
        </div>
      </div>
      <div class="banner-left-part">
        <h1 class="banner_before animated bounceInDown go">Nuestra solución</h1>      
      </div>
      <div class="banner-right-part animated bounceInDown go">
        <div class="banner_content">        
          <p>El lugar más fácil para <strong> comprar</strong> y <strong>vender agroinsumos</strong></p>       
        </div>
      </div>
    </div>
  </section>
  <section class="cards-info-section" >
    <div class="container">
      <div class="row cards-info-wrap animatedParent" style="overflow: hidden;">
        <div class="col-md-4 text-center animated fadeIn delay-250">
          <div class="contact-part">
            <div class="img-wrap"> 
              <img src="{{ asset('public/template/images/e-market_place.png') }}" alt="Location Icon"> 
            </div>
            <h4>e-Market Place</h4>
            <p>Mediante un catálogo online de registro gratuito se busca unir a productores y proveedores para realizar transacciones de compra venta de agro insumos. Las cotizaciones son inmediatas, pudiendo cerrar una transacción en minutos en cualquier momento y desde cualquier lugar. Todos los productos cumplen los requerimientos establecidos por SENASA e INASE para su comercialización.</p>
          </div>
        </div>
        <div class="col-md-4 text-center animated fadeIn delay-500">
          <div class="contact-part">
            <div class="img-wrap"> 
              <img src="{{ asset('public/template/images/ambiente_controlado.png') }}" alt="Contact Icon"> 
            </div>
            <h4>Ambiente controlado</h4>
            <p>Diseñamos un ambiente digital seguro, donde los participantes son validados al momento de registrarse. Ninguna Agronomía puede ver el precio publicado por otra. Productores y Agronomías son calificados luego de la transacción. Las comunicaciones y calificaciones son moderadas dentro de la plataforma.</p>
          </div>
        </div>
        <div class="col-md-4 text-center animated fadeIn delay-750">
          <div class="contact-part">
            <div class="img-wrap"> 
              <img src="{{ asset('public/template/images/comunicación_instantánea.png') }}" alt="Email Icon"> 
            </div>
            <h4>Comunicación instantánea</h4>
            <p>Una vez cerrada la operación, los detalles, formas de entrega se coordinan entre comprador y vendedor a través de un chat privado.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section >
    <div class="banner_part-wrap celular_app" > 
      <div class="banner-left-part celular_app_content animatedParent" style="overflow: hidden;">
        <h4 class="banner_before animated fadeIn delay-750">Plataforma Móvil para el Celular - App</h4>
        <p>Diseñada para la mayor comodidad y máxima funcionalidad operativa del usuario</p> 
        <h5> Otras ventajas son:</h5>  
        <ul class="otras_ventajas_son_list">
          <li class="check_list">Diseñada para que los productores puedan planificar la compra de insumos de manera estratégica y eficaz.</li>
          <li class="check_list">Interfaz fácil de utilizar, robusta y confiable.</li>
          <li class="check_list">Ofrece diversidad de opciones para elegir la propuesta de mayor conveniencia.</li>
          <li class="check_list">Se puede descargar en forma gratuita desde cualquier celular, a través de Google Play o Apple Store.</li>
        </ul>   
      </div>
      <div class="banner-right-part celular_app_right animatedParent">
        <div class="banner_content celular_app_right_content mbt_mobile animated fadeInUp">       
          <img src="{{ asset('public/template/images/app_mobile.png') }}" alt="Email Icon">      
        </div>
      </div>
    </div>
  </section>
  <section class="cta_btn animatedParent" style="overflow: hidden;">
    <div class="container">
      <div class="cta_btn_contnet animated bounceInLeft slow">
        <div class="cta_btn_title">
          <h4>DESCARGA LA APP</h4>
        </div>
        <div class="banner-btn-grp cta_btn_parent">
          <a href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/ke_insumos_android-2.png') }}" alt="Google Play">
            </div>
            <div class="icon-txt">    
              <span class="big-txt">Google play</span>
            </div>
          </a>
          <a href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/ke_insumos_iOS2.png') }}" alt="Apply">
            </div>
            <div class="icon-txt">
              <span class="big-txt">App Store</span>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  <section >
    <div class="plataforma_web animatedParent" style="overflow: hidden;"> 
      <div class="plataforma_web_content">
        <div class="plataforma_web_content_img">        
          <img src="{{ asset('public/template/images/desktop_bg.png') }}" alt="Email Icon" class="animated bounceInDown  delay-450">      
        </div>
      </div>
      <div class="plataforma_web_content_text">
        <h4 class="banner_before">Plataforma Web</h4>
        <p>Es un canal alternativo de acceso a la plataforma que complementa a la Aplicación Móvil</p> 
        <h5> Otras ventajas son:</h5>  
        <ul class="otras_ventajas_son_list">
          <li class="check_list">Posee las mismas funcionalidades que la Aplicación Móvil.</li>
          <li class="check_list">Ofrece mayor conveniencia a la Agronomía, desde la comodidad de su escritorio, para:
            <ul class="otras_ventajas_sub_list">         
              <li class="redio_list">la modificación de los parámetros y características de su oferta de productos.</li>
              <li class="redio_list">la aprobación de las transacciones.</li>
              <li class="redio_list">visualizar las métricas de gestión sobre la performance comercial de sus puntos de venta.</li>
            </ul>    
          </li>
        </ul>   
      </div>
    </div>
  </section>
  @endsection