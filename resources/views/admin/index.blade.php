<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('includes.google_analytics.head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="180x180">
    <link rel="shortcut icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="32x32" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}" sizes="16x16" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}">
    
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/fontawesome-free/css/all.min.css') }} ">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/bootstrap.min.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/dashboard.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/signin.css') }}">
    <style type="text/css">
      body {
        font-family: sans-serif !important;
      }
    </style>
  </head>
  <body>
    @include('includes.google_analytics.body')
    <div id="app"></div>
    <script>
      var ROUTE_FILE = 'admin-routes';
      var BASE_URL   = "{{ URL::to('/') }}";
      var VUE_BASE   = "{{ $vue_base }}";
      var PAGE_PATH   = "{{ $page_path }}";
      window._translations = {!! cache('translations') !!};
    </script>
    <script src="{{ asset('public/js/app.js?v=20.04.02') }}"></script>
  </body>
</html>
