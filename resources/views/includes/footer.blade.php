<div class="float-sm">
	<div class="fl-fl float-wa">
		<i class="fab fa-whatsapp"></i>
		<a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
	</div>
</div>
<footer class="animatedParent" data-sequence="1000" style="overflow: hidden;">	
  <div class="container">	
    <div class="footer-wrap row animated growIn go" data-id="1"> 
      <div class="footer-part"> 
        <p>© {{ date('Y') }} Ke Insumos. <i>Todos los derechos reservados.</i></p> 
      </div>  
      <div class="footer-part"> 
        <div class="footer-logo-content">
          <a href="http://piamweb.com" target="_blank">
            <img src="{{ asset('public/template/images/footer_logo.png') }}">
          </a>
        </div> 
      </div>  
    </div> 
    <div class="mobile-footer">
    	<div class="container">
    		<ul>
				@if($logged_in_user)
					@if($logged_in_user->user_type == config('global.BUSINESS_USER'))
					<li><a href="{{ URL('agronomia') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
					@elseif($logged_in_user->user_type == config('global.CUSTOMER_USER'))
					<li><a href="{{ URL('producer') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
					@else
					<li><a href="{{ URL('admin') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
					@endif
				@else
    			<li><a href="{{ URL('login') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
    			<li><a href="{{ URL('register') }}"><img src="{{ asset('public/template/images/ic_registrarse_deactive.png') }}"></a></li>
				@endif
					<li><a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"><img src="{{ asset('public/template/images/ic_whatsapp_deactive.png') }}"></a></li>
    			<li><a href="{{ URL('/ayuda') }}"><img src="{{ asset('public/template/images/ic_faqs_deactive.png') }}"></a></li>
    		</ul>
    	</div>
    </div>
  </div>	
</footer>