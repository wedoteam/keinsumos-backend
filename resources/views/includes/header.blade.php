<header class="cd-main-header fixed-top bg-color-light-dark logo_mobile_views">
  <div class="container-fluid">
    <div class="container__header">
      <div class="button__posi">
        <a class="cd-logo" href="{{ URL('') }}">
          <img src="{{ asset('public/template/images/home_logo.png') }}">
        </a>
        <div class="button__posi__fix" >
          <ul class="cd-header-buttons">
						<li><a class="cd-nav-trigger " href="#cd-primary-nav"><span></span></a></li>
					</ul>
        </div>
        <!-- cd-header-buttons -->
      </div>
    </div>
  </div>
</header>
<main class="cd-main-content">   
  <!-- your content here --> 
  <div class="cd-overlay"></div>   
</main>
<nav class="cd-nav navbar navbar-expand-lg navbar-dark">
<ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
  <li><a href="{{ URL('/quiénes-somos') }}">{{ __('labels.frontend.about_us') }}</a></li>
  <li><a href="{{ URL('/nuestra-solución') }}">{{ __('labels.frontend.our_solution') }}</a></li>
  <li class="has-children before__child">
    <a href="#" class="nav_next_arrow">{{ __('labels.frontend.proposal_value') }}</a>
    <ul class="cd-secondary-nav is-hidden">
      <li class="go-back"><a href="#0" class="selected nav_back_arrow">0</a></li>
			<li><a href="{{ URL('/para-las-agronomías') }}" >{{ __('labels.frontend.for_agronomia') }}</a></li>
			<li><a href="{{ URL('/para-los-productores') }}">{{ __('labels.frontend.for_producer') }}</a></li>     
    </ul>
  </li>
  <li><a href="{{ URL('/contacto') }}">{{ __('labels.frontend.contact') }}</a></li>
  <li><a href="{{ URL('/ayuda') }}">{{ __('labels.frontend.faq') }}</a></li>    
  <div class="nav__btn custom-btn-grp custom-btn-grp1">
  	@if(!empty($logged_in_user))
      @if($logged_in_user->user_type == config('global.BUSINESS_USER'))
      <a class="btn btn-outline-primary text-white outline2 hide-in-mobile-menu" href="{{ URL('agronomia') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
      @elseif($logged_in_user->user_type == config('global.CUSTOMER_USER'))
      <a class="btn btn-outline-primary text-white outline2 hide-in-mobile-menu" href="{{ URL('producer') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
      @else
      <a class="btn btn-outline-primary text-white outline2 hide-in-mobile-menu" href="{{ URL('admin') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
      @endif
      <a class="btn btn-outline-primary fill-bg" href="{{ URL('logout') }}"><b>{{ __('labels.frontend.logout') }}</b></a>
  	@else
    <a class="btn btn-outline-primary text-white outline2" href="{{ URL('login') }}"><b>{{ __('labels.frontend.login') }}</b></a>
    <a class="btn btn-outline-primary fill-bg" href="{{ URL('register') }}"><b>{{ __('labels.frontend.register') }}</b></a>
    @endif
  </div>
</ul>
</nav>