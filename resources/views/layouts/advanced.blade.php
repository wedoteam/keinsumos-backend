<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('includes.google_analytics.head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    @yield('title')
    <link rel="stylesheet" href="{{ asset('public/template/css/jquery.fancybox.css') }}" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="public/favicon_32x32.ico" sizes="180x180">
    <link rel="shortcut icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="32x32" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}" sizes="16x16" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}">
    
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('public/template/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('public/template/css/product.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/menu-style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/desktop_nuestra_solucion.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/template/css/animations.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/style.css') }}">
    @yield('style')
  </head>
  <body>
    @include('includes.google_analytics.body')
    @include('includes.header')
    @yield('content')
    @include('includes.footer')
    <script src="{{ asset('public/template/js/jquery-3.1.0.js') }}"></script>
    <script src="{{ asset('public/template/js/popper.min.js') }}"></script>
    <script src="{{ asset('public/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/template/js/jquery.fancybox.js') }}"></script>
    <script src="{{ asset('public/template/js/jquery.fancybox-media.js') }}"></script>
    <script src="{{ asset('public/template/js/scripts.js') }}"></script>
    <script src="{{ asset('public/template/js/menu/modernizr.js') }}"></script> 
    <script src="{{ asset('public/template/js/menu/jquery.mobile.custom.min.js') }}"></script>
    <script src="{{ asset('public/template/js/menu/main.js') }}"></script>
    <script src="{{ asset('public/template/js/css3-animate-it.min.js') }}"></script>
  </body>
</html>