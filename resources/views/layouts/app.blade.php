<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('includes.google_analytics.head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="180x180">
    <link rel="shortcut icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="32x32" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}" sizes="16x16" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}">
    
    @yield('title')
    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap" rel="stylesheet">
    <link href="{{ asset('public/template/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/product.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/menu-style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/style.css') }}">
    @yield('style')
  </head>
  <body>
    @include('includes.google_analytics.body')
    @include('includes.header')
    @yield('content')
    <footer class="custom-footer">  
      <div class="container"> 
        <div class="footer-wrap row"> 
          <div class="footer-part"> 
            <p>© {{ date('Y') }} Ke Insumos. <i>Todos los derechos reservados.</i></p> 
          </div>  
          <div class="footer-part"> 
            <div class="footer-logo-content">
              <a href="http://piamweb.com" target="_blank">
                <img src="{{ asset('public/template/images/footer_logo.png') }}">
              </a>
            </div> 
          </div>  
        </div> 
        <div class="mobile-footer">
          <div class="container">
            <ul>
              @if($logged_in_user)
                <li><a href="{{ URL('logout') }}"><img src="{{ asset('public/template/images/ic_lgoout_deactive.png') }}"></a></li>
              @else
                @if((\Request::route()) && (\Request::route()->getName() == 'password.request' || \Request::route()->getName() == 'password.reset'))
                  <li class="active"><a href="{{ URL('login') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"><span class="text-uppercase">{{ __('labels.frontend.login') }}</span></a></li>
                @else
                  <li><a href="{{ URL('login') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
                @endif
                @if(isset($active_page) && $active_page == 'register')
                  <li class="active"><a href="{{ URL('register') }}"><img src="{{ asset('public/template/images/ic_registrarse_deactive.png') }}"><span class="text-uppercase">{{ __('labels.frontend.register') }}</span></a></li>
                @else
                  <li><a href="{{ URL('register') }}"><img src="{{ asset('public/template/images/ic_registrarse_deactive.png') }}"></a></li>
                @endif
              @endif

              <li><a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"><img src="{{ asset('public/template/images/ic_whatsapp_deactive.png') }}"></a></li>

              @if(isset($active_page) && $active_page == 'faq')
                <li class="active"><a href="{{ URL('/ayuda') }}"><img src="{{ asset('public/template/images/ic_faqs_deactive.png') }}"><span class="text-uppercase">{{ __('labels.frontend.faq') }}</span></a></li>
              @else
                <li><a href="{{ URL('/ayuda') }}"><img src="{{ asset('public/template/images/ic_faqs_deactive.png') }}"></a></li>
              @endif
        		</ul>
          </div>
        </div> 
      </div>  
    </footer>
    <script src="{{ asset('public/template/js/jquery-3.1.0.js') }}"></script>
    <script src="{{ asset('public/template/js/popper.min.js') }}"></script>
    <script src="{{ asset('public/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/template/js/scripts.js') }}"></script>
    <script src="{{ asset('public/template/js/menu/modernizr.js') }}"></script> 
    <script src="{{ asset('public/template/js/menu/jquery.mobile.custom.min.js') }}"></script>
    <script src="{{ asset('public/template/js/menu/main.js') }}"></script>
    @yield('script')
  </body>
</html>
