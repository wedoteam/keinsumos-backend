<table style="min-width:320px" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
  <tbody>
    <tr>
      <td>
        <table style="width:600px!important" width="600" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td style="min-width:600px;font-size:0;line-height:0">&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td style="padding:0 10px">
        <table width="100%" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td bgcolor="#eaeced">
                <table style="margin:0 auto;border-top:#40875a solid 5px" width="600" cellspacing="0" cellpadding="0" align="center">
                  <tbody>
                    <tr>
                      <td style="padding:34px 60px 50px" bgcolor="#FFFFFF">
                        <table width="100%" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td style="text-align:center">
                                <img src="{{ asset('public/template/images/home_logo.png') }}" style="vertical-align:top;text-align:center;margin:0 auto;margin-bottom:30px" alt="" class="CToWUd" width="212" height="43">
                              </td>
                            </tr>
                            <tr>
                              <td style="font:14px/23px Arial,Helvetica,sans-serif;color:#888888;padding:0 0 51px">
                                @yield('content')
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td height="28"></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
              <td bgcolor="#eaeced">
                <table style="margin:0 auto" width="600" cellspacing="0" cellpadding="0" align="center">
                  <tbody>
                    <tr>
                      <td style="padding:0 0 10px">
                        <table width="100%" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <th style="vertical-align:top;padding:0" width="100%" align="center">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                  <tbody>
                                    <tr>
                                      <td style="font:12px/16px Arial,Helvetica,sans-serif;color:#000000;padding:0 0 10px" align="center">
                                        <a href="mailto:{{ env('MIX_HELP_EMAIL') }}" target="_blank">
                                          {{ str_replace(':help_mail', env('MIX_HELP_EMAIL'), __('labels.mail_messages.if_you_have_any_questions_or_problems_write_to_help_mail')) }}
                                        </a>.
                                        {{ __('labels.mail_messages.if_you_already_have_your_active_account_go_to_the_app_in_the_section_my_ccount_and_chat_with_us') }}
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="font:12px/16px Arial,Helvetica,sans-serif;color:#797c82;padding:0 0 10px" align="center">
                                        {{ __( config('global.APP_COMPANY') ) }} {{ date('Y') }}. &nbsp; {{ __('labels.mail_messages.all_rights_reserved') }}
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </th>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table> 
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
    <tr>
      <td style="line-height:0"><div style="display:none;white-space:nowrap;font:15px/1px courier">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
    </tr>
  </tbody>
</table>