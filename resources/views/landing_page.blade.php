<!DOCTYPE html>
<html lang="en">
  <head>
    @include('includes.google_analytics.head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="180x180">
    <link rel="shortcut icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="32x32" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}" sizes="16x16" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}">

    <title>Landing | Ke Insumos</title>
    <!-- Custom fonts for this template-->
    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('public/template/css/jquery.fancybox.css') }}" />
    <!-- Bootstrap core CSS -->
    <!-- Custom styles for this template-->
    <link href="{{ asset('public/template/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/css3-animate-it/1.0.3/css/animations.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,600&display=swap" rel="stylesheet">
    <link href="{{ asset('public/template/css/landing-page-opcion.css') }}" rel="stylesheet">

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '276009957089736');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=276009957089736&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->  
  </head>
  <body id="page-top">
    @include('includes.google_analytics.body')
    <header>
      <div class="header_top_line" > 
        <div class="container" >
          <form class="form-inline my-2 my-lg-0">
            <div >
              <ul class="social_bar">
                <li class="ke_insumos_insta"><a href="https://www.instagram.com/keinsumos/" title="Instagram" target="_blank"></a></li>
                <li class="ke_insumos_facebook"><a href="https://www.facebook.com/keinsumosapp/" title="Facebook" target="_blank"></a></li>
                <li class="ke_insumos_twitter"><a href="https://twitter.com/KInsumos" title="Twitter" target="_blank"></a></li>
              </ul>
            </div>
          </form>
        </div>
      </div>
      <div class="container" >
        <nav class="navbar navbar-expand-lg navbar-light p-0 ">
          <a class="navbar-brand" href="#" ><img src="{{ asset('public/template/images/ke_insumos_landing_page_logo.png') }}" alt="Ke Insumos" class="desktop-logo" title="Ke Insumos"><img src="{{ asset('public/template/images/ke-insumos-mobile-logo.png') }}" alt="" class="mobile-logo"></a>
          <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active mobile-remove">
                <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#characteristics-section">Características</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#know-more-section">Video</a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link" href="#inquiry-form">Contacto</a>
              </li>
            </ul>
            <div class="header_login">
              @if(!empty($logged_in_user))
                @if($logged_in_user->user_type == config('global.BUSINESS_USER'))
                <a class="btn btn-outline-primary" href="{{ URL('agronomia') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
                @elseif($logged_in_user->user_type == config('global.CUSTOMER_USER'))
                <a class="btn btn-outline-primary" href="{{ URL('producer') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
                @else
                <a class="btn btn-outline-primary" href="{{ URL('admin') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
                @endif
                <a class="btn btn-primary" href="{{ URL('logout') }}"><b>{{ __('labels.frontend.logout') }}</b></a> 
              @else
              <a class="btn btn-outline-primary" href="{{ URL('login') }}"><b>{{ __('labels.frontend.login') }}</b></a>
              <a class="btn btn-primary" href="{{ URL('register') }}"><b>{{ __('labels.frontend.register') }}</b></a>
              @endif
            </div>
            <form class="form-inline my-2 my-lg-0 for_mobile">
              <div >
                <ul class="social_bar">
                  <li class="ke_insumos_insta"><a href="https://www.instagram.com/keinsumos/" title="Instagram" target="_blank"></a></li>
                  <li class="ke_insumos_facebook"><a href="https://www.facebook.com/keinsumosapp/" title="Facebook" target="_blank"></a></li>
                  <li class="ke_insumos_twitter"><a href="https://twitter.com/KInsumos" title="Twitter" target="_blank"></a></li>
                </ul>
              </div>
            </form>
          </div>
        </nav>
      </div>
    </header>
    <div class="banner_overlay"></div>
    <section class="hero_bg animatedParent animateOnce" style="overflow:hidden;">
      <div class="container">
        <div class="row hero_text" style="overflow:hidden;">
          <div class="col-lg-g col-xl-6 col-md-6 col-sm-12 col-12 cp_hero">
            
            <h1 class="title_line animated fadeIn delay-250">Cotización y compra de insumos de manera simple y rápida</h1>
            <p class="animated fadeIn delay-500">Ke Insumos es una herramienta que facilita la cotización y compra-venta de insumos agrarios. Pensada y desarrollada para simplificar los procesos de venta de las agronomías y de compra para los productores.</p>
            <div class="get_download_app animated fadeIn delay-750">
              <a class="btn btn-primary ke_insumos_android" href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank" role="button"> Google Play</a>
              <a class="btn btn-primary ke_insumos_iOS" href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank" role="button"></i>App Store</a>
            </div>
          </div>
          <div class="col-lg-g col-xl-6 col-md-6 col-sm-12 col-12 d-flex zindex justify-content-center mo-img animatedParent animateOnce">
            <img src="{{ asset('public/template/images/ke_insumos_hero_mokup.png') }}" alt="ke_insumos_hero_mokup" class="animated fadeIn delay-1000" title=ke_insumos_mokup>
          </div>
        </div>
      </div>
      <a href="#characteristics-section" class="scroll-bottom"><span></span></a>
    </section>
    <section class="characteristics-section" id="characteristics-section">
      <div class="container">
        <div class="section-title">
          <h2>Características</h2>
        </div>
        <div class="characteristics-wrap row animatedParent animateOnce" style="overflow:hidden;">
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/¡Ahorrá tiempo!.png') }}" alt="¡Ahorrá tiempo!" title="¡Ahorrá tiempo!" class="animated fadeIn delay-250">
            </div>
            <h3>¡Ahorrá tiempo!</h3>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas. Sed sit amet malesuada libero.</p> -->
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Fácil_de_usar.png') }}" alt="Fácil de usar" title="Fácil de usar" class="animated fadeIn delay-500">
            </div>
            <h3>Fácil de usar</h3>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas. Sed sit amet malesuada libero.</p> -->
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/desde_cualquier_lugar.png') }}" alt="Desde cualquier lugar" title="Desde cualquier lugar" class="animated fadeIn delay-750">
            </div>
            <h3>Desde cualquier lugar</h3>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas. Sed sit amet malesuada libero.</p> -->
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Información_organizada.png') }}" alt="Información organizada" title="Información organizada" class="animated fadeIn delay-1000">
            </div>
            <h3>Información organizada</h3>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas.</p> -->
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Costo_cero.png') }}" alt="Costo cero" title="Costo cero" class="animated fadeIn delay-1250">
            </div>
            <h3>Costo cero</h3>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas.</p> -->
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Compra_estratégica.png') }}" alt="Compra estratégica" title="Compra estratégica" class="animated fadeIn delay-1500">
            </div>
            <h3>Compra estratégica</h3>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas.</p> -->
          </div>
        </div>
      </div>
    </section>
    <section id="know-more-section" class="know-more-section" style="background-image:url({{ asset('public/template/images/video-bg-img.png') }});">
      <div class="container">
        <div class="know-more-section-content">
          <div class="section-title">
            <h2>¡Conocé más!</h2>
          </div>
          <div class="video-wrap">
           <a data-fancybox class="video" href="https://www.youtube.com/embed/ymonZH327iQ">
             <img src="{{ asset('public/template/images/ke_insumos_youtube_video.png') }}">
           </a>
          </div>
        </div>
      </div>
    </section>
    <section  id="inquiry-form" class="inquiry-form" style="background-image:url({{ asset('public/template/images/form-bg-img.png') }});">
      <div class="container">
        <div class="inquiry-form-full-wrap">
          <div class="section-title">
            <h2>Estamos a disposición <br>para cualquier consulta.</h2>
            <p>Completá el formulario y te responderemos a la brevedad.</p>
          </div>
          <div class="inquiry-form-wrap">
            <form class="needs-validation" novalidate id="contact_form">
              <div class="col-md-12 mb-3">
                <label for="validationCustom01">Nombre</label>
                <input type="text" class="form-control" id="validationCustom01" name="name" placeholder="Ingresar nombre" required>
                <div class="valid-feedback">
                  Looks good!
                </div>
              </div>
              <div class="col-md-12 mb-3">
                <label for="validationCustom02">Email</label>
                <input type="email" class="form-control" id="validationCustom02" name="email" placeholder="Ingresar Email" required>
                <div class="valid-feedback">
                  Looks good!
                </div>
              </div>
              <div class="col-md-12 mb-3">
                <label for="validationCustom03">Teléfono</label>
                <input type="text" class="form-control" id="validationCustom03" name="phone" placeholder="Ingresar teléfono" required>
                <div class="invalid-feedback">
                  Please provide a valid phone Number.
                </div>
              </div>
              <div class="col-md-12 mb-3">
                <label for="validationCustom04">Mensaje</label>
                <textarea class="form-control" id="validationCustom04" rows="1" name="message" required placeholder="Ingresar mensaje"></textarea>
                <div class="invalid-feedback">
                  Please provide a Mensaje.
                </div>
              </div>
              <div class="text-center form-btn-wrap" id="contact_message">
                <button class="btn btn-primary" type="submit" id="submit">ENVIAR</button>
              </div>
            </form>
          </div>
          <div class="download-wrap">
            <h4>No pierdas tiempo, descargá la App.</h4>
            <div class="get_download_app">
              <a class="btn btn-primary ke_insumos_android" href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank" role="button"> Google Play</a>
              <a class="btn btn-primary ke_insumos_iOS" href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank" role="button">App Store</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer>
      <div class="container">
        <div class="footer-wrap">
          <div class="footer-logo">
            <a class="navbar-brand" href="#"><img src="{{ asset('public/template/images/ke_insumos_landing_page_logo.png') }}" alt=""></a>
          </div>
          <div class="social-wrap">
            <div>
              <h6 class="email-wrap"><a href="mailto:info@keinsumos.com">info@keinsumos.com</a></h6>
            </div>
            <ul class="social_bar">
              <li class="ke_insumos_insta"><a href="https://www.instagram.com/keinsumos/" title="Instagram" target="_blank"></a></li>
              <li class="ke_insumos_facebook"><a href="https://www.facebook.com/keinsumosapp/" title="Facebook" target="_blank"></a></li>
              <li class="ke_insumos_twitter"><a href="https://twitter.com/KInsumos" title="Twitter" target="_blank"></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="back-to-top-wrap" style="display: flex;">
        <a href="#" class="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
      </div>
    </footer>

    <div class="float-sm">
      <div class="fl-fl float-wa">
        <i class="fab fa-whatsapp"></i>
        <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="{{ asset('public/template/js/jquery.fancybox.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/css3-animate-it/1.0.3/js/css3-animate-it.min.js"></script>
    <script src="{{ asset('public/template/js/scripts.js') }}"></script>
    <!-- Custom scripts for all pages-->

    <script>
      jQuery(document).ready(function() {
        jQuery('#contact_form').on('submit', function(e){
          e.preventDefault();
          if(document.querySelector('#contact_form').checkValidity()) {
            const data = {};
            data.name = $('[name="name"]').val();
            data.email = $('[name="email"]').val();
            data.phone = $('[name="phone"]').val();
            data.message = $('[name="message"]').val();
            jQuery('#contact_form #submit').attr('disabled', true);
            jQuery.ajax({
              url: "{{ env('MIX_LOCAL_API_URL') }}contact_form",
              type: "POST",
              data: {
                _token: "{{ csrf_token() }}",
                data: data
              },
              dataType: 'JSON',
              success: function(response) {
                $('#contact_message').prepend('<div class="alert alert-success">' + response.message + '</div>');
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event' : 'FORMULARIO'
                });
                setTimeout(() => {
                  $('#contact_message .alert').remove();
                },4000);
              },
              complete: function() {
                jQuery('#contact_form').removeClass('was-validated');
                jQuery('#contact_form #submit').attr('disabled', false);
                jQuery('#contact_form')[0].reset();
              }
            });
          }
        });
        jQuery(document).on('click', '.float-wa a', function(e) {
          e.preventDefault();
          window.dataLayer = window.dataLayer || [];
          window.dataLayer.push ({
            'event': 'WHATSAPP'
          });
          window.open($(this).attr('href'), '_blank');
        });
      });
    </script>
    
  </body>
</html>