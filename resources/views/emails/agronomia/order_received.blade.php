@extends('layouts.email')
  @section('content')
    {{ __('labels.mail_messages.hi') }} {{ $order->agronomia->name }}!
    <br>{{ str_replace(':order_no', $order->custom_order_no, __('labels.mail_messages.you_have_received_a_new_order_x')) }}
    <br><br>{{ __('labels.mail_messages.enter_my_orders_to_approve_the_order') }}
    <br><br>{{ __('labels.mail_messages.any_questions_do_not_hesitate_to_write_to_us_by_the_whatsapp_chat_that_is_on_the_website') }}
    <br><br>
    {{ __('labels.mail_messages.regards') }}
  @endsection 
