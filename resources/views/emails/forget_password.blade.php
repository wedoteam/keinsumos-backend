@extends('layouts.email')
  @section('content')
  {{ __('labels.mail_messages.hi') }} {{ $user->name }}!
  <br><br>{{ __('labels.mail_messages.you_received_this_email_because_we_received_a_password_reset_request_for_your_account') }}
  <br><br>
  <div style="text-align: center;">
    <a href="{{ URL('password/reset/'.$token.'?email='.$user->email) }} " target="_blank" style="box-sizing: border-box; border-radius: 3px; color : #fff; display: inline-block; text-decoration: none; background-color: #40875a; border-top: 10px solid #40875a; border-right: 18px solid #40875a; border-bottom: 10px solid #40875a; border-left: 18px solid #40875a;">
      {{ __('labels.mail_messages.reset_password') }}
    </a>
  </div>
  <br>{{ __('labels.mail_messages.this_password_reset_link_will_expire_in_60_minutes') }}
  <br><br>{{ __('labels.mail_messages.if_you_did_not_request_a_password_reset_no_further_action_is_required') }}
  <br><br>{{ __('labels.mail_messages.regards') }}
  <br>{{ config('global.APP_COMPANY') }}
  @endsection 