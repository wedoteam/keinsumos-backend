@extends('layouts.email')
  @section('content')
    <strong>Nombre: </strong> {{ $name }}<br>
    <strong>Email: </strong> {{ $email }}<br>
    <strong>Teléfono: </strong> {{ $phone }}<br>
    <strong>Asunto: </strong><br> {!! nl2br($body) !!}
  @endsection