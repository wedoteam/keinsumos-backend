@extends('layouts.email')
  @section('content')
    {{ __('labels.mail_messages.hi') }} {{ $order->producer->name }}!
    <br>{{ str_replace(':order_no', $order->custom_order_no, __('labels.mail_messages.your_order_x_was_shipped_we_will_notifiy_you_when_agronomy_accepts_it')) }}
    <br><br>{{ __('labels.mail_messages.any_questions_do_not_hesitate_to_write_to_us_by_the_whatsapp_chat_that_is_on_the_website') }}
    <br><br>
    {{ __('labels.mail_messages.regards') }}
  @endsection 