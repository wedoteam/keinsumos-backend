@extends('layouts.email')
  @section('content')
    {{ __('labels.mail_messages.hi') }} {{ $order->producer->name }}!
    <br>{{ str_replace(':order_no', $order->custom_order_no, __('labels.mail_messages.your_order_x_has_been_rejected')) }}
    <br><br>{{ __('labels.mail_messages.any_questions_do_not_hesitate_to_write_to_us_by_the_whatsapp_chat_that_is_on_the_website') }}
    <br><br>
    {{ __('labels.mail_messages.regards') }}
  @endsection 