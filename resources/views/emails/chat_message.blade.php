@extends('layouts.email')
  @section('content')
    {{ __('labels.mail_messages.hi') }} {{ $receiver->name }}!
    <br>{{ str_replace(':order_no', $order->custom_order_no, __('labels.mail_messages.you_have_received_a_new_x_order_chat_message')) }}
    <br>{{ __('labels.mail_messages.check_the_chat_section_to_see_message') }}
    <br><br>{{ __('labels.mail_messages.any_questions_do_not_hesitate_to_write_to_us_by_the_whatsapp_chat_that_is_on_the_website') }}
    <br><br>
    {{ __('labels.mail_messages.regards') }}
  @endsection 