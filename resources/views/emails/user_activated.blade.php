@extends('layouts.email')
  @section('content')
    {{ __('labels.mail_messages.hi') }} {{ $user->name }}! {{ __('labels.mail_messages.welcome') }}.
    <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ __('labels.mail_messages.your_account_has_been_activated') }}
    <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ __('labels.mail_messages.start_now_to_enjoy_all_the_advantages_of_using_ke_insumos_its_easy_fast_and_safe') }}
    <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ __('labels.mail_messages.any_questions_do_not_hesitate_to_write_to_us_by_the_whatsapp_chat_that_is_on_the_website') }}
    <br><br>{{ __('labels.mail_messages.regards') }}
  @endsection 