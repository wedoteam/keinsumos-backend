<!DOCTYPE html>
<html lang="en">
  <head>
    @include('includes.google_analytics.head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>landing desktop ke insumos </title>
    <!-- Custom fonts for this template-->
    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    
    <!-- Bootstrap core CSS -->
    <!-- Custom styles for this template-->
    <link href="{{ asset('public/template/css/bootstrap.min.css') }}" rel="stylesheet">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/css3-animate-it/1.0.3/css/animations.css">
    <link href="{{ asset('public/template/css/landing-page-opcion.css') }}" rel="stylesheet">    
  </head>
  <body id="page-top">
    @include('includes.google_analytics.body')
    <header>
      <div class="container" >
        <nav class="navbar navbar-expand-lg navbar-light p-0 ">
          <a class="navbar-brand" href="#"><img src="{{ asset('public/template/images/ke_insumos_landing_page_logo.png') }}" alt="Ke Insumos" class="desktop-logo" title="Ke Insumos"><img src="{{ asset('public/template/images/ke-insumos-mobile-logo.png') }}" alt="" class="mobile-logo"></a>
          <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active mobile-remove">
                <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#characteristics-section">Características</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#know-more-section">Video</a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link" href="#inquiry-form">Contacto</a>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <div >
                <ul class="social_bar">
                  <li class="ke_insumos_insta"><a href="" title=""></a></li>
                  <li class="ke_insumos_facebook"><a href="" title=""></a></li>
                  <li class="ke_insumos_twitter"><a href="" title=""></a></li>
                </ul>
              </div>
            </form>
          </div>
        </nav>
      </div>
    </header>
    <div class="banner_overlay"></div>
    <section class="hero_bg hero-form-section  animatedParent animateOnce" style="overflow:hidden;">
      <div class="container">
        <div class="row hero_text">
          <div class="col-lg-g col-xl-6 col-md-6 col-sm-12 col-12 cp_hero">
            
            <h1 class="title_line animated fadeIn delay-250">Cotización de
            insumos de manera simple y rápida</h1>
            <p class="animated fadeIn delay-500">Ke Insumos es una herramienta que facilita la cotización de insumos agrarios. Pensada y desarrollada para facilitar procesos en las agronomías y a los productores. </p>
            <div class="get_download_app animated fadeIn delay-750">
              <a class="btn btn-primary ke_insumos_android" href="#" role="button"> Google Play</a>
              <a class="btn btn-primary ke_insumos_iOS" href="#" role="button"></i>App Store</a>
            </div>
          </div>
          <div class="col-lg-g col-xl-6 col-md-6 col-sm-12 col-12 d-flex zindex justify-content-center mo-img">
              <div class="inquiry-form-wrap animated fadeIn delay-1000">
                <h3 class="form-title">contactanos</h3>
              <form class="needs-validation" novalidate>
                <div class="col-md-12 mb-3">
                  <label for="validationCustom01">Nombre</label>
                  <input type="text" class="form-control" id="validationCustom01" placeholder="Ingresar nombre" required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-12 mb-3">
                  <label for="validationCustom02">Email</label>
                  <input type="email" class="form-control" id="validationCustom02" placeholder="Ingresar Email" required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-12 mb-3">
                  <label for="validationCustom03">Teléfono</label>
                  <input type="text" class="form-control" id="validationCustom03" placeholder="Ingresar teléfono" required>
                  <div class="invalid-feedback">
                    Please provide a valid phone Number.
                  </div>
                </div>
                <div class="col-md-12 mb-3">
                  <label for="validationCustom04">Mensaje</label>
                  <textarea class="form-control" id="validationCustom04" rows="1" required placeholder="Ingresar mensaje"></textarea>
                  <div class="invalid-feedback">
                    Please provide a Mensaje.
                  </div>
                </div>
                <div class="text-center form-btn-wrap">
                  <button class="btn btn-primary" type="submit">ENVIAR</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <a href="#characteristics-section" class="scroll-bottom"><span></span></a>
    </section>
     <section class="characteristics-section" id="characteristics-section">
      <div class="container">
        <div class="section-title">
          <h2>Características</h2>
        </div>
        <div class="characteristics-wrap row animatedParent animateOnce" style="overflow:hidden;">
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/¡Ahorrá tiempo!.png') }}" alt="¡Ahorrá tiempo!" title="¡Ahorrá tiempo!" class="animated fadeIn delay-250">
            </div>
            <h3>¡Ahorrá tiempo!</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas. Sed sit amet malesuada libero.</p>
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Fácil_de_usar.png') }}" alt="Fácil de usar" title="Fácil de usar" class="animated fadeIn delay-500">
            </div>
            <h3>Fácil de usar</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas. Sed sit amet malesuada libero.</p>
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/desde_cualquier_lugar.png') }}" alt="Desde cualquier lugar" title="Desde cualquier lugar" class="animated fadeIn delay-750">
            </div>
            <h3>Desde cualquier lugar</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas. Sed sit amet malesuada libero.</p>
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Información_organizada.png') }}" alt="Información organizada" title="Información organizada" class="animated fadeIn delay-1000">
            </div>
            <h3>Información organizada</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas.</p>
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Costo_cero.png') }}" alt="Costo cero" title="Costo cero" class="animated fadeIn delay-1250">
            </div>
            <h3>Costo cero</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas.</p>
          </div>
          <div class="characteristics-part col-xl-4 col-md-6 col-12">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/Compra_estratégica.png') }}" alt="Compra estratégica" title="Compra estratégica" class="animated fadeIn delay-1500">
            </div>
            <h3>Compra estratégica</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend elit non dolor condimentum, sit amet pharetra erat egestas.</p>
          </div>
        </div>
      </div>
    </section>
    <section id="know-more-section" class="know-more-section landing-second-knowmore" style="background-image:url({{ asset('public/template/images/video-bg-img.png') }});">
      <div class="container">
        <div class="know-more-section-content">
          <div class="section-title">
            <h2>¡Conocé más!</h2>
          </div>
          <div class="video-wrap">
            <video  controls>
              <source src="{{ asset('public/template/video/small.mp4') }}" type="video/mp4" />
            </video>
          </div>
        </div>
      </div>     
    </section>
     <section  id="inquiry-form" class="inquiry-form landing-second-inquiry-form" style="background-image:url({{ asset('public/template/images/form-bg-img.png') }});">
        <div class="container">
          <div class="inquiry-form-full-wrap bottom-mockups-wrap" style="overflow:hidden;">
            <div class="section-title">
              <h2>Estamos a disposición <br>para cualquier consulta.</h2>              
              <a href="#" class="contactor">CONTACTAR</a>
            </div>
              <div class="bottom-mockups animatedParent animateOnce">                
                <div class="download-wrap animated fadeInLeft">
                  <h4>No pierdas tiempo,</br> descargá la App.</h4>
                  <div class="get_download_app">
                    <a class="btn btn-primary ke_insumos_android" href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank" role="button"> Google Play</a>
                    <a class="btn btn-primary ke_insumos_iOS" href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank" role="button">App Store</a>
                  </div>
                </div>
                <img src="{{ asset('public/template/images/ke_insumos_hero_mokup.png') }}" alt="" class="mo-mockup-img animated fadeInRight">
              </div>
          </div>
        </div>
      </section>
      <footer>
        <div class="container">
          <div class="footer-wrap">
            <div class="footer-logo">
              <a class="navbar-brand" href="#"><img src="{{ asset('public/template/images/ke_insumos_landing_page_logo.png') }}" alt=""></a>
            </div>
            <div class="social-wrap">             
                <div>
                  <h6 class="email-wrap"><a href="mailto:info@keinsumos.com">info@keinsumos.com</a></h6>
                </div>
                <ul class="social_bar">                  
                  <li class="ke_insumos_insta"><a href="#" title=""></a></li>
                  <li class="ke_insumos_facebook"><a href="#" title=""></a></li>
                  <li class="ke_insumos_twitter"><a href="#" title=""></a></li>
                </ul>           
            </div>
          </div>
        </div>
        <div class="back-to-top-wrap" style="display: flex;">
        <a href="#" class="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        </div>
      </footer>
    <!-- Bootstrap core JavaScript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>    
     <script src="https://cdnjs.cloudflare.com/ajax/libs/css3-animate-it/1.0.3/js/css3-animate-it.min.js"></script>
    <script src="{{ asset('public/template/js/scripts.js') }}"></script>
    <!-- Custom scripts for all pages-->
    
  </body>
</html>