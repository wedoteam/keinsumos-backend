@extends('layouts.app')
  @section('title')
    <title>{{ __('labels.page_title.help') }} | {{ config('app.name') }}</title>
    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  @endsection 
  @section('style')
  <style type="text/css">
    @media (max-width: 576px) {
      .cd-nav-trigger { display: none; }
    }
  </style>
  @endsection
  @section('content')
  <div class="banner-form-wrap login-banner header_bg">
    <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
      <div class="mobile-menu back-btn">
        <a class="menu-icon-wrap" href="{{ URL('/') }}">
          <span></span>
        </a>
      </div>          
      <div class="mobile-banner-logo">
        <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
      </div>
    </div>
  </div>  
  <div class="success-section faq-section">
    <div class="container footer-bottom-container">
      <div class="wizard__header-content">        
        <div class="success-content-wrap">
          <h1>{{ __('labels.frontend.faq') }}</h1>
          @foreach($faq as $singleFaq)
          <div class="accordian-content-wrap">  
            <div class="accordian-title">
              <h2>{!! $singleFaq->question !!}</h2>
            </div>
            <div class="accordian-text">
              <p>{!! $singleFaq->reply !!}</p>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
    <div class="float-sm">
      <div class="fl-fl float-wa">
        <i class="fab fa-whatsapp"></i>
        <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
      </div>
    </div>
  </div>
  @endsection