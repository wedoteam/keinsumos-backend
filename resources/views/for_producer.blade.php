@extends('layouts.advanced')
  @section('title')
  <title>{{ __('labels.page_title.value_proposal_for_producer') }} | {{ config('app.name') }}</title>
  @endsection 
  @section('style')
  <style type="text/css">
    body { overflow-x: hidden; }
  </style>
  @endsection
  @section('content')
  <section >
    <div class="banner_part-wrap " style="background-image:url({{ asset('public/template/images/ke_insumos_bg.png') }} );"> 
      <div class="banner_mobile_views"> 
        <div class="mobile-content home-mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }} );">
          <div class="mobile-banner-logo">
            <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
          </div>
          <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
        </div>
      </div>
      <div class="banner-left-part propuesta_title animatedParent" style="overflow: hidden;">
        <h1 class="banner_before animated bounceInDown go">Propuesta de Valor <br> para <strong>Productores </strong></h1>
      </div>
      <div class="banner-right-part animated bounceInDown go">
        <div class="banner_content">        
          <p>Ke Insumos es una herramienta, al alcance de su mano, que tiene por objetivo facilitarle el <strong>proceso de cotización, comparación y compra </strong> de insumos, haciéndolo de una manera<strong> ágil, simple y rápida. </strong></p>         
        </div>
      </div>
    </div>
  </section>
  <section class="cards-info-section desktop_propuesta_de_valor_agronomias valor_productores_pt animatedParent" >
    <div class="container">
      <div class="row cards-info-wrap">
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center valor_productores_pt animated fadeIn delay-250">
          <div class="contact-part">
            <div class="img-wrap"> 
              <img src="{{ asset('public/template/images/¡Ahorrá_tiempos.png') }}" alt="Location Icon"> 
            </div>
            <h4 class="productores_card_title">¡Ahorrá tiempo!</h4>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12  text-center valor_productores_pt  animated fadeIn delay-500">
          <div class="contact-part">
            <div class="img-wrap"> 
              <img src="{{ asset('public/template/images/facil_de_usar.png') }}" alt="Contact Icon"> 
            </div>
            <h4 class="productores_card_title">Fácil de usar</h4>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12  text-center valor_productores_pt  animated fadeIn delay-750">
          <div class="contact-part">
            <div class="img-wrap "> 
              <img src="{{ asset('public/template/images/Desde_cualquier_lugars.png') }}" alt="Email Icon"> 
            </div>
            <h4 class="productores_card_title">Desde cualquier lugar</h4>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center valor_productores_pt  animated fadeIn delay-1000">
          <div class="contact-part">
            <div class="img-wrap"> 
              <img src="{{ asset('public/template/images/Información_organizadas.png') }}" alt="Email Icon"> 
            </div>
            <h4 class="productores_card_title"> Información organizada</h4>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center valor_productores_pt  animated fadeIn delay-1250">
          <div class="contact-part">
            <div class="img-wrap "> 
              <img src="{{ asset('public/template/images/Costo_ceros.png') }}" alt="Email Icon"> 
            </div>
            <h4 class="productores_card_title">Costo cero</h4>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 text-center valor_productores_pt animated fadeIn delay-1500">
          <div class="contact-part">
            <div class="img-wrap"> 
              <img src="{{ asset('public/template/images/Compra_estratégica.png') }}" alt="Email Icon"> 
            </div>
            <h4  class="productores_card_title">Compra estratégica</h4>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section >
    <div class="banner_part-wrap celular_app ofrecemos_video animatedParent" > 
      <div class="container">
        <div class="row align-items-center" style="overflow: hidden;">
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_content_display animated bounceInDown">
            <div class="ofrecemos_content">
              <h5>Ofrecemos a los Productores <strong>conveniencia y comodidad </strong> sin costo.</h5>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_video_thumbnail animated bounceInRight slow"> 
            <a data-fancybox class="full-click-wrap" href="https://www.youtube.com/embed/_c06hsTx64g">
              <img src="{{ asset('public/template/images/eres_productor.png') }}">
              <i class="fa fa-play-circle" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="cta_btn animatedParent" style="overflow: hidden;">
    <div class="container">
      <div class="cta_btn_contnet animated bounceInLeft slow">
        <div class="cta_btn_title">
          <h4>DESCARGA LA APP</h4>
        </div>
        <div class="banner-btn-grp cta_btn_parent">    
          <a href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/ke_insumos_android-2.png') }}" alt="Google Play">
            </div>
            <div class="icon-txt">  
              <span class="big-txt">Google play</span>
            </div>
          </a>
          <a href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/ke_insumos_iOS2.png') }}" alt="Apply">
            </div>
            <div class="icon-txt">
              <span class="big-txt">App Store</span>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  @endsection