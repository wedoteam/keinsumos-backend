@extends('layouts.app')
@section('title')
<title>{{ __('labels.terms_and_conditions') }} | {{ config('app.name') }}</title>
@endsection
@section('style')
<style type="text/css">
  @media (max-width: 576px) {
    .cd-nav-trigger { display: none; }
  }
</style>
@endsection
@section('content')
<div class="banner-form-wrap login-banner header_bg term-con-section">
  <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
    <div class="mobile-menu back-btn">
      <a class="menu-icon-wrap" href="{{ URL('register/'.$user) }}">
        <span></span>
      </a>
    </div>
    <div class="mobile-banner-logo">
      <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
    </div>
  </div>
</div>
<section class="position-relative terms-and-condition-wrap pt-3 pt-lg-0 pb-5 mt-lg-n10 iniciar__sesion registration-wrap footer-bottom-container">
  <div class="container">
    <div class="col-xl-12 col-lg-12">
      <div class="card shadow-sm">
        <div class="card-body">
          <div class="desktop-menu back-btn">
            <a class="menu-icon-wrap" href="{{ URL('register/'.$user) }}">
              <span></span>
            </a>
          </div>
          <h1 class="text-center">{{ __('labels.terms_and_conditions') }}</h1>        
          <div class="tnc-content">
            {!! html_entity_decode($content) !!}
          </div>
        </div>
         <img src="{{ asset('public/template/images/page_up_icon.png') }}" class="back-to-top-wrap">
      </div>
    </div>
  </div>
  <div class="fl-fl float-wa">
    <i class="fab fa-whatsapp"></i>
    <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
  </div>
</section>
@endsection
@section('script') 
  <script src="{{ asset('public/template/js/jquery-3.3.1.js') }}"></script> 
  <script src="{{ asset('public/template/js/scripts.js') }}"></script>
@endsection