<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		@include('includes.google_analytics.head')
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		<meta name="generator" content="Jekyll v3.8.6">
		<title>{{ config('global.APP_COMPANY') }}</title>
		<link rel="stylesheet" href="{{ asset('public/template/css/jquery.fancybox.css') }}" />

		<!-- Favicons -->
		<link rel="apple-touch-icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="180x180">
		<link rel="shortcut icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="32x32" type="image/png">
		<link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}" sizes="16x16" type="image/png">
		<link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}">
		
		<!-- Bootstrap core CSS -->
		<link href="{{ asset('public/template/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="{{ asset('public/template/css/product.css') }}" rel="stylesheet">
		<link href="{{ asset('public/template/css/dashboard.css') }}" rel="stylesheet">
		<link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
		<link href="{{ asset('public/template/css/menu-style.css') }}" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/style.css') }}">

		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '276009957089736');
			fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=276009957089736&ev=PageView&noscript=1"/></noscript>
		<!-- End Facebook Pixel Code -->  
	</head>
	<body>
	@include('includes.google_analytics.body')
	@include('includes.header')
		<div class="banner-form-wrap comman-background" style="background-image:url({{ asset('public/template/images/ke_insumos_bg.png') }});">
			<div class="container">
				<div class="banner-wrap">
				<div class="mobile-content home-mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
					<div class="mobile-banner-logo">
						<img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
					</div>
					<h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
				</div>
					<div class="banner-content">						
							<h1>¡El lugar más fácil<br> para comprar y vender<br> agroinsumos!</h1>
						<div class="banner-btn-grp">
			  <a href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank">
								<div class="icon-wrap">
									<img  src="{{ asset('public/template/images/ke_insumos_iOS2.png') }}" alt="Apply">
								</div>
								<div class="icon-txt">
									<span class="sm-txt">Disponible en</span>
									<span class="big-txt">App Store</span>
								</div>
							</a>
							<a href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank">
								<div class="icon-wrap">
									<img  src="{{ asset('public/template/images/ke_insumos_android-2.png') }}"  alt="Google Play">
								</div>
								<div class="icon-txt">
									<span class="sm-txt">Disponible en</span>
									<span class="big-txt">Google play</span>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="banner-video-wrap row">
					<div class="video-part col-xl-4 col-lg-4 col-sm-6 col-6">
						<h3>¿Eres productor? ¡Echa un vistazo a este video! </h3>
						<a data-fancybox class="full-click-wrap" href="https://www.youtube.com/embed/_c06hsTx64g">
							<img src="{{ asset('public/template/images/eres_productor.png') }}">
							<i class="fa fa-play-circle" aria-hidden="true"></i>
						</a>
					</div>
					<div class="video-part col-xl-4 col-lg-4 col-sm-6 col-6">
						<h3>¿Eres agronomía? ¡Echa un vistazo a este video! </h3>
						<a data-fancybox class="full-click-wrap" href="https://www.youtube.com/embed/5Koy2TUs3l0">
							<img src="{{ asset('public/template/images/placeholder-2.jpg') }}">
							<i class="fa fa-play-circle" aria-hidden="true"></i>
						</a>
					</div>
					<div class="video-part col-xl-4 col-lg-4 col-sm-6">
						<h3>Conoce más sobre Ke Insumos</h3>
						<a data-fancybox class="full-click-wrap" href="https://www.youtube.com/embed/ymonZH327iQ">
							<img src="{{ asset('public/template/images/conoce_todo_ms_sobre_Keinsumos.png') }}">
							<i class="fa fa-play-circle" aria-hidden="true"></i>
						</a>
					</div>
				</div>
				<h4 class="text-center text-success d-block d-sm-none">
				  ¡Tené a Ke Insumos en tu bolsillo, descarga la App!
				</h4>
		<div class="banner-btn-grp d-block d-sm-none d-flex justify-content-between">
		  <a href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank">
			<div class="icon-wrap">
			  <img  src="{{ asset('public/template/images/ke_insumos_iOS2.png') }}" alt="Apply">
			</div>
			<div class="icon-txt">
			  <span class="sm-txt">Disponible en</span>
			  <span class="big-txt">App Store</span>
			</div>
		  </a>
		  <a href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank">
			<div class="icon-wrap">
			  <img  src="{{ asset('public/template/images/ke_insumos_android-2.png') }}"  alt="Google Play">
			</div>
			<div class="icon-txt">
			  <span class="sm-txt">Disponible en</span>
			  <span class="big-txt">Google play</span>
			</div>
		  </a>
		</div>
			</div>
		</div>
	@include('includes.footer')
		<script src="{{ asset('public/template/js/jquery-3.1.0.js') }}"></script>
		<script src="{{ asset('public/template/js/popper.min.js') }}"></script>
		<script src="{{ asset('public/template/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('public/template/js/jquery.fancybox.js') }}"></script>
		<script src="{{ asset('public/template/js/jquery.fancybox-media.js') }}"></script>
		<script src="{{ asset('public/template/js/scripts.js') }}"></script>
		<script src="{{ asset('public/template/js/menu/modernizr.js') }}"></script>	
		<script src="{{ asset('public/template/js/menu/jquery.mobile.custom.min.js') }}"></script>
		<script src="{{ asset('public/template/js/menu/main.js') }}"></script>
	</body>
</html>