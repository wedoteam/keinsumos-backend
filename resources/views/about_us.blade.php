@extends('layouts.advanced')
  @section('title')
  <title>{{ __('labels.page_title.about_us') }} | {{ config('app.name') }}</title>
  @endsection
  @section('style')
  <style type="text/css">
    body { overflow-x: hidden; }
  </style>
  @endsection
  @section('content')
  <section >
    <div class="banner_part-wrap desktop_quienes_somos_banner" style="background-image:url({{ asset('public/template/images/desktop_quienes_somos_01.png') }});">  
      <div class="banner_mobile_views"> 
        <div class="mobile-content home-mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
          <div class="mobile-banner-logo">
            <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
          </div>
          <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
        </div>
      </div>
      <div class="banner-left-part propuesta_title animatedParent" style="overflow: hidden;">
        <h1 class="banner_before animated bounceInDown go">Quiénes somos</h1>     
      </div>
      <div class="banner-right-part animated bounceInDown go">
        <div class="banner_content">        
          <p>Somos la tercera generación de una <strong>familia de productores</strong> y heredamos <strong> la pasión por el campo.</strong></p>        
        </div>
      </div>
    </div>
  </section>
  <section class="desktop_quienes_somos" >
    <div class="mt_p" > 
      <div class="container">
        <div class="row align-items-center animatedParent" style="overflow: hidden;">
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_content_display animated fadeIn">
            <div class="ofrecemos_content"> 
              <h5>Usamos <strong>tecnología </strong> para simplificar procesos de la <strong>cadena de valor </strong>agropecuaria. </h5>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_video_thumbnail animated fadeIn"> 
            <a data-fancybox class="full-click-wrap" href="https://youtu.be/ymonZH327iQ">
              <img src="{{ asset('public/template/images/conoce_todo_ms_sobre_Keinsumos.png') }}">
              <i class="fa fa-play-circle" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="bagr_gro">
    <div class="row desktop_quienes_somos_second animatedParent" style="overflow: hidden;">
      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 p-0 video-img animated fadeIn">
        <div class="custom_pp" >
          <img src="{{ asset('public/template/images/desktop_quienes_somos_02.jpg') }}">
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 p-0 mobile_p1 animated video-img animated fadeIn">
        <div class="desktop_quienes_somos_content"> 
          <h5>En nuestro día a día, <strong> identificamos los obstáculos </strong>  que se presentan a la hora de gestionar el negocio y  <strong> desarrollamos una solución </strong> que crea valor para los principales actores de la cadena.</h5>
        </div>
      </div>
    </div>
  </section>
  <section class="desktop_quienes_somos_thard bagr_gro animatedParent" >
    <div class="mt_p" > 
      <div class="container">
        <div class="row align-items-center d-flexingss">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ofrecemos_content_display animated fadeIn">
            <div class="ofrecemos_content"> 
              <h5><strong> Ke Insumos </strong> es nuestro aporte innovador al campo.</h5>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ofrecemos_video_thumbnail animated fadeIn"> 
            <img src="{{ asset('public/template/images/desktop_quienes_somos_divice.png') }}">
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="desktop_quienes_somos_fith animatedParent" >
    <div class="mt_p" > 
      <div class="row align-items-center d-flexingss d-flexingss-mobil" style="overflow: hidden;">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 somos_fith animated bounceInUp">
          <div class="ofrecemos_content"> 
            <h5><strong>Visión </strong> </h5>
              <p>Ser la plataforma y canal de preferencia de los productores agropecuarios y las agronomías para resolver sus necesidades de planificación y compra-venta de agro-insumos, ofreciendo una experiencia superlativa, buscando también generar un impacto positivo en el medio ambiente.</p>
          </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_video_thumbnail animated bounceInUp"> 
          <div class="spacing"></div>
        </div>
      </div> 
    </div>
  </section>
  <section class="desktop_quienes_somos_last1 animatedParent" style="overflow: hidden;" >
    <div class=""> 
      <div class="row align-items-center d-flexingss d-flexingss-mobil">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_video_thumbnail animated fadeIn"> 
          <div class="spacing"></div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 somos_fith animated fadeIn">
          <div class="ofrecemos_content"> 
            <h5><strong>Misión </strong> </h5>
            <p>Creamos valor prestando servicios a productores y agronomías a través de una plataforma digital que los conecta, para resolver los procesos de compra-venta de agro-insumos de una manera ágil, sencilla y eficiente, en un ambiente de confianza.</p>
          </div>
        </div>
      </div> 
    </div>
  </section>
  @endsection