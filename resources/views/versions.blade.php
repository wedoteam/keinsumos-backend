<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('includes.google_analytics.head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="180x180">
    <link rel="shortcut icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="32x32" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}" sizes="16x16" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}">
    <title>{{ __('labels.page_title.versions') }} | {{ config('app.name') }}</title>
    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap" rel="stylesheet">
    <link href="{{ asset('public/template/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/product.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/style.css') }}">
  </head>
  <body class="release-page">
    @include('includes.google_analytics.body')
    <header>
      <div class="site-header">
        <div class="container-fluid d-flex flex-column flex-md-row align-items-center p-2 px-md-4 mb-3 header-wrap">
          <a class="d-none d-md-block navbar-brand header-logo" href="{{ URL('') }}" >
            <img src="{{ asset('public/template/images/home_logo.png') }}">
          </a>
          <a class="d-md-none my-0 m-auto navbar-brand mr-0 header-logo" href="{{ URL('') }}" >
            <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}" height="100">
          </a>
        </div>
      </div>
    </header>
    <div class="content container-fluid d-flex h-100 flex-column">
      <div class="row">
        <div class="col-12 col-lg-9 col-xl-9 m-auto pb-3 pt-2">
          <a href="{{ URL('') }}" class="go-to-home">
            {{ __('labels.frontend.go_to_home') }}
          </a>
        </div>
      </div>
      <div class="row d-flex h-100 flex-column">
        <div class="col-12 col-lg-9 col-xl-9 flex-grow-1 mr-auto ml-auto">
          <div class="card release-notes-card d-flex h-100 flex-column">
            <div class="card-body pt-5 pr-5 pl-5">
              <div class="row">
                <div class="col-12">
                  <h1 class="text-success">{{ __('labels.frontend.versions') }}</h1>
                </div>
              </div>
              <div class="row d-flex">
                <div class="col-12 col-md-4 order-md-2 mb-5">
                  <h3>{{ __('labels.frontend.versions') }}</h3>
                  <div id="accordion">
                    <div class="card">
                      <div class="card-header" id="headingWeb" data-toggle="collapse" data-target="#collapseWeb" aria-expanded="true" aria-controls="collapseWeb">
                        <h5 class="mb-0">Web</h5>
                      </div>
                      <div id="collapseWeb" class="collapse show" aria-labelledby="headingWeb" data-parent="#accordion">
                        <div class="card-body">
                          <ul class="nav nav-tabs flex-column" id="webTab" role="tablist">
                            @php $active = true; @endphp
                            @foreach($web as $version)
                            <li class="nav-item">
                              <a class="nav-link {{ ($active) ? 'active' : '' }}"
                                 id="version-{{ $version->id }}-tab"
                                 data-toggle="tab"
                                 href="#version-{{ $version->id }}"
                                 role="tab"
                                 aria-controls="version-{{ $version->id }}"
                                 aria-selected=" {{ ($active) ? 'true' : 'false' }}">
                                {{ $version->name }}
                              </a>
                            </li>
                            @php $active = false; @endphp
                            @endforeach
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingAndroid" data-toggle="collapse" data-target="#collapseAndroid" aria-expanded="false" aria-controls="collapseAndroid">
                        <h5 class="mb-0">App Android</h5>
                      </div>
                      <div id="collapseAndroid" class="collapse" aria-labelledby="headingAndroid" data-parent="#accordion">
                        <div class="card-body">
                          <ul class="nav nav-tabs flex-column" id="androidTab" role="tablist">
                            @foreach($android as $version)
                            <li class="nav-item">
                              <a class="nav-link"
                                 id="version-{{ $version->id }}-tab"
                                 data-toggle="tab"
                                 href="#version-{{ $version->id }}"
                                 role="tab"
                                 aria-controls="version-{{ $version->id }}"
                                 aria-selected="false">
                                {{ $version->name }}
                              </a>
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingIos" data-toggle="collapse" data-target="#collapseIos" aria-expanded="false" aria-controls="collapseIos">
                        <h5 class="mb-0">App iOS</h5>
                      </div>
                      <div id="collapseIos" class="collapse" aria-labelledby="headingIos" data-parent="#accordion">
                        <div class="card-body">
                          <ul class="nav nav-tabs flex-column" id="iosTab" role="tablist">
                            @foreach($ios as $version)
                            <li class="nav-item">
                              <a class="nav-link"
                                 id="version-{{ $version->id }}-tab"
                                 data-toggle="tab"
                                 href="#version-{{ $version->id }}"
                                 role="tab"
                                 aria-controls="version-{{ $version->id }}"
                                 aria-selected="false">
                                {{ $version->name }}
                              </a>
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-8 order-md-1">
                  <div class="tab-content" id="versionTabContent">

                    @php $active = true; @endphp
                    @foreach($web as $version)
                    <div class="tab-pane fade {{ ($active) ? 'show active' : '' }}"
                         id="version-{{ $version->id }}"
                         role="tabpanel"
                         aria-labelledby="version-{{ $version->id }}-tab">
                      <h2 class="text-success">Web</h2>
                      <h3 class="text-success">{{ $version->name }}</h3>
                      <p>{!! nl2br($version->content) !!}</p>  
                    </div>
                    @php $active = false; @endphp
                    @endforeach 
                    {{-- end of web --}}

                    @foreach($android as $version)
                    <div class="tab-pane fade"
                         id="version-{{ $version->id }}"
                         role="tabpanel"
                         aria-labelledby="version-{{ $version->id }}-tab">
                      <h2 class="text-success">App Android</h2>
                      <h3 class="text-success">{{ $version->name }}</h3>
                      <p>{!! nl2br($version->content) !!}</p>    
                    </div>
                    @php $active = false; @endphp
                    @endforeach
                    {{-- end of android --}}

                    @foreach($ios as $version)
                    <div class="tab-pane fade"
                         id="version-{{ $version->id }}"
                         role="tabpanel"
                         aria-labelledby="version-{{ $version->id }}-tab">
                      <h2 class="text-success">App iOS</h2>
                      <h3 class="text-success">{{ $version->name }}</h3>
                      <p>{!! nl2br($version->content) !!}</p>
                    </div>
                    @php $active = false; @endphp
                    @endforeach
                    {{-- end of ios --}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer>  
      <div class="container"> 
        <div class="footer-wrap row"> 
          <div class="footer-part"> 
            <p>© {{ date('Y') }} Ke Insumos. <i>Todos los derechos reservados.</i></p> 
          </div>  
          <div class="footer-part"> 
            <div class="footer-logo-content">
              <a href="http://piamweb.com" target="_blank">
                <img src="{{ asset('public/template/images/footer_logo.png') }}">
              </a>
            </div> 
          </div>  
        </div> 
      </div>  
    </footer>
    <script src="{{ asset('public/template/js/jquery-3.3.1.js') }}"></script> 
    <script src="{{ asset('public/template/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
      $('#accordion a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#accordion a.nav-link.active').not(this).attr('aria-selected', 'false');
        $('#accordion a.nav-link.active').not(this).removeClass('active');
      });
    </script>
  </body>
</html>
