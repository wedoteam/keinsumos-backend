<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('includes.google_analytics.head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('labels.page_title.login') }} | {{ config('app.name') }}</title>
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="180x180">
    <link rel="shortcut icon" href="{{ asset('public/favicon_32x32.ico') }}" sizes="32x32" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}" sizes="16x16" type="image/png">
    <link rel="shortcut icon" href="{{ asset('public/favicon_16x16.ico') }}">

    <link href="{{ asset('public/template/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap" rel="stylesheet">
    <link href="{{ asset('public/template/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/dashboard.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/product.css') }}" rel="stylesheet">
    <link href="{{ asset('public/template/css/menu-style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/style.css') }}">
    <style type="text/css">
      @media (max-width: 576px) {
        .cd-nav-trigger { display: none; }
      }
    </style>
  </head>
  <body>
    @include('includes.google_analytics.body')
    <header class="cd-main-header fixed-top bg-color-light-dark logo_mobile_views">
      <div class="container-fluid">
        <div class="container__header">
          <div class="button__posi">
            <a class="cd-logo" href="{{ URL('') }}">
              <img src="{{ asset('public/template/images/home_logo.png') }}">
            </a>
            <div class="button__posi__fix" >
              <ul class="cd-header-buttons">
                <li><a class="cd-nav-trigger " href="#cd-primary-nav"><span></span></a></li>
              </ul>
            </div>
            <!-- cd-header-buttons -->
          </div>
        </div>
      </div>
    </header>
    <main class="cd-main-content">   
      <!-- your content here --> 
      <div class="cd-overlay"></div>   
    </main>
    <nav class="cd-nav navbar navbar-expand-lg navbar-dark">
    <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
      <li><a href="{{ URL('/quiénes-somos') }}">{{ __('labels.frontend.about_us') }}</a></li>
      <li><a href="{{ URL('/nuestra-solución') }}">{{ __('labels.frontend.our_solution') }}</a></li>
      <li class="has-children before__child">
        <a href="#" class="nav_next_arrow">{{ __('labels.frontend.proposal_value') }}</a>
        <ul class="cd-secondary-nav is-hidden">
          <li class="go-back"><a href="#0" class="selected nav_back_arrow">0</a></li>
          <li><a href="{{ URL('/para-las-agronomías') }}" >{{ __('labels.frontend.for_agronomia') }}</a></li>
          <li><a href="{{ URL('/para-los-productores') }}">{{ __('labels.frontend.for_producer') }}</a></li>     
        </ul>
      </li>
      <li><a href="{{ URL('/contacto') }}">{{ __('labels.frontend.contact') }}</a></li>
      <li><a href="{{ URL('/ayuda') }}">{{ __('labels.frontend.faq') }}</a></li>    
      <div class="nav__btn custom-btn-grp custom-btn-grp1">
        @if(!empty($user))
          @if($user->user_type == config('global.BUSINESS_USER'))
          <a class="btn btn-outline-primary text-white outline2 hide-in-mobile-menu" href="{{ URL('agronomia') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
          @elseif($user->user_type == config('global.CUSTOMER_USER'))
          <a class="btn btn-outline-primary text-white outline2 hide-in-mobile-menu" href="{{ URL('producer') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
          @else
          <a class="btn btn-outline-primary text-white outline2 hide-in-mobile-menu" href="{{ URL('admin') }}"><b>{{ __('labels.frontend.dashboard') }}</b></a>
          @endif
          <a class="btn btn-outline-primary fill-bg" href="{{ URL('logout') }}"><b>{{ __('labels.frontend.logout') }}</b></a>
        @else
        <a class="btn btn-outline-primary text-white outline2" href="{{ URL('login') }}"><b>{{ __('labels.frontend.login') }}</b></a>
        <a class="btn btn-outline-primary fill-bg" href="{{ URL('register') }}"><b>{{ __('labels.frontend.register') }}</b></a>
        @endif
      </div>
    </ul>
    </nav>
    <div class="banner-form-wrap login-banner header_bg">
      <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">  
        <div class="mobile-menu back-btn">
          <a class="menu-icon-wrap" href="{{ URL('/') }}">
            <span></span>
          </a>
        </div>          
        <div class="mobile-banner-logo">
          <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">      
        </div>
        <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
      </div>
    </div>
    <section class="position-relative login-form pt-3 pt-lg-0 pb-5 mt-lg-n10 iniciar__sesion footer-bottom-container custom-login-pb">
      <div class="container">
        <div class="col-xl-12 col-lg-12">
          <div class="row ">
            <div class="col-xl-7 col-lg-7 m-auto">
              <div class="card shadow-sm">
                <div class="card-body">
                  <h1 class="card-title">{{ __('labels.frontend.login') }}</h1>
                  @if($errors->any())
                  <div class="alert alert-danger text-center">
                    @foreach ($errors->all() as $error)
                      {{ $error }}<br/>
                    @endforeach
                  </div>
                  @endif
                  <form class="text-center" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                      <input type="email" name="email" class="form-control" id="email" placeholder="{{ __('labels.frontend.email') }}"  value="{{ old('email') }}" style="text-align:center" required autocomplete="email" autofocus>
                    </div>
                    <div class="form-group mb__custom">
                      <input type="password" name="password" class="form-control" id="password" placeholder="{{ __('labels.frontend.password') }}" style="text-align:center" required>
                    </div>
                    <div class="form-group form-check">
                      <label><a href="{{ route('password.request') }}">{{ __('labels.frontend.i_forgot_the_password') }}</a></label>
                    </div>
                    <a class="btn btn-outline-primary btn-lg" href="{{ URL('') }}">{{ __('labels.frontend.cancel') }}</a>
                    <button type="submit" class="btn btn-primary btn-lg">{{ __('labels.frontend.login') }}</button>
                    <div class="form-group form-check registration-link">
                      <label>{{ __('labels.frontend.not_a_user') }}</label>
                      <a href="{{ URL('register') }}">{{ __('labels.frontend.register') }}</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="custom-footer">  
      <div class="container"> 
        <div class="footer-wrap row"> 
          <div class="footer-part"> 
            <p>© {{ date('Y') }} Ke Insumos. <i>Todos los derechos reservados.</i></p> 
          </div>  
          <div class="footer-part"> 
            <div class="footer-logo-content">
              <a href="http://piamweb.com" target="_blank">
                <img src="{{ asset('public/template/images/footer_logo.png') }}">
              </a>
            </div> 
          </div>  
        </div> 
        <div class="mobile-footer">
          <div class="container">
            <ul>
						@if(!empty($user))
							@if($user->user_type == config('global.BUSINESS_USER'))
							<li><a href="{{ URL('agronomia') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
							@elseif($user->user_type == config('global.CUSTOMER_USER'))
							<li><a href="{{ URL('producer') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
							@else
							<li><a href="{{ URL('admin') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}"></a></li>
							@endif
						@else
        			<li class="active"><a href="{{ URL('login') }}"><img src="{{ asset('public/template/images/ic_iniciar_sesion_deactive.png') }}">  <span class="text-uppercase">{{ __('labels.frontend.login') }}</span></a></li>
        			<li><a href="{{ URL('register') }}"><img src="{{ asset('public/template/images/ic_registrarse_deactive.png') }}"></a></li>
						@endif
							<li><a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"><img src="{{ asset('public/template/images/ic_whatsapp_deactive.png') }}"></a></li>
        			<li><a href="{{ URL('/ayuda') }}"><img src="{{ asset('public/template/images/ic_faqs_deactive.png') }}"></a></li>
        		</ul>
          </div>
        </div> 
      </div>  
      <div class="fl-fl float-wa">
          <i class="fab fa-whatsapp"></i>
          <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
        </div>
    </footer>
    <script src="{{ asset('public/template/js/jquery-3.1.0.js') }}"></script>
    <script src="{{ asset('public/template/js/popper.min.js') }}"></script>
    <script src="{{ asset('public/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/template/js/scripts.js') }}"></script>
    <script src="{{ asset('public/template/js/menu/modernizr.js') }}"></script> 
    <script src="{{ asset('public/template/js/menu/jquery.mobile.custom.min.js') }}"></script>
    <script src="{{ asset('public/template/js/menu/main.js') }}"></script>
  </body>
</html>
