@extends('layouts.app')
  @section('title')
  <title>{{ __('labels.page_title.register') }} | {{ config('app.name') }}</title>
  @endsection 
  @section('style')
  <style type="text/css">
    @media (max-width: 576px) {
      .cd-nav-trigger { display: none; }
    }
  </style>
  @endsection
  @section('content')
  <div class="banner-form-wrap login-banner register-banner header_bg">
    <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">  
      <div class="mobile-menu back-btn">
        <a class="menu-icon-wrap" href="{{ URL('/') }}"><span></span></a>
      </div>          
      <div class="mobile-banner-logo">
        <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">              
      </div>
      <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
    </div>
  </div>
  <section class="position-relative register-cards pt-3 pt-lg-0 pb-5 cutom__card__rig footer-bottom-container">
    <div class="container">
      <div class="col-xl-12 col-lg-12">
        <div class="row justify-content-center registration-card-wrap">
          <div class="col-xl-3 col-lg-3 col-md-4 col-6 mb-5 registration-card-custom">
            <a href="{{ URL('register/producer') }}" class="">
              <div class="card shadow-sm">
                <div class="card-body">
                  <img src="{{ asset('public/template/images/ke_insumos_producer.png') }}">
                  <h1 class="card-title">¿{{ __('labels.frontend.producer') }}?</h1>
                  <h2>{{ __('labels.frontend.sign_up_to_start_shopping') }}</h2>                    
                </div>
              </div>
            </a>
          </div>
          <div class="col-xl-3 col-lg-3 col-6 col-md-4 registration-card-custom">
            <a href="{{ URL('register/agronomia') }}" class="">
              <div class="card shadow-sm">
                <div class="card-body">
                  <img src="{{ asset('public/template/images/ke_insumos_agronomia.png') }}">
                  <h1 class="card-title">¿{{ __('labels.frontend.agronomia') }}?</h1>
                  <h2>{{ __('labels.frontend.sign_up_to_start_selling') }}</h2>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="fl-fl float-wa">
      <i class="fab fa-whatsapp"></i>
      <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
    </div>
  </section>
  @endsection
  @section('script') 
  @endsection

