@extends('layouts.app')
  @section('title')
    <title>@lang('labels.page_title.registration_success') | {{ config('app.name') }}</title>
  @endsection
  @section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('public/template/css/validation_style.css') }}">
    <style>
      .cd-nav-trigger { display: none; }
    </style>
  @endsection
  @section('content')
    <div class="banner-form-wrap login-banner header_bg">
      <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
        <div class="mobile-banner-logo">
          <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
        </div>
      </div>
    </div>
    <div class="success-section">
      <div class="container footer-bottom-container">
        <div class="col-xl-6 col-lg-6 m-auto">
          <div class="wizard__header-content register__success">
            <img src="{{ asset('public/template/images/ke_insumos_'.$user.'.png') }}">
            <div class="success-content-wrap">
              <p class="mt-3 mb-3"><b>@lang('labels.frontend.thanks_for_signing_up')</b></p>
              <p>@lang('labels.frontend.we_are_verifying_your_data')<br>@lang('labels.frontend.your_account_will_be_registered_within_24_hours')</p>
              <a href="{{ URL('/') }}" class="btn btn-success custom-success-btn">{{ __('labels.frontend.pages.register_success.go_to_home') }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endsection