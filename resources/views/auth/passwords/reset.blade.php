@extends('layouts.app')
@section('style')
<style type="text/css">
  @media (max-width: 576px) {
    .cd-nav-trigger { display: none; }
  }
</style>
@endsection
@section('content')
<div class="banner-form-wrap login-banner header_bg">
  <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">  
    <div class="mobile-menu back-btn">
      <a class="menu-icon-wrap" href="{{ URL('/login') }}">
        <span></span>
      </a>
      </div>          
      <div class="mobile-banner-logo">
        <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">              
      </div>           
    </div>
  </div>
  <section class="reset_password_wrap position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10 iniciar__sesion footer-bottom-container">
    <div class="container">
      <div class="col-xl-12 col-lg-12">
        <div class="row ">
          <div class="col-xl-7 col-lg-7 m-auto">
            <div class="card shadow-sm">
              <div class="card-body">
                <h1 class="card-title">{{ __('labels.frontend.reset_password') }}</h1>
                @if($errors->any())
                <div class="alert alert-danger text-center">
                  @foreach ($errors->all() as $error)
                    {{ $error }}<br/>
                  @endforeach
                </div>
                @endif
                <form class="text-center" method="POST" action="{{ route('password.update') }}">
                  @csrf
                  <input type="hidden" name="token" value="{{ $token }}">
                  <div class="form-group">
                    <input type="email" name="email" class="form-control" id="email" placeholder="{{ __('labels.frontend.email') }}"  value="{{ old('email') }}" style="text-align:center" required autocomplete="email" autofocus>
                  </div>
                  <div class="form-group">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" style="text-align:center" name="password" required placeholder="{{ __('labels.frontend.new_password') }}" />
                  </div>

                  <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('labels.frontend.confim_new_password') }}" style="text-align:center">
                    @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <div class="mt-5">
                    <button type="submit" class="btn btn-primary btn-lg reset-btn">
                      {{ __('labels.frontend.reset_password') }}
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="fl-fl float-wa">
      <i class="fab fa-whatsapp"></i>
      <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
    </div>
  </section>
@endsection