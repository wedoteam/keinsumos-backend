@extends('layouts.app')
@section('title')
<title>{{ __('labels.page_title.forgot_the_password') }} | {{ config('app.name') }}</title>
@endsection
@section('style')
<style type="text/css">
  @media (max-width: 576px) {
    .cd-nav-trigger { display: none; }
  }
</style>
@endsection

@section('content')
  <div class="banner-form-wrap login-banner header_bg">
    <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">  
      <div class="mobile-menu back-btn">
          <a class="menu-icon-wrap" href="{{ URL('/login') }}">
          <span></span>
        </a>
        </div>          
        <div class="mobile-banner-logo">
          <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">              
        </div>           
      </div>
  </div>
  <section class="reset_password_wrap position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10 iniciar__sesion footer-bottom-container">
    <div class="container">
      <div class="col-xl-12 col-lg-12">
        <div class="row ">
          <div class="col-xl-7 col-lg-7 m-auto">
            <div class="card shadow-sm">
              <div class="card-body">
                <h1 class="card-title">{{ __('labels.frontend.i_forgot_the_password') }}</h1>
                @if($errors->any())
                <div class="alert alert-danger text-center">
                  @foreach ($errors->all() as $error)
                    {{ $error }}<br/>
                  @endforeach
                </div>
                @endif
                @if (session('status'))
                <div class="alert alert-success text-center">
                  {{ session('status') }}
                </div>
                @endif
                <form class="text-center" method="POST" action="{{ route('password.email') }}">
                  @csrf
                  <div class="form-group mb-5">
                    <input type="email" name="email" class="form-control" id="email" placeholder="{{ __('labels.frontend.email') }}"  value="{{ old('email') }}" style="text-align:center" required autocomplete="email" autofocus>
                  </div>
                  <button type="submit" class="btn btn-primary btn-lg reset-btn">{{ __('labels.frontend.send_reset_link') }}</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     <div class="fl-fl float-wa">
          <i class="fab fa-whatsapp"></i>
          <a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
        </div>
  </section>
@endsection