@extends('layouts.app')
  @section('title')
    <title>@lang('labels.page_title.agronomia_registration') | {{ config('app.name') }}</title>
  @endsection
  @section('style')
    <link href="{{ asset('public/template/css/select2.min.css') }}" rel="stylesheet" />
    <style type="text/css">
      @media (max-width: 576px) {
        .cd-nav-trigger { display: none; }
      }
      #password-icon-container button { height: 38px; width: 40px; }
    </style>
  @endsection
  @section('content')
  <div class="banner-form-wrap login-banner register-form header_bg">
    <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">  
      <div class="mobile-menu back-btn">
        <a class="menu-icon-wrap" href="{{ URL('/register') }}"><span></span></a>
      </div>          
      <div class="mobile-banner-logo">
        <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">              
      </div>
      <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
    </div>
  </div>
  <section class="position-relative register-form-card pt-3 pt-lg-0 pb-5 mt-lg-n10 iniciar__sesion registration-wrap footer-bottom-container register__section">
    <div class="container">
      <div class="col-xl-12 col-lg-12">
        <div class="row ">
          <div class="col-xl-6 col-lg-6 m-auto registration-card">
            <div class="card shadow-sm">
              <div class="card-body text-center">
                <img src="{{ asset('public/template/images/ke_insumos_agronomia.png') }}" class="registeration_icon">
                <h1 class="card-title mb-0">@lang('labels.frontend.create_agronomia_account')</h1>
                <form method="POST" action="{{ URL('web_api/agronomia/register') }}" id="register_form">
                  @csrf
                  <h1 class="text-success">@lang('labels.frontend.company_data')</h1>
                  <div class="form-group">
                    <input type="text" name="business_name" class="form-control" id="business_name" placeholder="@lang('labels.frontend.business_name')" autocomplete="off" autofocus />
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" name="cuit" class="form-control" id="cuit" placeholder="@lang('labels.frontend.cuit')" autocomplete="off" />
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="far fa-check-circle text-success" id="cuit_success" style="display: none;"></i><i class="far fa-times-circle text-danger" id="cuit_error" style="display: none;"></i></span>
                      </div>
                    </div>
                    <label id="cuit-error" class="error" for="cuit" style="display: none;"></label>
                  </div>
                  <div class="form-group">
                    <select id="province" name="province" class="form-control" style="width: 100%;"></select>
                  </div>
                  <div class="form-group mb-4">
                    <select id="locality" name="locality" class="form-control" style="width: 100%;"></select>
                  </div>  
                  <h1 class="text-success mb-4">@lang('labels.frontend.contact_information')</h1>
                  <div class="form-group">
                    <input type="text" name="firstname" class="form-control" id="firstname" placeholder="@lang('labels.frontend.firstname')" style="text-align:center" autocomplete="off" autofocus />
                  </div>
                  <div class="form-group">
                    <input type="text" name="surname" class="form-control" id="surname" placeholder="@lang('labels.frontend.surname')" style="text-align:center" autocomplete="off" />
                  </div>
                  <div class="form-group">
                    <input type="text" name="email" class="form-control" id="email" placeholder="@lang('labels.frontend.email')" style="text-align:center" autocomplete="off" />
                  </div>
                  <div class="form-group">
                    <label><b>@lang('labels.frontend.mobile')</b></label>
                    <div class="row mb-3">
                      <div class="col-4 col-md-3">
                        <input type="text" id="mobile_prefix" name="mobile_prefix" placeholder="@lang('labels.frontend.prefix')" class="form-control" autocomplete="off">
                      </div>
                      <div class="col-8 col-md-9">
                        <input type="text" id="mobile" name="mobile" placeholder="@lang('labels.frontend.number_without_15')" class="form-control" autocomplete="off">
                      </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <input type="password" name="password" class="form-control" id="password" placeholder="@lang('labels.frontend.password')" style="text-align:center" autocomplete="off">
                      <div class="input-group-append" id="password-icon-container">
                        <button type="button" class="input-group-text"><img src="{{ asset('public/template/images/view_icon.png') }}" id="show_password"><img src="{{ asset('public/template/images/view_disable_icon.png') }}" id="hide_password" style="display: none;"></button>
                      </div>
                    </div>
                  </div>
                  <div class="form-group" >
                    <div class="custom-checkbox text-left custom-checkbox-select">
                      <input type="checkbox" name="terms_and_conditions" class="custom-control-input" id="terms_and_conditions" value="1">
                      <label class="custom-control-label" for="terms_and_conditions">@lang('labels.frontend.i_have_read_and_accept_the') <a href="{{ URL('terms-and-conditions/agronomia') }}" id="terms_and_conditions_link">@lang('labels.frontend.terms_and_conditions')</a> @lang('labels.frontend.of_company')
                      </label>
                    </div>
                  </div>
                  <div style="width: 100%; display: none;">
                    <p class="pull-left text-danger" style="font-size: 13px;" id="terms_and_conditions_error"></p>
                    <br><br>
                  </div>
                  <div style="width: 100%">
                    <a class="btn btn-outline-primary btn-lg" href="{{ URL('') }}">@lang('labels.frontend.cancel') </a>
                    <button type="submit" name="create" value="create" class="btn btn-primary btn-lg">@lang('labels.frontend.create')</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  @endsection
  @section('script')
  <script src="{{ asset('public/template/js/jquery.validate.min.js') }}"></script>  
  <script src="{{ asset('public/template/js/jquery.form.min.js') }}"></script>
  <script src="{{ asset('public/template/js/select2.min.js') }}"></script>
  <script src="{{ asset('public/template/js/jquery.mask.js') }}"></script>
  <script src="{{ asset('public/template/js/jquery.cookie.js') }}"></script>

  <script>

    // initialize province
    $('#province').select2({
      placeholder: "@lang('labels.frontend.province')",
      ajax: {
        type:'POST',
        url: "{{ URL('api/provinces') }}",
        dataType: "json",
        processResults: function (data, params) {
          return {
            results: $.map(data.data, function (item) {
              return {
                text: item.name,
                id: item.id,
                data: item
              };
            })
          };
        }
      }
    });

    // initialize locality
    $('#locality').select2({
      placeholder: "@lang('labels.frontend.locality')",
      ajax: {
        type:'POST',
        url: "{{ URL('api/localities') }}",
        dataType: "json",
        processResults: function (data, params) {
          return {
            results: $.map(data.data, function (item) {
              return {
                text: item.name,
                id: item.id,
                data: item
              };
            })
          };
        }
      }
    });

    // only if redirected from terms & conditions page
    @if(URL::previous() == URL('terms-and-conditions/agronomia'))

      // check if fields set 
      var business_name = $.cookie("agronomia_business_name");
      if(typeof business_name !== "undefined" && business_name != '')
      {
        $('#business_name').val(business_name);
        $.removeCookie('agronomia_business_name');
      }

      var cuit = $.cookie("agronomia_cuit");
      if(typeof cuit !== "undefined" && cuit != '')
      {
        $('#cuit').val(cuit);
        $.ajax({
          url: '{{ URL('api/validate_cuit') }}',
          type: 'POST',
          data: {
            cuit: function() {
              return $( "#cuit" ).val();
            }
          },
          async: false,
          success: function (response) {
            if(response.data.is_valid)
            {
              $('#cuit_error').hide();
              $('#cuit_success').show();
            }
            else
            {
              $('#cuit_error').show();
              $('#cuit_success').hide();
            }
          }
        });
        $.removeCookie('agronomia_cuit');
      }

      var province_id   = $.cookie("agronomia_province_id");
      var province_text = $.cookie("agronomia_province_text");

      if(typeof province_id !== "undefined" && typeof province_text !== "undefined")
      {
        $("#province").select2("trigger", "select", {
          data: {"id": province_id, "text" : province_text}
        });

        initLocality();

        $.removeCookie('agronomia_province_id');
        $.removeCookie('agronomia_province_text');
      }

      var locality_id   = $.cookie("agronomia_locality_id");
      var locality_text = $.cookie("agronomia_locality_text");

      if(typeof locality_id !== "undefined" && typeof locality_text !== "undefined")
      {
        $("#locality").select2("trigger", "select", {
          data: {"id": locality_id, "text" : locality_text}
        });

        $.removeCookie('agronomia_locality_id');
        $.removeCookie('agronomia_locality_text');
      }

      var firstname = $.cookie("agronomia_firstname");
      if(typeof firstname !== "undefined" && firstname != '')
      {
        $('#firstname').val(firstname);
        $.removeCookie('agronomia_firstname');
      }

      var surname = $.cookie("agronomia_surname");
      if(typeof surname !== "undefined" && surname != '')
      {
        $('#surname').val(surname);
        $.removeCookie('agronomia_surname');
      }

      var email = $.cookie("agronomia_email");
      if(typeof email !== "undefined" && email != '')
      {
        $('#email').val(email);
        $.removeCookie('agronomia_email');
      }

      var mobile_prefix = $.cookie("agronomia_mobile_prefix");
      if(typeof mobile_prefix !== "undefined" && mobile_prefix != '')
      {
        $('#mobile_prefix').val(mobile_prefix);
        $.removeCookie('agronomia_mobile_prefix');
      }

      var mobile = $.cookie("agronomia_mobile");
      if(typeof mobile !== "undefined" && mobile != '')
      {
        $('#mobile').val(mobile);
        $.removeCookie('agronomia_mobile');
      }

      var password = $.cookie("agronomia_password");
      if(typeof password !== "undefined" && password != '')
      {
        $('#password').val(password);
        $.removeCookie('agronomia_password');
      }

      var password_visible = $.cookie("agronomia_password_visible");
      if(typeof password_visible !== "undefined" && password_visible == "1")
      {
        $('#show_password').hide();
        $('#hide_password').show();
        $('#password').attr('type', 'text');
        $.removeCookie('agronomia_password_visible');
      }

      var terms_and_conditions = $.cookie("agronomia_terms_and_conditions");
      if(typeof terms_and_conditions !== "undefined" && terms_and_conditions == "1")
      {
        $('#terms_and_conditions').prop('checked', true);
        $.removeCookie('agronomia_terms_and_conditions');
      }

    @endif

    // on click of terms and conditions
    $(document).on('click', '#terms_and_conditions_link', function(e) {

      $.cookie('agronomia_business_name', $('#business_name').val());
      $.cookie('agronomia_cuit', $('#cuit').val());
      if($('#province').val() != null)
      {
        var province = $('#province').select2('data');
        $.cookie('agronomia_province_id', province[0].id);
        $.cookie('agronomia_province_text', province[0].text);
      }
      if($('#locality').val() != null)
      {
        var locality = $('#locality').select2('data');
        $.cookie('agronomia_locality_id', locality[0].id);
        $.cookie('agronomia_locality_text', locality[0].text);
      }
      $.cookie('agronomia_firstname', $('#firstname').val());
      $.cookie('agronomia_surname', $('#surname').val());
      $.cookie('agronomia_email', $('#email').val());
      $.cookie('agronomia_mobile_prefix', $('#mobile_prefix').val());
      $.cookie('agronomia_mobile', $('#mobile').val());
      $.cookie('agronomia_password', $('#password').val());

      if($('#password').attr('type') == 'text')
      {
        $.cookie('agronomia_password_visible', "1");
      }

      if($('#terms_and_conditions').prop('checked'))
      {
        $.cookie('agronomia_terms_and_conditions', "1");
      }

      // finally follow the link
      location.href = $(this).attr("href");

      // Or may be giving `return true;` could work?
      return false;
    });

    // on change event of province
    $(document).on('change', '#province', function(){
      initLocality();
    });

    function initLocality()
    {
      $("#locality").select2("destroy").select2();
      $('#locality').val('');
      $('#locality').select2({
        placeholder: "@lang('labels.frontend.locality')",
        ajax: {
          type:'POST',
          url: "{{ URL('api/localities') }}",
          dataType: "json",
          data: function (params) {
            return {
              term: params.term, 
              province_id: $('#province').val(), 
            };
          },
          processResults: function (data, params) {
            return {
              results: $.map(data.data, function (item) {
                return {
                  text: item.name,
                  id: item.id,
                  data: item
                };
              })
            };
          }
        }
      });
    }

    // show password
    $(document).on('click', '#show_password', function(){
      $('#show_password').hide();
      $('#hide_password').show();
      $('#password').attr('type', 'text');
    });

    // hide password
    $(document).on('click', '#hide_password', function(){
      $('#hide_password').hide();
      $('#show_password').show();
      $('#password').attr('type', 'password');
    });

    $("#mobile_prefix").mask('0000');
    $("#mobile").mask('00000000');

    $('#firstname').mask(alphaSpaceMask(), {
      translation: {
        'S': {
          pattern: /[a-zA-Z ]/,
        }
      }
    });

    $('#surname').mask(alphaSpaceMask(), {
      translation: {
        'S': {
          pattern: /[a-zA-Z ]/,
        }
      }
    });

    // alpha space mask
    function alphaSpaceMask(length = 128) {
      var mask = '';
      for (var i = 0; i < length; i++) {
        mask += "S";
      } 
      return mask;
    }

    // cuit validator
    jQuery.validator.addMethod("valid_cuit", function(value, element) {
      var is_valid = false;
      $.ajax({
        url: '{{ URL('api/validate_cuit') }}',
        type: 'POST',
        data: {
          cuit: function() {
            return $( "#cuit" ).val();
          }
        },
        async: false,
        success: function (response) {
          if(response.data.is_valid)
          {
            $('#cuit_error').hide();
            $('#cuit_success').show();
            is_valid = true;
          }
          else
          {
            $('#cuit_error').show();
            $('#cuit_success').hide();
            is_valid = false;
          }
        }
      });
      return is_valid;
    }, '');

    // validate form
    $("#register_form").validate({
      rules: {
        cuit : {
          valid_cuit: true,
        },
      },
    });

    // submit form
    $('#register_form').ajaxForm({ 
      beforeSubmit : function(arr, $form, options){
        $('#register_form input').removeClass('placeholder-danger');
        $('#business_name').attr('placeholder', "@lang('labels.frontend.business_name')");
        $('#cuit').attr('placeholder', "@lang('labels.frontend.cuit')");
        $('#firstname').attr('placeholder', "@lang('labels.frontend.firstname')");
        $('#surname').attr('placeholder', "@lang('labels.frontend.surname')");
        $('#email').attr('placeholder', "@lang('labels.frontend.email')");
        $('#mobile_prefix').attr('placeholder', "@lang('labels.frontend.prefix')");
        $('#mobile').attr('placeholder', "@lang('labels.frontend.number_without_15')");
        $('#password').attr('placeholder', "@lang('labels.frontend.password')");
        $('#terms_and_conditions_error').html("");
        $('#terms_and_conditions_error').closest('div').hide();
      },
      success : function(response){
        if(response.code == 200)
        {
          window.location = "{{ URL('register/agronomia/success') }}";
        }
        else
        {
          var focused = false;
          for (var key in response.errors) 
          {
            if(key == 'province')
            {
              $('#select2-province-container').find('.select2-selection__placeholder').addClass('text-danger');
              $('#select2-province-container').find('.select2-selection__placeholder').html(response.errors[key]);
            }
            if(key == 'locality')
            {
              $('#select2-locality-container').find('.select2-selection__placeholder').addClass('text-danger');
              $('#select2-locality-container').find('.select2-selection__placeholder').html(response.errors[key]);
            }
            if(key == 'terms_and_conditions')
            {
              $('#terms_and_conditions_error').html(response.errors[key]);
              $('#terms_and_conditions_error').closest('div').show();
            }
            else
            {
              $(`#${key}`).val('');
              $(`#${key}`).addClass('placeholder-danger');
              $(`#${key}`).attr('placeholder', response.errors[key]);

              if(!focused)
              {
                $(`#${key}`).focus();
                focused = true;
              }
            }
          }
        }
      }
    }); 
  </script>
  @endsection
