@extends('layouts.app')
  @section('title')
  <title>@lang('labels.page_title.producer_registration') | {{ config('app.name') }}</title>
  @endsection
  @section('style')
    <style type="text/css">
      @media (max-width: 576px) {
        .cd-nav-trigger { display: none; }
      }
      #password-icon-container button { height: 38px; width: 40px; }
    </style>
  @endsection
  @section('content')
  <div class="banner-form-wrap login-banner register-form header_bg">
    <div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">  
      <div class="mobile-menu back-btn">
        <a class="menu-icon-wrap" href="{{ URL('/register') }}"><span></span></a>
      </div>          
      <div class="mobile-banner-logo">
        <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">              
      </div>
      <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
    </div>
  </div>
  <section class="position-relative register-form-card pt-3 pt-lg-0 pb-5 mt-lg-n10 iniciar__sesion registration-wrap footer-bottom-container register__section">
    <div class="container">
      <div class="col-xl-12 col-lg-12">
        <div class="row ">
          <div class="col-xl-6 col-lg-6 m-auto registration-card">
            <div class="card shadow-sm">
              <div class="card-body text-center">
                <img src="{{ asset('public/template/images/ke_insumos_producer.png') }}" class="registeration_icon">
                <h1 class="card-title">@lang('labels.frontend.create_producer_account')<br><span class="text-success">@lang('labels.frontend.complete_with_the_establishment_details')</span>
                </h1>
                <form method="POST" action="{{ URL('web_api/producer/register') }}" id="register_form">
                  @csrf
                  <div class="form-group">
                    <input type="text" name="business_name" class="form-control" id="business_name" placeholder="@lang('labels.frontend.business_name')" autocomplete="off" autofocus />
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <input type="text" name="cuit" class="form-control" id="cuit" placeholder="@lang('labels.frontend.cuit')" autocomplete="off" />
                      <div class="input-group-append">
                        <span class="input-group-text"><i class="far fa-check-circle text-success" id="cuit_success" style="display: none;"></i><i class="far fa-times-circle text-danger" id="cuit_error" style="display: none;"></i></span>
                      </div>
                    </div>
                    <label id="cuit-error" class="error" for="cuit" style="display: none;"></label>
                  </div>
                  <div class="form-group">
                    <input type="text" name="email" class="form-control" id="email" placeholder="@lang('labels.frontend.email')" style="text-align:center" autocomplete="off" />
                  </div>
                  <div class="form-group">
                    <div class="input-group">
                      <input type="password" name="password" class="form-control" id="password" placeholder="@lang('labels.frontend.password')" style="text-align:center" autocomplete="off">
                      <div class="input-group-append" id="password-icon-container">
                        <button type="button" class="input-group-text"><img src="{{ asset('public/template/images/view_icon.png') }}" id="show_password"><img src="{{ asset('public/template/images/view_disable_icon.png') }}" id="hide_password" style="display: none;"></button>
                      </div>
                    </div>
                  </div>
                  <div class="form-group" >
                    <div class="custom-checkbox text-left custom-checkbox-select">
                      <input type="checkbox" name="terms_and_conditions" class="custom-control-input" id="terms_and_conditions" value="1">
                      <label class="custom-control-label" for="terms_and_conditions">@lang('labels.frontend.i_have_read_and_accept_the') <a href="{{ URL('terms-and-conditions/producer') }}" id="terms_and_conditions_link"> @lang('labels.frontend.terms_and_conditions')</a> @lang('labels.frontend.of_company')
                      </label>
                    </div>
                  </div>
                  <div style="width: 100%; display: none;">
                    <p class="pull-left text-danger" style="font-size: 13px;" id="terms_and_conditions_error"></p>
                    <br><br>
                  </div>
                  <div style="width: 100%">
                    <a class="btn btn-outline-primary btn-lg" href="{{ URL('') }}">@lang('labels.frontend.cancel')</a>
                    <button type="submit" name="create" value="create" class="btn btn-primary btn-lg">@lang('labels.frontend.create')</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  @endsection
  @section('script')
  <script src="{{ asset('public/template/js/jquery.validate.min.js') }}"></script>  
  <script src="{{ asset('public/template/js/jquery.form.min.js') }}"></script>
  <script src="{{ asset('public/template/js/jquery.cookie.js') }}"></script>
  <script>

    // only if redirected from terms & conditions page
    @if(URL::previous() == URL('terms-and-conditions/producer'))

      // check if fields set 
      var business_name = $.cookie("producer_business_name");
      if(typeof business_name !== "undefined" && business_name != '')
      {
        $('#business_name').val(business_name);
        $.removeCookie('producer_business_name');
      }

      var cuit = $.cookie("producer_cuit");
      if(typeof cuit !== "undefined" && cuit != '')
      {
        $('#cuit').val(cuit);
        $.ajax({
          url: '{{ URL('api/validate_cuit') }}',
          type: 'POST',
          data: {
            cuit: function() {
              return $( "#cuit" ).val();
            }
          },
          async: false,
          success: function (response) {
            if(response.data.is_valid)
            {
              $('#cuit_error').hide();
              $('#cuit_success').show();
            }
            else
            {
              $('#cuit_error').show();
              $('#cuit_success').hide();
            }
          }
        });
        $.removeCookie('producer_cuit');
      }

      var email = $.cookie("producer_email");
      if(typeof email !== "undefined" && email != '')
      {
        $('#email').val(email);
        $.removeCookie('producer_email');
      }

      var password = $.cookie("producer_password");
      if(typeof password !== "undefined" && password != '')
      {
        $('#password').val(password);
        $.removeCookie('producer_password');
      }

      var password_visible = $.cookie("producer_password_visible");
      if(typeof password_visible !== "undefined" && password_visible == "1")
      {
        $('#show_password').hide();
        $('#hide_password').show();
        $('#password').attr('type', 'text');
        $.removeCookie('producer_password_visible');
      }

      var terms_and_conditions = $.cookie("producer_terms_and_conditions");
      if(typeof terms_and_conditions !== "undefined" && terms_and_conditions == "1")
      {
        $('#terms_and_conditions').prop('checked', true);
        $.removeCookie('producer_terms_and_conditions');
      }

    @endif

    // on click of terms and conditions
    $(document).on('click', '#terms_and_conditions_link', function(e) {

      $.cookie('producer_business_name', $('#business_name').val());
      $.cookie('producer_cuit', $('#cuit').val());
      $.cookie('producer_email', $('#email').val());
      $.cookie('producer_password', $('#password').val());

      if($('#password').attr('type') == 'text')
      {
        $.cookie('producer_password_visible', "1");
      }

      if($('#terms_and_conditions').prop('checked'))
      {
        $.cookie('producer_terms_and_conditions', "1");
      }

      // finally follow the link
      location.href = $(this).attr("href");

      // Or may be giving `return true;` could work?
      return false;
    });

    // show password
    $(document).on('click', '#show_password', function(){
      $('#show_password').hide();
      $('#hide_password').show();
      $('#password').attr('type', 'text');
    });

    // hide password
    $(document).on('click', '#hide_password', function(){
      $('#hide_password').hide();
      $('#show_password').show();
      $('#password').attr('type', 'password');
    });

    // cuit validator
    jQuery.validator.addMethod("valid_cuit", function(value, element) {
      var is_valid = false;
      $.ajax({
        url: '{{ URL('api/validate_cuit') }}',
        type: 'POST',
        data: {
          cuit: function() {
            return $( "#cuit" ).val();
          }
        },
        async: false,
        success: function (response) {
          if(response.data.is_valid)
          {
            $('#cuit_error').hide();
            $('#cuit_success').show();
            is_valid = true;
          }
          else
          {
            $('#cuit_error').show();
            $('#cuit_success').hide();
            is_valid = false;
          }
        }
      });
      return is_valid;
    }, '');

    // validate form
    $("#register_form").validate({
      rules: {
        cuit : {
          valid_cuit: true,
        },
      },
    });

    // submit form
    $('#register_form').ajaxForm({ 
      beforeSubmit : function(arr, $form, options){
        $('#register_form input').removeClass('placeholder-danger');
        $('#business_name').attr('placeholder', "@lang('labels.frontend.business_name')");
        $('#cuit').attr('placeholder', "@lang('labels.frontend.cuit')");
        $('#email').attr('placeholder', "@lang('labels.frontend.email')");
        $('#password').attr('placeholder', "@lang('labels.frontend.password')");
        $('#terms_and_conditions_error').html("");
        $('#terms_and_conditions_error').closest('div').hide();
      },
      success : function(response){
        if(response.code == 200)
        {
          window.location = "{{ URL('register/producer/success') }}";
        }
        else
        {
          var focused = false;
          for (var key in response.errors) 
          {
            if(key != 'terms_and_conditions')
            {
              $(`#${key}`).val('');
              $(`#${key}`).addClass('placeholder-danger');
              $(`#${key}`).attr('placeholder', response.errors[key]);

              if(!focused)
              {
                $(`#${key}`).focus();
                focused = true;
              }
            }
            else
            {
              $('#terms_and_conditions_error').html(response.errors[key]);
              $('#terms_and_conditions_error').closest('div').show();
            }
          }
        }
      }
    }); 
  </script>
  @endsection
