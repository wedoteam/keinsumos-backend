@extends('layouts.advanced')
  @section('title')
  <title>{{ __('labels.page_title.value_proposal_for_agronomies') }} | {{ config('app.name') }}</title>
  @endsection
  @section('style')
  <style type="text/css">
    body { overflow-x: hidden; }
  </style>
  @endsection
  @section('content')
  <section >
    <div class="banner_part-wrap " style="background-image:url({{ asset('public/template/images/ke_insumos_bg.png') }});"> 
      <div class="banner_mobile_views"> 
        <div class="mobile-content home-mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }} );">
          <div class="mobile-banner-logo">
            <img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
          </div>
          <h1>¡El lugar más fácil para comprar <br> y vender agroinsumos!</h1>
        </div>
      </div>
      <div class="banner-left-part propuesta_title animatedParent" style="overflow: hidden;">
        <h1 class="banner_before animated bounceInDown go">Propuesta de Valor <br> para las <strong>Agronomías </strong></h1>     
      </div>
      <div class="banner-right-part animated bounceInDown go">
        <div class="banner_content">        
          <p>Ke Insumos es un nuevo <strong> canal de venta,</strong> que le brinda acceso a una base de  <strong>Productores de calidad </strong>y la facilidad para poder  <strong> cerrar operaciones</strong> en el momento.</p>       
        </div>
      </div>
    </div>
  </section>
  <section class="cards-info-section desktop_propuesta_de_valor_agronomias animatedParent" >
    <div class="container">
      <div class="row cards-info-wrap">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12 text-center custom__max-width animated fadeIn delay-250">
          <div class="contact-part">
            <div class="img-wrap mayores_ingresos"> 
              <img src="{{ asset('public/template/images/mayores_ingresos.png') }}" alt="Location Icon"> 
            </div>
            <h4>Mayores Ingresos</h4>
            <p>Permite ampliar sin inversión su cartera de clientes, llegar a más ciudades y vender más.</p>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12  text-center custom__max-width  animated fadeIn delay-500">
          <div class="contact-part">
            <div class="img-wrap mayor_eficiencia"> 
              <img src="{{ asset('public/template/images/mayor_eficiencia.png') }}" alt="Contact Icon"> 
            </div>
            <h4>Mayor Eficiencia</h4>
            <p>Genera eficiencias y ahorros en tiempo comercial por la migración de los llamados de los productores para solicitar cotizaciones a la plataforma, pudiendo captar pedidos on line, las 24 horas.</p>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12  text-center custom__max-width  animated fadeIn delay-750">
          <div class="contact-part">
            <div class="img-wrap transformacion_digital"> 
              <img src="{{ asset('public/template/images/transformación_digital.png') }}" alt="Email Icon"> 
            </div>
            <h4>Transformación Digital</h4>
            <p>Permite sumarse a la tendencia de transformación mediante la adopción de tecnología digital en el proceso de venta.</p>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12 text-center  custom__max-width animated fadeIn delay-1250">
          <div class="contact-part">
            <div class="img-wrap mejor_informacion"> 
              <img src="{{ asset('public/template/images/mejor_información.png') }}" alt="Email Icon"> 
            </div>
            <h4>Mejor Información</h4>
            <p>Brinda acceso a Información de gestión del negocio desde distintas perspectivas, como cliente, producto, región.</p>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-12  text-center custom__max-width animated fadeIn delay-1250">
          <div class="contact-part">
            <div class="img-wrap campanas_comerciales"> 
              <img src="{{ asset('public/template/images/campañas_comerciales.png') }}" alt="Email Icon"> 
            </div>
            <h4>Campañas Comerciales</h4>
            <p>Presenta un canal para realizar campañas y promociones de venta y comunicarlas a la base de clientes seleccionados.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="banner_part-wrap celular_app ofrecemos_video animatedParent" > 
      <div class="container">
        <div class="row align-items-center" style="overflow: hidden;">
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_content_display animated bounceInLeft slow">
            <div class="ofrecemos_content">
              <h5>Ofrecemos <strong>oportunidades de venta </strong>  a través del tráfico de <strong>  Productores </strong>  en la plataforma. </h5>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ofrecemos_video_thumbnail animated bounceInRight slow"> 
            <a data-fancybox class="full-click-wrap" href="https://www.youtube.com/embed/5Koy2TUs3l0">
              <img src="{{ asset('public/template/images/placeholder-2.jpg') }}">
              <i class="fa fa-play-circle" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="cta_btn animatedParent" style="overflow: hidden;">
    <div class="container">
      <div class="cta_btn_contnet animated bounceInLeft slow">
        <div class="cta_btn_title">
          <h4>DESCARGA LA APP</h4>
        </div>
        <div class="banner-btn-grp cta_btn_parent">    
          <a href="https://play.google.com/store/apps/details?id=com.keinsumos" target="_blank">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/ke_insumos_android-2.png') }}" alt="Google Play">
            </div>
            <div class="icon-txt">  
              <span class="big-txt">Google play</span>
            </div>
          </a>
          <a href="https://apps.apple.com/us/app/ke-insumos/id1500566236?ls=1" target="_blank">
            <div class="icon-wrap">
              <img src="{{ asset('public/template/images/ke_insumos_iOS2.png') }}" alt="Apply">
            </div>
            <div class="icon-txt">
              <span class="big-txt">App Store</span>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  @endsection