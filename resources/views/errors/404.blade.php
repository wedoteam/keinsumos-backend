@extends('layouts.app')
  @section('title')
    <title>{{ __('labels.page_title.register') }} | {{ config('app.name') }}</title>

  @endsection 
  @section('content')
	<div class="banner-form-wrap login-banner header_bg page-404-banner">
		<div class="mobile-content" style="background-image:url({{ asset('public/template/images/ke_insumos_banner_img_mobile.png') }});">
			<div class="mobile-banner-logo">
				<img src="{{ asset('public/template/images/ke_insumos_logo_mobile.png') }}">
			</div>
		</div>
    </div>	
	<div class="success-section page-404-content">
		<div class="container footer-bottom-container">
			<div class="wizard__header-content">				
				<div class="success-content-wrap">
					<h1>Error 404</h1>
					<h4>página no encontrada</h4>
					<img src="{{ asset('public/template/images/404.png') }}">
					<p>La página que intentas solicitar no está en el servidor.
					Por favor, intenta volviendo al <a href="#">inicio.</a></p>
				</div>
			</div>
		</div>
		<div class="float-sm">
				<div class="fl-fl float-wa">
					<i class="fab fa-whatsapp"></i>
					<a href="https://wa.me/{{ env('MIX_WHATSAPP_NO') }}" target="_blank"> WhatsApp</a>
				</div>
			</div>
    </div>
  @endsection
  @section('script') 
  @endsection

